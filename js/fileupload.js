/*
 * This file is used for drag & drop file upload and image preview
 */

/************************************************************
 * Add the JavaScript support for drag & drop/browse upload *
 ***********************************************************/
function makeDroppable(element, callback) {
    if(typeof element === 'undefined' || element === null) {
        return;
    }

    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('multiple', true);
    input.style.display = 'none';
    input.addEventListener('change', function (e) {
        triggerCallback(e, callback);
    });
    element.appendChild(input);

    element.addEventListener('dragover', function (e) {
        e.preventDefault();
        e.stopPropagation();
        element.classList.add('dragover');
    });

    element.addEventListener('dragleave', function (e) {
        e.preventDefault();
        e.stopPropagation();
        element.classList.remove('dragover');
    });

    element.addEventListener('drop', function (e) {
        e.preventDefault();
        e.stopPropagation();
        element.classList.remove('dragover');
        triggerCallback(e, callback);
    });

    element.addEventListener('click', function () {
        input.value = null;
        input.click();
    });
}

function triggerCallback(e, callback) {
    if (!callback || typeof callback !== 'function') {
        return;
    }
    var files;
    if (e.dataTransfer) {
        files = e.dataTransfer.files;
    } else if (e.target) {
        files = e.target.files;
    }
    callback.call(null, files);
}

/*************************************************************************
 * After drag & drop upload, create image elements and add image preview *
 * Make images draggable to canvas and register mouse & drag events      *
 ************************************************************************/
 var ul = document.createElement('ul');
        ul.className = ("thumb-Images");
        ul.id = "imgList";
makeDroppable(document.querySelector('#dropZone'), function (files) {
    var output = document.querySelector('#images_preview');
    output.innerHTML = '';

    for (var i = 0; i < files.length; i++) {
        if (files[i].type.indexOf('image/') === 0) {

            var reader = new FileReader();
            reader.onload = (function (readerEvt) {
                    return function (e) {
                 var li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = ['<div class="img-wrap"> <span class="close">&times;</span>' +
                '<img width="80" height="80" draggable="true" id="'+Math.random().toString(36).substr(2, 9)+'" class="imagedelete" src="', e.target.result, '" title="', escape(readerEvt.name), '" data-id="',
                readerEvt.name, '"/>' + '</div>'].join('');

            var div = document.createElement('div');
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join('');
            document.getElementById('images_preview').insertBefore(ul, null);

		    li.ondragstart = function (e) {  // Register drag event						  
		    e.dataTransfer.setData("text", e.target.id);
                };
				
                output.appendChild(li);      // Add image preview to page
			}
            })(i);
            reader.readAsDataURL(files[i]);
        }
    }
});

var AttachmentArray = [];
jQuery(function ($) {
            $('div').on('click', '.img-wrap .close', function () {
                var id = $(this).closest('.img-wrap').find('img').data('id');

                //to remove the deleted item from array
                var elementPos = AttachmentArray.map(function (x) { return x.FileName; }).indexOf(id);
                if (elementPos !== -1) {
                    AttachmentArray.splice(elementPos, 1);
                }

                //to remove image tag
                $(this).parent().find('img').not().remove();

                //to remove div tag that contain the image
                $(this).parent().find('div').not().remove();

                //to remove div tag that contain caption name
                $(this).parent().parent().find('div').not().remove();

                //to remove li tag
                var lis = document.querySelectorAll('#imgList li');
                for (var i = 0; li = lis[i]; i++) {
                    if (li.innerHTML == "") {
                        li.parentNode.removeChild(li);
                    }
                }

            });
        }
        )
        
        
 function FillAttachmentArray(e, readerEvt)
        {
            AttachmentArray[arrCounter] =
            {
                AttachmentType: 1,
                ObjectType: 1,
                FileName: readerEvt.name,
                FileDescription: "Attachment",
                NoteText: "",
                MimeType: readerEvt.type,
                Content: e.target.result.split("base64,")[1],
                FileSizeInBytes: readerEvt.size,
            };
            arrCounter = arrCounter + 1;
        }        
