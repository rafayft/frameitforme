<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frames extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_portfolio');
        $this->load->model('Model_kids');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['page_portfolio'] = $this->Model_common->all_page_portfolio();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $data['portfolio_subcategory'] = $this->Model_kids->get_portfolio_subcategory();

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_frame_page');
        $this->load->view('view_footer');
    }


}
