<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller
{
    private $outletRef;
    private $apiKey;
    private $tokenURL;
    private $authorizedURL;
    private $captureURL;

    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_contact');
        $this->load->model('Model_portfolio');
        $this->load->model('Model_order');
        $this->load->model('Model_payments');
        $this->load->model('Model_emailtemplates');

        $this->outletRef = $this->config->item('outletRef');
        $this->apiKey = $this->config->item('apiKey');
        $this->tokenURL = $this->config->item('tokenURL');
        $this->authorizedURL = $this->config->item('authorizedURL');
        $this->captureURL = $this->config->item('captureURL');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_contact'] = $this->Model_common->all_page_contact();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $data['testimonials'] = $this->Model_contact->all_testimonial();
        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        if ($this->session->userdata('cart')) {
            $data['total'] = $this->total();
        }


        $this->load->view('view_header', $data);
        $this->load->view('view_cart', $data);
        $this->load->view('view_footer', $data);
    }

    private function total()
    {
        if(!is_array($this->session->userdata('cart'))) {
            $this->session->unset_userdata('cart');
            $this->session->unset_userdata('totalcart');
        }

        if(is_array($this->session->userdata('cart'))) {
            $total = array_sum(array_column($this->session->userdata('cart'), 'price'));
        } else {
            $total = array_sum(array_column(unserialize($this->session->userdata('cart')), 'price'));
        }

        return $total;
    }


    public function checkout()
    {
       /* echo "<pre>";
        print_r($this->input->post('installationservice'));
        die;*/
       if($this->input->post("totalamountnew")) {
           $totalitem = array(
               'shippingamount' => 0,
               'promoamount' => $this->input->post('promocodevalue'),
               'subtotal' => $this->input->post('subtotalss'),
               'totalamount' => $this->input->post('totalamountnew'),
               'installtion' => $this->input->post('installationservice'),
               'vatamount' => '',
               'weight' => '',
           );
           //$tatcart = array($totalitem);
           $this->session->set_userdata('totalcart', $totalitem);
       }



        if (!$this->session->userdata('logged_in')) {
            redirect('login?status=checkout');
        } else {
            $data['setting'] = $this->Model_common->all_setting();
            $data['page_contact'] = $this->Model_common->all_page_contact();
            $data['comment'] = $this->Model_common->all_comment();
            $data['social'] = $this->Model_common->all_social();
            $data['all_news'] = $this->Model_common->all_news();

            $data['testimonials'] = $this->Model_contact->all_testimonial();
            $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();
            if ($this->session->userdata('cart')) {
                $data['total'] = $this->total();
            }

            $this->load->view('view_header', $data);
            $this->load->view('view_checkout', $data);
            $this->load->view('view_footer', $data);
        }
    }


    public function getcoupon()
    {
        $code = $this->input->post('couponcode');
        $date = date('Y-m-d');

        $data = array(
            'code' => $code,
            'status' => 1
        );


        $getcode = $this->db->get_where('tbl_coupon', $data);
        //print_r($this->db->last_query());
        if ($getcode->num_rows() > 0) {
            foreach ($getcode->result() as $row) {
                $datarow = $row->amount;
            }

            $hsonarray = array(
                'success' => true,
                'amount' => $datarow,
                'message' => 'Promo Code Is added'
            );
            echo json_encode($hsonarray);
        } else {
            $hsonarray = array(
                'error' => true,
                'message' => 'The Code Is expired'
            );
            echo json_encode($hsonarray);
        }


    }


    public function createpayment()
    {
      /*  print_r($this->input->post('famrname'));
        die();*/
        $order_id = $this->input->post('order_id');

        if ($order_id > 0) {
            $id = array(
                'id' => $order_id,
            );
            $orderdata = array(
                'customer_id' => $this->session->userdata('id'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'addressone' => $this->input->post('addressone'),
                'addresstwo' => $this->input->post('addresstwo'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'phone' => $this->input->post('phone'),
                'zipcode' => $this->input->post('zipcode'),
                'shippingaddressone' => $this->input->post('shippingaddressone'),
                'shippingaddresstwo' => $this->input->post('shippingaddresstwo'),
                'shippingcountry' => $this->input->post('shippingcountry'),
                'shippingstate' => $this->input->post('shippingstate'),
                'shippingzipcode' => $this->input->post('shippingzipcode'),
                'status' => 1,
                'installationservice' => $this->input->post('installtion'),
                'subtotal' => $this->input->post('subtotal'),
                'discount' => $this->input->post('discount'),
                'totalamount' => $this->input->post('totalamount'),
                'shippingamount' => $this->input->post('shippingfee'),
            );
            $updateorder = $this->Model_order->update_order($orderdata, $id);
            $updateorder = $this->Model_order->update_frameorder(array('status' => 1), array('order_id' => $order_id));
            $updateorder = $this->Model_order->update_subscriptionorder(array('status' => 1), array('order_id' => $order_id));
        } else {

            $orderdata = array(
                'customer_id' => $this->session->userdata('id'),
                'installationservice' => $this->input->post('installtion'),
                'subtotal' => $this->input->post('subtotal'),
                'discount' => $this->input->post('discount'),
                'vat' => 0,
                'totalweight' => 0,
                'totalamount' => $this->input->post('totalamount'),
                'status' => 2,
                'shippingamount' => $this->input->post('shippingfee'),
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'addressone' => $this->input->post('addressone'),
                'addresstwo' => $this->input->post('addresstwo'),
                'country' => $this->input->post('country'),
                'state' => $this->input->post('state'),
                'phone' => $this->input->post('phone'),
                'zipcode' => $this->input->post('zipcode'),
                'shippingaddressone' => $this->input->post('shippingaddressone'),
                'shippingaddresstwo' => $this->input->post('shippingaddresstwo'),
                'shippingcountry' => $this->input->post('shippingcountry'),
                'shippingstate' => $this->input->post('shippingstate'),
                'shippingzipcode' => $this->input->post('shippingzipcode'),
            );
            $order_id = $addorder = $this->Model_order->add_order($orderdata);
            if ($addorder) {
                $frameid = $this->input->post('id');
                $frameimage = $this->input->post('fameimage');
                $frameprice = $this->input->post('frameamount');
                $framequantity = $this->input->post('framequantity');
                $framename = $this->input->post('famrname');
                for ($i = 0; $i < count($frameid); $i++) {
                    $frame_data = array(
                        'order_id' => $addorder,
                        'customer_id' => $this->session->userdata('id'),
                        'name' => $framename[$i],
                        'price' => $frameprice[$i],
                        'image' => $frameimage[$i],
                        'quantity' => $framequantity[$i],
                        'desciption' => '-',
                        'status' => 2,

                    );
                    $updateorder = $this->Model_order->add_order_frames($frame_data);
                }
            }
        }

        if ($updateorder) {
            $order = new StdClass();
            $order->action = "SALE";                                        // Transaction mode ("AUTH" = authorize only, no automatic settle/capture, "SALE" = authorize + automatic settle/capture)
            $order->amount = new StdClass();
            $order->amount->currencyCode = "AED";                           // Payment currency ('AED' only for now)
            $order->amount->value = $this->input->post('totalamount') * 100;                                   // Minor units (1000 = 10.00 AED)
            $order->language = "en";                                        // Payment page language ('en' or 'ar' only)
            $order->emailAddress = $this->session->userdata('email');                                        // Payment page language ('en' or 'ar' only)
            $order->billingAddress = new StdClass();
            $order->billingAddress->firstName = $orderdata['firstname'];                                        // Payment page language ('en' or 'ar' only)
            $order->billingAddress->lastName = $orderdata['lastname'];                                       // Payment page language ('en' or 'ar' only)
            $order->merchantOrderReference = $order_id;
            $order->merchantAttributes = new StdClass();
            $order->merchantAttributes->redirectUrl = base_url() . 'Cart/acceptpayment';     // A redirect URL to a page on your site to return the customer to
            $order->merchantAttributes->skipConfirmationPage = true;

            $output = $this->invokeCurl($order, 'AUTH');
            if(!isset($output->reference)){
                $output = $this->invokeCurl($order, 'AUTH');
                if(!isset($output->reference)){
                    $output = $this->invokeCurl($order, 'AUTH');
                }
            }
            $order_reference = $output->reference;
            $order_paypage_url = $output->_links->payment->href;
            $this->Model_order->update_order(['order_ref' => $order_reference], ['id' => $order_id]);

            $this->session->unset_userdata('cart');
            $this->session->unset_userdata('totalcart');
            $this->session->unset_userdata('subscriptioncart');

            redirect($order_paypage_url);
        }
    }

    /**
     * @return mixed
     */
    private function getToken()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->tokenURL);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "accept: application/vnd.ni-identity.v1+json",
            "authorization: Basic " . $this->apiKey,
            "content-type: application/vnd.ni-identity.v1+json"
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"realmName\":\"ni\"}");
        $output = json_decode(curl_exec($ch));

        return $output->access_token;
    }

    /**
     * @param $postData
     * @param string $type
     * @param array $options
     * @return mixed
     */
    private function invokeCurl($postData, $type = 'AUTH', $options = [])
    {
        $access_token = $this->getToken();

        if ($type == 'AUTH') {
            $url = str_replace('{outletRef}', $this->outletRef, $this->authorizedURL);
        } else {
            $url = str_replace(
                ['{outletRef}', '{orderReference}', '{paymentReference}'],
                [$this->outletRef, $options['orderReference'], $options['paymentReference']],
                $this->captureURL
            );;
        }

        $json = json_encode($postData);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authorization: Bearer " . $access_token,
            "Content-Type: application/vnd.ni-payment.v2+json",
            "Accept: application/vnd.ni-payment.v2+json"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $output = json_decode(curl_exec($ch));

        curl_close($ch);

        return $output;
    }

    function acceptpayment()
    {
        $send_email_from=$this->Model_common->all_setting()['send_email_from'];
       // print_r($receive_email_to);
        $receive_email_to=$this->Model_common->all_setting()['receive_email_to'];
        //die();
        $options['orderReference'] = $_GET['ref'];
        $options['paymentReference'] = $_GET['ref'];

        $orderRow = $this->Model_order->get_order_by_ref($options['orderReference']);

        if (empty($orderRow)) {
            $this->session->set_flashdata('error', "No order found or successful.");
            //redirect(base_url() . 'Cart/checkout');
        } elseif ($orderRow['paymentstatus'] == '1') {
            $this->session->set_flashdata('error', "Order already successful.");
           // redirect(base_url() . 'Cart/checkout');
        }

        /*$order = new StdClass();
        $order->amount = new StdClass();
        $order->amount->currencyCode = "AED";                           // Payment currency ('AED' only for now)
        $order->amount->value = $orderRow['totalamount'] * 100;

        $orderResponse = $this->invokeCurl($order, 'CAPTURE', $options);*/

        $orderFrames = $this->Model_order->get_order_frames($orderRow['id']);

        $updateorder = $this->Model_order->update_order(array('paymentstatus' => 1), array('id' => $orderRow['id']));
        $this->Model_payments->add_payment([
            'order_id' => $orderRow['id'],
            'customer_id' => $this->session->userdata('id'),
            'order_ref' => $options['orderReference'],
            'amount' => $orderRow['totalamount'],
        ]);

        $email = $this->Model_emailtemplates->get('new_order_user');
        $emailAdmin = $this->Model_emailtemplates->get('new_order_admin');
        $tr = '';
        $billing_address = "{$orderRow['firstname']} {$orderRow['lastname']} <br> {$orderRow['addressone']}, {$orderRow['addresstwo']}, {$orderRow['state']} <br> {$orderRow['country']}  <br> {$orderRow['phone']} <br>{$this->session->userdata('email')}";
        $shipping_address=$billing_address;
        if(!empty($orderRow['shippingcountry']))
            $shipping_address = "{$orderRow['firstname']} {$orderRow['lastname']} <br> {$orderRow['shippingaddressone']}, {$orderRow['shippingaddresstwo']},{$orderRow['shippingstate']} <br> {$orderRow['shippingcountry']} <br> {$orderRow['phone']}<br>{$this->session->userdata('email')}";

        foreach ($orderFrames as $orderFrame) {

            $tr .= '<tr class="x_order_item">
                                          <td class="x_td" style="color:#737373; border:1px solid #e4e4e4; padding:12px; text-align:left; vertical-align:middle; font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif; word-wrap:break-word">
                                            '.$orderFrame['name'].'
                                          </td>
                                          <td class="x_td" style="color:#737373; border:1px solid #e4e4e4; padding:12px; text-align:left; vertical-align:middle; font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">
                                            '.$orderFrame['quantity'].'
                                          </td>
                                          <td class="x_td" style="color:#737373; border:1px solid #e4e4e4; padding:12px; text-align:left; vertical-align:middle; font-family:\'Helvetica Neue\',Helvetica,Roboto,Arial,sans-serif">
                                            <span class="x_woocommerce-Price-amount x_amount">AED '.$orderFrame['price']*$orderFrame['quantity'].'</span>
                                          </td>
                                        </tr>';
        }
        //print_r($this->session->userdata('email'));
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'frameitforme.com',
            'smtp_port' => 587,
            'smtp_user' => 'no-reply@frameitforme.com',
            'smtp_pass' => 'Ffme#569745',
            'mailtype'  => 'html',
            'wordwrap'   => TRUE,
            'newline'   => '\r\n'
        );

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($send_email_from, 'Frame It For Me');
        $this->email->to($this->session->userdata('email'));
        $this->email->subject($email['subject']);
        $this->email->message($this->getBody($orderRow, $tr, $billing_address, $email,$shipping_address));
        $this->email->send();
        //die();

        $this->email->initialize($config);
        $this->email->from($send_email_from, 'Frame It For Me');
        $this->email->to($receive_email_to);
        $this->email->subject($emailAdmin['subject']);
        $this->email->message($this->getBody($orderRow, $tr, $billing_address, $emailAdmin,$shipping_address));
        $this->email->send();

        if ($updateorder) {
            redirect(base_url() . 'Cart/paymentsuccess');
        }

        $this->session->set_flashdata('error', "Order Status not updated.");
        redirect(base_url() . 'Cart/checkout');
    }

    function paymentsuccess()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_contact'] = $this->Model_common->all_page_contact();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();

        $this->load->view('view_header', $data);
        $this->load->view('view_thankyou', $data);
        $this->load->view('view_footer', $data);
    }

    function deletedraft()
    {
        $order_id= $this->uri->segment(3);


        $cart = array_values(($this->session->userdata('cart')));
        $cat=[];
        foreach ($cart as $c){
            if($c['id']!=$order_id)
                $cat[]=$c;
        }
        $this->session->set_userdata('cart', ($cat));

        $deletedraft = $this->Model_order->deletedraft($order_id);
        //print_r($this->db->last_query());
        if ($deletedraft) {
            $json = array(
                'success' => true,
            );
            echo json_encode($json);
        }
    }

    /**
     * @param $orderRow
     * @param string $tr
     * @param string $billing_address
     * @param $email
     * @return string|string[]
     */
    public function getBody($orderRow,  $tr, $billing_address, $email,$shipping_address)
    {
        return str_replace([
            '{{order_id}}',
            '{{date}}',
            '{{order_details}}',
            '{{total}}',
            '{{subtotal}}',
            '{{shippingfee}}',
            '{{promo}}',
            '{{installation}}',
            '{{billing_address}}',
            '{{first_name}}',
            '{{last_name}}',
            '{{shipping_address}}',
            '{{name}}'
        ], [
            $orderRow['id'],
            date('m/d/Y'),
            $tr,
            $orderRow['totalamount'],
            $orderRow['subtotal'],
            $orderRow['shippingamount'],
            $orderRow['discount'],
            $orderRow['installationservice'],
            $billing_address,
            $orderRow['firstname'],
            $orderRow['lastname'],
            $shipping_address,
            $orderRow['firstname']
        ], $email['body']);
    }


}