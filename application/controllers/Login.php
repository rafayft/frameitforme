<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_home');
        $this->load->model('Model_portfolio');
        $this->load->model('Model_user');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['all_news_category'] = $this->Model_common->all_news_category();

        $data['sliders'] = $this->Model_home->all_slider();
        $data['services'] = $this->Model_home->all_service();
        $data['features'] = $this->Model_home->all_feature();
        $data['why_choose'] = $this->Model_home->all_why_choose();
        $data['team_members'] = $this->Model_home->all_team_member();
        $data['testimonials'] = $this->Model_home->all_testimonial();
        $data['clients'] = $this->Model_home->all_client();
        $data['pricing_table'] = $this->Model_home->all_pricing_table();
        $data['home_faq'] = $this->Model_home->all_faq_home();

        $data['portfolio_category'] = $this->Model_portfolio->get_portfolio_category();
        $data['portfolio'] = $this->Model_portfolio->get_portfolio_data();

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_login', $data);
        $this->load->view('view_footer', $data);
    }


    public function check_login()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $json = array(
                'success' => false,
                'message' => validation_errors(),
            );
            echo json_encode($json);
        } else {
            $email = $this->input->post('email', true);
            $password = $this->input->post('password', true);
            $checkstatus = $this->Model_user->checkstatus($email);

            if ($checkstatus->num_rows() > 0) {
                $error = 'Your account is not active kindly active your account';
                $json = array(
                    'success' => false,
                    'message' => $error,
                );
                echo json_encode($json);
                die();
            }
            // Checking the email address
            $un = $this->Model_user->check_email($email);

            if ($un->num_rows() == 0) {
                $error = 'Email address or password is wrong!';
                $json = array(
                    'success' => false,
                    'message' => $error,
                );
                echo json_encode($json);

            } else {

                // When email found, checking the password
                $pw = $this->Model_user->check_password($email, $password);

                if ($pw->num_rows() == 0) {

                    $error = 'Email address or password is wrong!';
                    $json = array(
                        'success' => false,
                        'message' => $error,
                    );
                    echo json_encode($json);

                } else {

                    // When email and password both are correct
                    $user = $pw->result_array();
                    foreach ($user as $use) {
                        $this->loginSession($use);
                    }


                    $json = array(
                        'success' => true,
                        'message' => 'You have successfully Logged In',
                    );
                    echo json_encode($json);
                }
            }
        }
    }


    function logout()
    {
        $this->session->unset_userdata(['logged_in', 'id', 'email', 'password', 'status']);
       // $this->session->sess_destroy();
        redirect(base_url('login'));
    }


    public function active()
    {
        $code = $this->input->get('code');

        //$id   = $this->input->post('userid');


        $data = array(
            'status' => 1,
        );
        $id = array(
            'code' => $code
        );

        $update = $this->Model_user->updatestatus($data, $id);

        if ($update > 0) {
            $success = 'Your Account is being active';
            $this->session->set_flashdata('success', $success);
            redirect(site_url() . '/login');
        } else {
            $error = 'Server is not responding correctly';
            $this->session->set_flashdata('error', $error);
            redirect(site_url() . '/login');
        }
    }

    public function google_login()
    {
        $this->load->library('google');

        $clientId = $this->config->item('google_client_id');
        $clientSecret = $this->config->item('google_client_secret');
        $redirectURL = base_url() . 'Login/google_login/';

        //Call Google API
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectURL);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['token'])) {
            $gClient->setAccessToken($_SESSION['token']);
        }

        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();

            if(empty($userProfile)) {
                $error = 'Server is not responding correctly';
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'login');
            }

            $email = $this->Model_user->check_email($userProfile['email']);

            if ($email->num_rows() > 0) {
                $this->Model_user->updatestatus([
                    'status' => 1,
                    'oauth_provider' => 'google',
                    'oauth_uid' => $userProfile['id'],
                    'photo' => $userProfile['picture'],
                ], ['email' => $userProfile['email']]);
                $user = $email->row_array();

                $this->loginSession($user);
            } else {
                $code = rand(1000, 9999);
                $data = [
                    'name' => $userProfile['name'],
                    'email' => $userProfile['email'],
                    'password' => md5($userProfile['email']),
                    'status' => 1,
                    'code' => $code,
                    'oauth_provider' => 'google',
                    'oauth_uid' => $userProfile['id'],
                    'photo' => $userProfile['picture'],
                ];
                $inserdata = $this->Model_user->add_customer($data);
                $data['customer_id'] = $inserdata;

                $this->loginSession($data);
            }

            $this->session->set_flashdata('success', "Login Successful.");
            redirect(base_url() . 'frame_layout');
        } else {
            $url = $gClient->createAuthUrl();
            redirect($url);
        }
    }

    /**
     * @param $user
     */
    private function loginSession($user)
    {
        $array = [
            'id' => $user['customer_id'],
            'email' => $user['email'],
            'password' => $user['password'],
            'status' => $user['status'],
            'logged_in' => true,
        ];
        $this->session->set_userdata($array);
    }
}
