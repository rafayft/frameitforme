<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_subscription');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['subscription'] = $this->Model_subscription->show_active_subscriber();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_subscription', $data);
        $this->load->view('admin/view_footer');
    }


    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $error = '';
        $success = '';

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('content', 'Content', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $next_id = $this->Model_subscription->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }

                $form_data = array(
                    'name' => $_POST['name'],
                    'description' => $_POST['content'],
                    'price' => $_POST['price'],
                );
                $this->Model_subscription->add($form_data);

                $success = 'Subscription Plan is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/subscription');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/subscription/add');
            }

        } else {

            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_subscription_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {

        $tot = $this->Model_subscription->subscription_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/subscription');
            exit;
        }

        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';


        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('content', 'Content', 'trim|required');
            //$this->form_validation->set_rules('icon', 'Icon', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $data['why_choose'] = $this->Model_subscription->getData($id);


                $form_data = array(
                    'name' => $_POST['name'],
                    'description' => $_POST['content'],
                    'price' => $_POST['price']
                );
                $this->Model_subscription->update($id, $form_data);

                $success = 'Subscription Plan is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/subscription');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/subscription/edit/' . $id);
            }

        } else {
            $data['why_choose'] = $this->Model_subscription->getData($id);
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_subscription_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        $tot = $this->Model_subscription->subscriber_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/subscriber');
            exit;
        }

        $this->Model_subscription->delete($id);
        $success = 'Subscriber is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/subscriber');
    }


}