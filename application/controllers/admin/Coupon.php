<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_coupon');
    }

    public function index()
    {

        $header['setting'] = $this->Model_common->get_setting_data();
        $data['partner'] = $this->Model_coupon->show();

        $this->load->view('admin/view_header', $header);
        $this->load->view('admin/view_coupon', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {


        $data['error'] = '';
        $data['success'] = '';
        $error = '';
        $header['setting'] = $this->Model_common->get_setting_data();

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('code', 'Promo Code', 'trim|required');
            $this->form_validation->set_rules('expirydate', 'Expiry Date', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $next_id = $this->Model_coupon->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }


                $form_data = array(
                    'code' => $_POST['code'],
                    'expirydate' => $_POST['expirydate'],
                    'amount' => $_POST['amount'],

                );
                $this->Model_coupon->add($form_data);

                $data['success'] = 'Promo Code is added successfully!';

                unset($_POST['code']);
                unset($_POST['expirydate']);
                unset($_POST['amount']);
            } else {
                $data['error'] = $error;
            }

            $this->load->view('admin/view_header', $header);
            $this->load->view('admin/view_coupon_add', $data);
            $this->load->view('admin/view_footer');

        } else {

            $this->load->view('admin/view_header', $header);
            $this->load->view('admin/view_coupon_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {

        // If there is no partner in this id, then redirect
        $tot = $this->Model_coupon->partner_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/coupon');
            exit;
        }

        $header['setting'] = $this->Model_common->get_setting_data();
        $data['error'] = '';
        $data['success'] = '';
        $error = '';


        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('code', 'Promo Code', 'trim|required');
            $this->form_validation->set_rules('expirydate', 'Expiry Date', 'trim|required');
            $this->form_validation->set_rules('amount', 'Amount', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $data['partner'] = $this->Model_coupon->getData($id);
                $form_data = array(
                    'code' => $_POST['code'],
                    'expirydate' => $_POST['expirydate'],
                    'amount' => $_POST['amount'],

                );


                $this->Model_coupon->update($id, $form_data);


                $data['success'] = 'Coupon Code is updated successfully';
            } else {
                $data['error'] = $error;
            }

            $data['partner'] = $this->Model_coupon->getData($id);
            $this->load->view('admin/view_header', $header);
            $this->load->view('admin/view_coupon_edit', $data);
            $this->load->view('admin/view_footer');

        } else {
            $data['partner'] = $this->Model_coupon->getData($id);
            $this->load->view('admin/view_header', $header);
            $this->load->view('admin/view_coupon_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        // If there is no partner in this id, then redirect
        $tot = $this->Model_coupon->partner_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/customer');
            exit;
        }


        $this->Model_coupon->delete($id);
        redirect(base_url() . 'admin/customer');
    }

}