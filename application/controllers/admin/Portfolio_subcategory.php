<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio_subcategory extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_portfolio_subcategory');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['portfolio_subcategory'] = $this->Model_portfolio_subcategory->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_portfolio_subcategory', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['category'] = $this->Model_portfolio_subcategory->getcategory();

        $error = '';
        $success = '';

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('subcategory_name', 'Sub Category Name', 'trim|required');
            $this->form_validation->set_rules('category_id', 'Category Name', 'required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            }

            if ($valid == 1) {

                $form_data = array(
                    'category_id' => $_POST['category_id'],
                    'subcategoryname' => $_POST['subcategory_name'],
                    'status' => $_POST['status']
                );
                $this->Model_portfolio_subcategory->add($form_data);

                $success = 'Frame Sub category is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/portfolio_subcategory');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/portfolio_subcategory/add');
            }

        } else {

            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_portfolio_subcategory_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {
        $tot = $this->Model_portfolio_subcategory->portfolio_category_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/portfolio_subcategory');
            exit;
        }

        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';


        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('subcategory_name', 'Sub Category Name', 'trim|required');
            $this->form_validation->set_rules('category_id', 'Category Name', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            } else {

                // Duplicate Category Checking
                $data['portfolio_category'] = $this->Model_portfolio_subcategory->getData($id);
                $total = $this->Model_portfolio_subcategory->duplicate_check($_POST['subcategory_name'], $data['portfolio_category']['subcategoryname']);

                if ($total) {
                    $valid = 0;
                    $error = 'Sub Category name already exists';
                }
            }

            if ($valid == 1) {
                $form_data = array(
                    'category_id' => $_POST['category_id'],
                    'subcategoryname' => $_POST['subcategory_name'],
                    'status' => $_POST['status']
                );

                $this->Model_portfolio_subcategory->update($id, $form_data);


                $success = 'Frame Sub Category is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/portfolio_subcategory');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/portfolio_subcategory/add');
            }

        } else {
            $data['portfolio_category'] = $this->Model_portfolio_subcategory->getData($id);
            $data['category'] = $this->Model_portfolio_subcategory->getcategory();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_portfolio_subcategory_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        $tot = $this->Model_portfolio_subcategory->portfolio_category_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/portfolio_subcategory');
            exit;
        }


        $result = $this->Model_portfolio_subcategory->getData1($id);
        foreach ($result as $row) {
            $result1 = $this->Model_portfolio_subcategory->show_portfolio_by_id($row['id']);
            foreach ($result1 as $row1) {
                $photo = $row1['photo'];
            }
            if ($photo != '') {
                unlink('./public/uploads/' . $photo);
            }
            $result1 = $this->Model_portfolio_subcategory->show_portfolio_photo_by_portfolio_id($row['id']);
            foreach ($result1 as $row1) {
                $photo = $row1['photo'];
                unlink('./public/uploads/portfolio_photos/' . $photo);
            }

            $this->Model_portfolio_subcategory->delete1($row['id']);
            $this->Model_portfolio_subcategory->delete2($row['id']);
        }
        $this->Model_portfolio_subcategory->delete($id);

        $success = 'Frame sub category is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/portfolio_subcategory');
    }

}