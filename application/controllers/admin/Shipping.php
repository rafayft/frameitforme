<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_shipping');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['shipping'] = $this->Model_shipping->show_active_subscriber();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_shipping', $data);
        $this->load->view('admin/view_footer');
    }


    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $error = '';
        $success = '';

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('initial_amount', 'Initial Amount', 'trim|required');
            $this->form_validation->set_rules('additional_amount', 'Additional Amount', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $next_id = $this->Model_shipping->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }

                $form_data = array(
                    'country' => $_POST['country'],
                    'initial_amount' => $_POST['initial_amount'],
                    'additional_amount' => $_POST['additional_amount'],
                );
                $this->Model_shipping->add($form_data);

                $success = 'Shippment is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/shipping');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/shipping/add');
            }

        } else {

            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_shipping_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {

        $tot = $this->Model_shipping->subscription_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/shipping');
            exit;
        }

        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';


        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('initial_amount', 'Initial Amount', 'trim|required');
            $this->form_validation->set_rules('additional_amount', 'Additional Amount', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }


            if ($valid == 1) {
                $data['why_choose'] = $this->Model_shipping->getData($id);


                $form_data = array(
                    'country' => $_POST['country'],
                    'initial_amount' => $_POST['initial_amount'],
                    'additional_amount' => $_POST['additional_amount'],
                );
                $this->Model_shipping->update($id, $form_data);

                $success = 'Shipping Plan is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/shipping');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/shipping/edit/' . $id);
            }

        } else {
            $data['why_choose'] = $this->Model_shipping->getData($id);
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_shipping_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        $tot = $this->Model_shipping->subscriber_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/shipping');
            exit;
        }

        $this->Model_shipping->delete($id);
        $success = 'Shipping is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/shipping');
    }


}