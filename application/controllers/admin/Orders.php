<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_orders');
        $this->load->model('admin/Model_emailtemplates');
        $this->load->library('zip');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['orders'] = $this->Model_orders->get_order(1);

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_orders', $data);
        $this->load->view('admin/view_footer');
    }


    public function detail()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $id = $this->uri->segment(4);
        $data['orders'] = $this->Model_orders->get_order_single($id);

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_orders_detail', $data);
        $this->load->view('admin/view_footer');
    }


    public function update($id)
    {
        if (isset($_POST['form1'])) {
            $form_data = array(
                'status' => $_POST['status'],
            );
            $orderupdate = $this->Model_orders->update($form_data, array('id' => $id));

            if ($orderupdate) {
                $status = [
                    '1' => 'Not Delivered',
                    '2' => 'Pending',
                    '4' => 'Delivered',
                    '5' => 'Cancel',
                    '6' => 'Hold',
                ];
                $success = 'Order updated successfully';

                $order = $this->Model_orders->get_order_single($id);

                $email = $this->Model_emailtemplates->get('update_order_user');

                $this->load->library('email');
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'frameitforme.com',
                    'smtp_port' => 587,
                    'smtp_user' => 'no-reply@frameitforme.com',
                    'smtp_pass' => 'Ffme#569745',
                    'mailtype'  => 'html',
                    'wordwrap'   => TRUE,
                    'newline'   => '\r\n'
                );
                $this->email->initialize($config);
                $this->email->from('no-reply@frameitforme.com', 'Frame It For Me');
                $this->email->to($order['email']);
                $this->email->subject(str_replace('{order_status}', $status[$form_data['status']], $email['subject']));
                $this->email->message(str_replace('{order_status}', $status[$form_data['status']], $email['body']));
                $this->email->send();

                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/orders/delivered');
            }

        }
    }


    public function delivered()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['orders'] = $this->Model_orders->get_order(4);

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_orders', $data);
        $this->load->view('admin/view_footer');
    }


    public function unpaid()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['orders'] = $this->Model_orders->get_order(2);

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_orders', $data);
        $this->load->view('admin/view_footer');
    }

    public function delete($id)
    {
        $tot = $this->Model_orders->order_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/orders/unpaid');
            exit;
        }
        $this->Model_orders->delete($id);
        $this->Model_orders->delete_order_frame($id);

        $success = 'Order is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/orders/unpaid');
    }

    public function downloadzip()
    {
        $id = $this->input->get('frame_id');
        $frame_images = $this->Model_orders->get_order_frame_images($id);
        $canvasImage = $this->getCanvasImage($id);

        $this->zip->read_file($canvasImage);

        foreach ($frame_images as $frame_image) {
            $filepath2 = FCPATH . '/public/uploads/' . $frame_image['image'];
            // Add file
            $this->zip->read_file($filepath2);
        }

        // Download
        $filename = "{$id}-Order-Frame-Images-" . date('Y-m-d H:i') . ".zip";
        @unlink($canvasImage);

        $this->zip->download($filename);
    }

    /**
     * @param $id
     * @return string
     */
    private function getCanvasImage($id)
    {
        $frame_image = $this->Model_orders->get_order_frame_image($id);

        $img = str_replace(['data:image/jpeg;base64,', 'data:image/png;base64,'], '', $frame_image['image']);
        $ext = false;
        if ($img[0] == '/') {
            $ext = 'jpg';
        }
        if ($img[0] == 'i') {
            $ext = 'png';
        }

        $data = base64_decode($img);
        $canvasImage = FCPATH . "/public/uploads/{$id}-Order-Frame-Canvas-" . time() . "." . $ext;
        file_put_contents($canvasImage, $data);

        return $canvasImage;
    }

}