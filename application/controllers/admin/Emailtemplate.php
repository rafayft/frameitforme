<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailtemplate extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_emailtemplates');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();
        $data['templates'] = $this->Model_emailtemplates->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_email_templates', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        if (isset($_POST['form1'])) {
            if (($error = $this->save()) === true) {
                $success = 'Email template is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/emailtemplate');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/emailtemplate/add');
            }
        } else {
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_email_templates_add', $data);
            $this->load->view('admin/view_footer');
        }
    }

    public function edit($id)
    {
        $template = $this->Model_emailtemplates->check($id);
        if (!$template) {
            redirect(base_url() . 'admin/emailtemplate');
            exit;
        }

        $data['template'] = $template;
        $data['setting'] = $this->Model_common->get_setting_data();

        if (isset($_POST['form1'])) {
            if (($error = $this->save($id)) === true) {
                $success = 'Email template is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/emailtemplate');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/emailtemplate/edit/' . $id);
            }
        } else {
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_email_templates_add', $data);
            $this->load->view('admin/view_footer');
        }
    }

    public function view($id)
    {
        $template = $this->Model_emailtemplates->check($id);
        if (!$template) {
            redirect(base_url() . 'admin/emailtemplate');
            exit;
        }

        $data['template'] = $template;

        $this->load->view('admin/view_email_templates_view', $data);
    }

    /**
     * @param bool $id
     * @return bool|string
     */
    private function save($id = false)
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('body', 'Email Body', 'trim|required');
        $this->form_validation->set_rules('subject', 'Email Subject', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            return validation_errors();
        }

        $form_data = [
            'name' => $_POST['name'],
            'body' => $_POST['body'],
            'subject' => $_POST['subject'],
        ];

        if ($id) {
            $this->Model_emailtemplates->update($id, $form_data);
        } else {
            $this->Model_emailtemplates->add($form_data);
        }
        
        return  true;
    }
}