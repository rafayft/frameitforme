<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frame extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_frame');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['frame'] = $this->Model_frame->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_frame', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $error = '';
        $success = '';

        if (isset($_POST['form1'])) {

            $valid = 1;


            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];

            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }


            if ($valid == 1) {
                $next_id = $this->Model_frame->get_auto_increment_id();
                foreach ($next_id as $row) {
                    $ai_id = $row['Auto_increment'];
                }

                $final_name = 'frame-' . $ai_id . '.' . $ext;
                move_uploaded_file($path_tmp, './public/uploads/' . $final_name);

                $form_data = array(
                    'category_id' => $_POST['category_id'],
                    'photo' => $final_name,
                    'weight' => $_POST['weight'],
                    'price' => $_POST['price'],
                );
                $this->Model_frame->add($form_data);


                $success = 'Frame is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/frame');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/frame/add');
            }
        } else {
            $data['all_photo_category'] = $this->Model_frame->get_all_photo_category();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_frame_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {

        // If there is no service in this id, then redirect
        $tot = $this->Model_frame->frame_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/frame');
            exit;
        }

        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';


        if (isset($_POST['form1'])) {

            $valid = 1;


            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];

            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }


            if ($valid == 1) {
                $data['frame'] = $this->Model_frame->getData($id);

                if ($path == '') {
                    $form_data = array(
                        'category_id' => $_POST['category_id'],
                        'weight' => $_POST['weight'],
                        'price' => $_POST['price'],
                    );
                    $this->Model_frame->update($id, $form_data);
                } else {
                    unlink('./public/uploads/' . $data['frame']['photo']);

                    $final_name = 'frame-' . $id . '.' . $ext;
                    move_uploaded_file($path_tmp, './public/uploads/' . $final_name);

                    $form_data = array(
                        'category_id' => $_POST['category_id'],
                        'photo' => $final_name,
                        'weight' => $_POST['weight'],
                        'price' => $_POST['price'],
                    );
                    $this->Model_frame->update($id, $form_data);
                }


                $success = 'Frame is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/frame');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/frame/edit/' . $id);
            }

        } else {
            $data['frame'] = $this->Model_frame->getData($id);
            $data['all_photo_category'] = $this->Model_frame->get_all_photo_category();
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_frame_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        $tot = $this->Model_frame->frame_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/frame');
            exit;
        }

        $data['frame'] = $this->Model_frame->getData($id);
        if ($data['frame']) {
            unlink('./public/uploads/' . $data['frame']['photo']);
        }


        $this->Model_frame->delete($id);

        $success = 'Frame is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/frame');
    }

    public function single_photo_delete($photo_id = 0, $frame_id = 0)
    {

        $frame_photo = $this->Model_frame->frame_photo_by_id($photo_id);
        unlink('./public/uploads/frame_photos/' . $frame_photo['photo']);

        $this->Model_frame->delete_frame_photo($photo_id);

        redirect(base_url() . 'admin/frame/edit/' . $frame_id);

    }

}