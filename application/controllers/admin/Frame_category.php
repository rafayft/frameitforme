<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frame_category extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Model_common');
        $this->load->model('admin/Model_frame_category');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $data['frame_category'] = $this->Model_frame_category->show();

        $this->load->view('admin/view_header', $data);
        $this->load->view('admin/view_frame_category', $data);
        $this->load->view('admin/view_footer');
    }

    public function add()
    {
        $data['setting'] = $this->Model_common->get_setting_data();

        $error = '';
        $success = '';

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
            $this->form_validation->set_rules('price', 'Price', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            }

            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];

            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            } else {
                $valid = 0;
                $error .= 'You must have to select a photo for featured photo<br>';
            }

            if ($valid == 1) {

                $final_name = 'frame-' . time() . '.' . $ext;
                move_uploaded_file($path_tmp, './public/uploads/' . $final_name);

                $form_data = array(
                    'category_name' => $_POST['category_name'],
                    'price' => $_POST['price'],
                    'photo' => $final_name,
                    'status' => $_POST['status']
                );
                $this->Model_frame_category->add($form_data);

                $success = 'Frame category is added successfully!';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/frame_category');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/frame_category/add');
            }

        } else {

            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_frame_category_add', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function edit($id)
    {
        $tot = $this->Model_frame_category->frame_category_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/frame_category');
            exit;
        }

        $data['setting'] = $this->Model_common->get_setting_data();
        $error = '';
        $success = '';


        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('category_name', 'Category Name', 'trim|required');
            $this->form_validation->set_rules('price', 'Price', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            } else {

                // Duplicate Category Checking
                $data['frame_category'] = $this->Model_frame_category->getData($id);
                $total = $this->Model_frame_category->duplicate_check($_POST['category_name'], $data['frame_category']['category_name']);
                if ($total) {
                    $valid = 0;
                    $error = 'Category name already exists';
                }
            }

            $path = $_FILES['photo']['name'];
            $path_tmp = $_FILES['photo']['tmp_name'];

            if ($path != '') {
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $file_name = basename($path, '.' . $ext);
                $ext_check = $this->Model_common->extension_check_photo($ext);
                if ($ext_check == FALSE) {
                    $valid = 0;
                    $error .= 'You must have to upload jpg, jpeg, gif or png file for featured photo<br>';
                }
            }

            if ($valid == 1) {
                $form_data = array(
                    'category_name' => $_POST['category_name'],
                    'price' => $_POST['price'],
                    'status' => $_POST['status']
                );

                if ($path != '') {
                    unlink('./public/uploads/' . $data['frame_category']['photo']);

                    $final_name = 'frame-' . time() . '.' . $ext;
                    move_uploaded_file($path_tmp, './public/uploads/' . $final_name);
                    $form_data['photo'] = $final_name;
                }

                $this->Model_frame_category->update($id, $form_data);

                $success = 'Frame Category is updated successfully';
                $this->session->set_flashdata('success', $success);
                redirect(base_url() . 'admin/frame_category');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(base_url() . 'admin/frame_category/add');
            }

        } else {
            $data['frame_category'] = $this->Model_frame_category->getData($id);
            $this->load->view('admin/view_header', $data);
            $this->load->view('admin/view_frame_category_edit', $data);
            $this->load->view('admin/view_footer');
        }

    }


    public function delete($id)
    {
        $tot = $this->Model_frame_category->frame_category_check($id);
        if (!$tot) {
            redirect(base_url() . 'admin/frame_category');
            exit;
        }


        $result = $this->Model_frame_category->getData1($id);
        foreach ($result as $row) {
            $result1 = $this->Model_frame_category->show_frame_by_id($row['id']);
            foreach ($result1 as $row1) {
                $photo = $row1['photo'];
            }
            if ($photo != '') {
                unlink('./public/uploads/' . $photo);
            }


            $this->Model_frame_category->delete1($row['id']);
        }
        $this->Model_frame_category->delete($id);

        $success = 'Frame category is deleted successfully';
        $this->session->set_flashdata('success', $success);
        redirect(base_url() . 'admin/frame_category');
    }

}