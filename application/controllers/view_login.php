<section class="login-page" id="loginresult">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                if ($this->session->flashdata('error')) {
                    ?>
                    <div class="label label-danger" style="display:grid">
                        <p><?php echo $this->session->flashdata('error'); ?></p>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('success')) {
                    ?>
                    <div class="label label-success" style="display:grid">
                        <p><?php echo $this->session->flashdata('success'); ?></p>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="login-head">
            <h5>Log In </h5>
            <p>Don't have an account yet? <a href="<?php echo site_url(); ?>/register"> Register here. </a></p>
        </div>

        <div class="login-data">
            <div class="loginresult"></div>
            <form class="loginform">
                <div class="form-field2">
                    <p>Email Address</p>
                    <input type="email" placeholder="johndoe54@gmail.com" name="email">
                </div>


                <div class="form-field2">
                    <p>Password</p>
                    <input type="password" name="password">
                    <a href="<?php echo base_url(); ?>forgotpassword" class="forgot-pass"> Forgot Password?</a>
                </div>


                <div class="form-field2">
                    <input type="button" class="loginbtn" value="LOG IN" name="">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                           value="<?php echo $this->security->get_csrf_hash(); ?>">
                </div>
            </form>
        </div>

    </div>
</section>
<div class="modal"></div>