<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kids_collection extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_portfolio');
        $this->load->model('Model_kids');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['page_portfolio'] = $this->Model_common->all_page_portfolio();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $data['portfolio_subcategory'] = $this->Model_kids->get_portfolio_subcategory();

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_kids_collection');
        $this->load->view('view_footer');
    }

    public function view($id)
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['page_portfolio'] = $this->Model_common->all_page_portfolio();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $data['portfolio_order_by_name'] = $this->Model_portfolio->get_portfolio_data_order_by_name();

        $data['portfolio'] = $this->Model_portfolio->get_portfolio_detail($id);
        $data['portfolio_photo'] = $this->Model_portfolio->get_portfolio_photo($id);
        $data['portfolio_photo_total'] = $this->Model_portfolio->get_portfolio_photo_number($id);

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_kids_details', $data);
        $this->load->view('view_footer');
    }


    public function views($id)
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['page_portfolio'] = $this->Model_common->all_page_portfolio();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $data['portfolio_order_by_name'] = $this->Model_portfolio->get_portfolio_data_order_by_name();

        $data['portfolio'] = $this->Model_portfolio->get_portfolio_detail($id);
        $data['portfolio_photo'] = $this->Model_portfolio->get_portfolio_photo($id);
        $data['portfolio_photo_total'] = $this->Model_portfolio->get_portfolio_photo_number($id);

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_birthdy_details', $data);
        $this->load->view('view_footer');
    }

    public function send_email()
    {

        $data['setting'] = $this->Model_common->all_setting();

        $error = '';

        if (isset($_POST['form_portfolio'])) {

            $valid = 1;

            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
            $this->form_validation->set_rules('phone', 'Phone', 'trim|required');
            $this->form_validation->set_rules('message', 'Message', 'trim|required');
            $this->form_validation->set_error_delimiters('', '<br>');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error .= validation_errors();
            }

            if ($valid == 1) {
                $msg = '
            		<h3>Sender Information</h3>
					<b>Name: </b> ' . $_POST['name'] . '<br><br>
					<b>Phone: </b> ' . $_POST['phone'] . '<br><br>
					<b>Email: </b> ' . $_POST['email'] . '<br><br>
					<b>Portfolio Name: </b> ' . $_POST['portfolio'] . '<br><br>
					<b>Message: </b> ' . $_POST['message'] . '
				';
                $this->load->library('email');

                $this->email->from($data['setting']['send_email_from']);
                $this->email->to($data['setting']['receive_email_to']);

                $this->email->subject('Portfolio Page Email');
                $this->email->message($msg);

                $this->email->set_mailtype("html");

                $this->email->send();

                $success = 'Thank you for sending the email. We will reply you shortly.';
                $this->session->set_flashdata('success', $success);

            } else {
                $this->session->set_flashdata('error', $error);
            }

            redirect($this->agent->referrer());

        } else {

            redirect($this->agent->referrer());
        }
    }


    public function addcart()
    {
        $id = $this->input->post('product_id');
       $total= $this->Model_portfolio->get_portfolio_photo_number($id);
       $size='stnd';
       if($total<=3){
           $size='lrg';
       }
       if($total>3){
           $size='sml';
       }
        $item = array(
            'id' => $id,
            'name' => $this->input->post('name'),
            'photo' => $this->input->post('image'),
            'price' => $this->input->post('price'),
            'quantity' => $this->input->post('quantity'),
            'description' => $this->input->post('description'),
            'size' => $size
        );


        $totalitem = array(
            'shippingamount' => $this->input->post('shippingamount'),
            'promoamount' => '',
            'subtotal' => '',
            'vatamount' => '',
            'weight' => '',
        );

        $tatcart = array($totalitem);
        $this->session->set_userdata('totalcart', $tatcart);

        if (!$this->session->has_userdata('cart')) {
            $cart = array($item);
            $this->session->set_userdata('cart', ($cart));
        } else {
            $index = $this->exists($id);
            $cart = array_values(($this->session->userdata('cart')));
            if ($index == -1) {
                array_push($cart, $item);
                $this->session->set_userdata('cart', ($cart));
            } else {
                $cart[$index]['quantity']++;
                $this->session->set_userdata('cart', ($cart));
            }
        }
        $this->session->set_userdata('cart', ($this->session->userdata('cart')));
        $jsonarray = array(
            'success' => true,
            'Message' => 'Frame Successfully Added',
        );
        echo json_encode($jsonarray);
    }


    public function remove($id)
    {
        //$index = $this->exists($id);
        $cart = array_values(($this->session->userdata('cart')));
        unset($cart[$id]);
        $this->session->set_userdata('cart', ($cart));
        redirect('cart');
    }

    private function exists($id)
    {
        $cart = array_values(($this->session->userdata('cart')));
        for ($i = 0; $i < count($cart); $i++) {
            if ($cart[$i]['id'] == $id) {
                return $i;
            }
        }
        return -1;
    }

    private function total()
    {
        $items = array_values(($this->session->userdata('cart')));
        $s = 0;
        foreach ($items as $item) {
            $s += $item['price'] * $item['quantity'];
        }
        return $s;
    }


    public function getshipping()
    {
        $customer_id = $this->input->post('customer_id');
        $weight = $this->input->post('weight');


        $initialweight = 1;
        $additionalweight = $weight - $initialweight;

        $getcountry = $this->db->get_where('tbl_customer', array('customer_id' => $customer_id));
        if ($getcountry->num_rows() > 0) {
            foreach ($getcountry->result_array() as $customer) {
                $customercountry = $customer['country'];
                if ($customercountry != '') {
                    $getshipping = $this->db->get_where('tbl_shipping', array('country' => $customercountry));
                    if ($getshipping->num_rows() > 0) {
                        foreach ($getshipping->result() as $ship) {
                            $initialweightamount = $ship->initial_amount;
                            $additionalamount = $ship->additional_amount;
                            if ($weight == 1) {
                                $shipamount = $initialweight * $initialweightamount;
                            } else {
                                $initialweightcal = $initialweight * $initialweightamount;
                                $additionalweightcal = $additionalweight * $additionalamount;
                                $shipamount = $initialweightcal + $additionalweightcal;

                            }


                            $resultarray = array(
                                'success' => true,
                                'initialamount' => round($initialweightamount),
                                'additionalamount' => round($additionalamount),
                                'shipamount' => round($shipamount)
                            );
                            echo json_encode($resultarray);
                        }
                    } else {
                        $resultarray = array(
                            'error' => true,
                            'message' => 'No shipping found in your area',
                        );
                        echo json_encode($resultarray);
                    }
                } else {
                    $resultarray = array(
                        'error' => true,
                        'message' => 'Customer Country not set yet',
                    );
                    echo json_encode($resultarray);
                }
            }

        } else {
            $resultarray = array(
                'error' => true,
                'message' => 'Customer Not Found',
            );
            echo json_encode($resultarray);
        }
    }

}
