<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_profile');


    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $data['userdata'] = $this->Model_profile->get_userdata($id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $this->load->view('view_header', $data);
        $this->load->view('view_forgotpassword', $data);
        $this->load->view('view_footer');

    }

    public function forgotpass()
    {
        $id = $this->session->userdata('id');
        $data['userdata'] = $this->Model_profile->get_userdata($id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();

        $this->load->view('view_header', $data);
        $this->load->view('view_updateforgotpassword', $data);
        $this->load->view('view_footer');

    }


    public function update()
    {
        $error = '';
        $success = '';

        //$data['setting'] = $this->Model_common->get_setting_data();

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            }

            if ($valid == 1) {
                if (isset($_POST['password'])) {
                    $form_data = array(
                        'email' => $_POST['email'],
                        'name' => $_POST['name'],
                        'phonenumber' => $_POST['phone'],
                        'city' => $_POST['city'],
                        'addressone' => $_POST['addressone'],
                        'addresstwo' => $_POST['addresstwo'],
                        'country' => $_POST['country'],
                        'password' => md5($_POST['password']),
                    );
                } else {
                    $form_data = array(
                        'email' => $_POST['email'],
                        'name' => $_POST['name'],
                        'phonenumber' => $_POST['phone'],
                        'city' => $_POST['city'],
                        'addressone' => $_POST['addressone'],
                        'addresstwo' => $_POST['addresstwo'],
                        'country' => $_POST['country'],
                    );
                }


                $this->Model_profile->update($form_data, $this->session->userdata('id'));
                $success = 'Profile Information is updated successfully!';

                $this->session->set_userdata($form_data);

                $this->session->set_flashdata('success', $success);
                redirect(site_url() . '/profile');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(site_url() . '/profile');
            }
        }


    }


    public function send()
    {
        $email = $this->input->post('email');


        $this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');


        if ($this->form_validation->run() == FALSE) {
            $jsonarray = array(
                'error' => true,
                'message' => validation_errors(),
            );
            echo json_encode($jsonarray);

        } else {
            ob_start();
            $this->load->library('email');
            $config = array(
                'charset' => 'utf-8',
                'wordwrap' => TRUE,
                'mailtype' => 'html'
            );
            $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'frameitforme.com',
                'smtp_port' => 587,
                'smtp_user' => 'no-reply@frameitforme.com',
                'smtp_pass' => 'Ffme#569745',
                'mailtype'  => 'html',
                'wordwrap'   => TRUE,
                'newline'   => '\r\n'
            );

            $this->email->initialize($config);
            $result = $this->db->get_where('tbl_customer', array('email' => $this->input->post('email')));
            if ($result->num_rows() > 0) {
                foreach ($result->result() as $row) {
                    $this->email->from('no-reply@frameitforme.com', 'Frame It For Me');
                    $this->email->to($this->input->post('email'));


                    $this->email->subject('Forgot Password Email');
                    $this->email->message('<a href="' . site_url('forgotpassword/forgotpass/?link=' . $row->code . '') . '">Update Password</a>');

                    if ($this->email->send()) {
                        $jsonarray = array(
                            'success' => true,
                            'message' => "We have sent you an email.  Please click on the link to reset Your Account",
                        );
                        echo json_encode($jsonarray);


                    } else {
                        $jsonarray = array(
                            'error' => true,
                            'message' => $this->email->print_debugger(),
                        );
                        echo json_encode($jsonarray);

                    }
                }

            } else {
                $jsonarray = array(
                    'error' => true,
                    'message' => "A valid email address must be entered ",
                );
                echo json_encode($jsonarray);
            }


        }
    }


    public function forgot_pass()
    {
        $link = $this->input->post('code');

        $this->load->library('email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[8]');
        $emails = $this->db->get_where('tbl_customer', array('code' => $link))->result();
        $email = $emails[0]->email;

        if ($this->form_validation->run() == FALSE) {
            $jsonarray = array(
                'error' => true,
                'message' => validation_errors(),
            );
            echo json_encode($jsonarray);

        } else {
            $data = array(
                'password' => md5($this->input->post('password')),
            );
            if ($this->db->update('tbl_customer', $data, array('code' => $link))) {

                ob_start();
                $config = array(
                    'charset' => 'utf-8',
                    'wordwrap' => TRUE,
                    'mailtype' => 'html'
                );
                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'frameitforme.com',
                    'smtp_port' => 587,
                    'smtp_user' => 'no-reply@frameitforme.com',
                    'smtp_pass' => 'Ffme#569745',
                    'mailtype'  => 'html',
                    'wordwrap'   => TRUE,
                    'newline'   => '\r\n'
                );
                $this->email->initialize($config);
                $result = $this->db->get_where('tbl_customer', array('email' => $this->input->post('email')))->num_rows();
                if ($result > 0) {
                    $this->email->from('no-reply@frameitforme.com', 'Frame It For Me');
                    $this->email->to($email);


                    $this->email->subject('Password Reset Email');
                    $this->email->message('Your Password has been reset<br>Your new Password is :' . $this->input->post('password'));

                    if ($this->email->send()) {
                        $jsonarray = array(
                            'success' => true,
                            'message' => "Password reset successfully",
                        );
                        echo json_encode($jsonarray);
                    } else {
                        $jsonarray = array(
                            'error' => true,
                            'message' => "Some Thing went wrong",
                        );
                        echo json_encode($jsonarray);
                    }
                } else {
                    $jsonarray = array(
                        'error' => true,
                        'message' => "Some Thing went wrong",
                    );
                    echo json_encode($jsonarray);
                }
            }

        }
    }


}
