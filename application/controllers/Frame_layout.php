<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frame_layout extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        /*if(!$this->session->userdata('logged_in'))
        {
			redirect('login');
		}*/
        $this->load->model('Model_common');
        $this->load->model('Model_home');
        $this->load->model('Model_frame_category');
        $this->load->model('Model_user');
        $this->load->model('Model_order');

    }

    public function index()
    {
        $cart = $this->session->userdata('cart');
        $framecart_cart = $this->session->userdata('framecart');

        if (isset($cart) && !empty($cart) && !empty($framecart_cart)) {
            //redirect('/cart/index');
        }
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['all_news_category'] = $this->Model_common->all_news_category();

        $data['sliders'] = $this->Model_home->all_slider();
        $data['services'] = $this->Model_home->all_service();
        $data['features'] = $this->Model_home->all_feature();
        $data['why_choose'] = $this->Model_home->all_why_choose();
        $data['team_members'] = $this->Model_home->all_team_member();
        $data['testimonials'] = $this->Model_home->all_testimonial();
        $data['clients'] = $this->Model_home->all_client();
        $data['pricing_table'] = $this->Model_home->all_pricing_table();
        $data['home_faq'] = $this->Model_home->all_faq_home();

        $data['frame_category'] = $this->Model_frame_category->show();
        $data['subcription'] = $this->db->get_where('tbl_subscription', array('status' => 1));
        $data['go'] = $this->input->get('go');

        //$data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();
//print_r($this->session->userdata('cart'));exit;
        $this->load->view('view_header', $data);
        $this->load->view('view_framelayout', $data);
        $this->load->view('view_footer', $data);
    }


    public function addcart()
    {
        $id = $this->input->post('id');
        $frameimages = $this->input->post('frameimageall');

        $item = array(
            'id' => $id,
            'framename' => $this->input->post('framename'),
            'frameimage' => $this->input->post('frameimage'),
            'frameamount' => $this->input->post('frameamount'),
            'quantity' =>  $this->input->post('quantity'),
        );

        if ($_POST['subscription'] != '') {
            if (!$this->session->has_userdata('subscriptioncart')) {
                $subscriptionarray = array(
                    'subscriptionval' => $this->input->post('subscriptionval'),
                    'subscription' => $this->input->post('subscription'),
                    'subscriptionprice' => $this->input->post('subscriptionprice'),
                    'subscriptiondesc' => $this->input->post('subscriptiondesc'),
                );
                $subscriptioncart = array($subscriptionarray);
                $this->session->set_userdata('subscriptioncart', $subscriptioncart);

            }

        }

        $totalitem = array(
            'subtotal' => $this->input->post('subtotal'),
            'vatamount' => $this->input->post('vatamount'),
            'totalamount' => $this->input->post('totalamount'),
            'promoamount' => $this->input->post('promocodevalue'),
            'weight' => $this->input->post('weight'),
        );

        $tatcart = array($totalitem);
        $this->session->set_userdata('totalcart', $tatcart);

        if (!$this->session->has_userdata('cart')) {
            $cart = array($item);
            $this->session->set_userdata('cart', $cart);
        } else {

            $index = $this->exists($id);
            $cart = array_values($this->session->userdata('cart'));

            if ($index == -1) {
                array_push($cart, $item);
                $this->session->set_userdata('cart', $cart);
            } else {
                $cart[$index]['quantity']++;
                $this->session->set_userdata('cart', $cart);
            }
        }
        //print_r($this->session->userdata('cart'));
        echo json_encode(array('success' => true));

    }

    public function proceedcart()
    {
       // print_r($this->input->post());

        $id = $this->input->post('id');
        $frameimages = $this->input->post('frameimageall');

        $item = array(
            'id' => $id,
            'name' => $this->input->post('famrname'),
            'photo' => $this->input->post('fameimage'),
            'frameamount' => $this->input->post('frameamount'),
            'quantity' => $this->input->post('quantity'),
            'image' => $frameimages,
            'price' => $this->input->post('frameamount'),
        );
      /*  print_r($this->input->post());
        die();*/

        $total = array_sum(array_values($this->input->post('frameamount')))*$this->input->post('quantity');

        $totalitem = array(
            'subtotal' => $total,
            'vatamount' => $this->input->post('vatamount'),
            'totalamount' => $total,
            'promoamount' => $this->input->post('promocodevalue'),
            'weight' => $this->input->post('weight'),
            'shippingamount' => $this->input->post('shippingamount'),
        );

        $tatcart = array($totalitem);
        $this->session->set_userdata('totalcart', $tatcart);
        $this->session->set_userdata('framecart', array('id' => 1));

        if (!$this->session->has_userdata('cart')) {
            $cart = array($item);
            $this->session->set_userdata('cart', $cart);
        } else {

            $index = $this->exists($id);
            $cart = array_values($this->session->userdata('cart'));

            if ($index === -1) {
                array_push($cart, $item);
                $this->session->set_userdata('cart', $cart);
            } else {
                $cart[$index]['quantity']++;
                $this->session->set_userdata('cart', $cart);
            }
        }
        //print_r($this->session->userdata('cart'));
        echo json_encode(array('success' => true));

    }

    private function exists($id)
    {
        $cart = array_values($this->session->userdata('cart'));
        for ($i = 0; $i < count($cart); $i++) {
            if ($cart[$i]['id'][0] == $id) {
                return $i;
            }
        }
        return -1;
    }

    function delete()
    {
        header('Content-Type: application/json');

        $this->updateTotal();
        $cart = $this->session->userdata('cart');
        $id = $_GET['id'];
        $index = $this->exists($id);

        if($index !== -1) {
            unset($cart[$index]);

            $this->session->unset_userdata('framecart');
            $this->session->set_userdata('cart', $cart);

            $this->updateTotal();
        }

        if(empty($this->session->userdata('cart'))) {
            $this->session->unset_userdata('cart');
            $this->session->unset_userdata('totalcart');
        }

        exit(json_encode(array('success' => true)));
    }

    private function updateTotal()
    {
        $totalcart = $this->session->userdata('totalcart');
        $total = 0;

        array_map(function ($row) use (&$total) {
            $total += array_sum($row);
        }, array_column($this->session->userdata('cart'), 'frameamount'));

        $totalcart[0]['subtotal'] = $total;
        $totalcart[0]['totalamount'] = $total;

        $this->session->set_userdata('totalcart', $totalcart);
    }



    public function getframes()
    {
        $id = $this->input->post('id');

        $page['frames'] = $this->db->get_where('tbl_frame', array('category_id' => $id));;
        $this->load->view('view_frames', $page);


    }

    public function getframeslayout()
    {
        $id = $this->input->post('id');

        $page['frame'] = $this->db->get_where('tbl_frame', array('id' => $id));
        $this->load->view('view_frameslayoutdetail', $page);


    }


    public function getshipping()
    {
        $customer_id = $this->input->post('customer_id');
        $weight = $this->input->post('weight');


        $initialweight = 1;
        $additionalweight = $weight - $initialweight;

        $getcountry = $this->db->get_where('tbl_customer', array('customer_id' => $customer_id));
        if ($getcountry->num_rows() > 0) {
            foreach ($getcountry->result_array() as $customer) {
                $customercountry = $customer['country'];
                if ($customercountry != '') {
                    $getshipping = $this->db->get_where('tbl_shipping', array('country' => $customercountry));
                    if ($getshipping->num_rows() > 0) {
                        foreach ($getshipping->result() as $ship) {
                            $initialweightamount = $ship->initial_amount;
                            $additionalamount = $ship->additional_amount;
                            if ($weight == 1) {
                                $shipamount = $initialweight * $initialweightamount;
                            } else {
                                $initialweightcal = $initialweight * $initialweightamount;
                                $additionalweightcal = $additionalweight * $additionalamount;
                                $shipamount = $initialweightcal + $additionalweightcal;

                            }


                            $resultarray = array(
                                'success' => true,
                                'initialamount' => round($initialweightamount),
                                'additionalamount' => round($additionalamount),
                                'shipamount' => round($shipamount)
                            );
                            echo json_encode($resultarray);
                        }
                    } else {
                        $resultarray = array(
                            'error' => true,
                            'message' => 'No shipping found in your area',
                        );
                        echo json_encode($resultarray);
                    }
                } else {
                    $resultarray = array(
                        'error' => true,
                        'message' => 'Customer Country not set yet',
                    );
                    echo json_encode($resultarray);
                }
            }

        } else {
            $resultarray = array(
                'error' => true,
                'message' => 'Customer Not Found',
            );
            echo json_encode($resultarray);
        }
    }


    function savedraft()
    {
        $order_frame = array(
            'customer_id' => $this->session->userdata('id'),
            'installationservice' => $this->input->post('installationservice'),
            'subtotal' => $this->input->post('subtotal'),
            'discount' => $this->input->post('promocode'),
            'vat' => $this->input->post('vatamount'),
            'totalweight' => $this->input->post('weightselected'),
            'totalamount' => $this->input->post('totalamount'),
            'status' => 3,
            'shippingamount' => $this->input->post('shippingamount'),
        );
        $addorder = $this->Model_order->add_order($order_frame);
        if ($addorder) {
            $frameid = $this->input->post('id');
            $frameimage = $this->input->post('fameimage');
            $frameprice = $this->input->post('frameamount');
            $framename = $this->input->post('famrname');
            for ($i = 0; $i < count($frameid); $i++) {
                $frame_data = array(
                    'order_id' => $addorder,
                    'customer_id' => $this->session->userdata('id'),
                    'name' => $framename[$i],
                    'price' => $frameprice[$i],
                    'image' => $frameimage[$i],
                    'desciption' => '-',
                    'status' => 3,

                );

                if($frame_data['name'] == '' || (int)$frame_data['price'] < 1) {
                    continue;
                }

                $addframe = $this->Model_order->add_order_frames($frame_data);
            }


            $subscription = $this->input->post('subscription');
            $subscriptionprice = $this->input->post('subscriptionprice');
            $subscriptiondesc = $this->input->post('subscriptiondesc');
            $subscriptionid = $this->input->post('subscriptionid');

            if (isset($subscription)) {
                $subcription_data = array(
                    'order_id' => $addorder,
                    'customer_id' => $this->session->userdata('id'),
                    'name' => $subscription,
                    'price' => $subscriptionprice,
                    'status' => 3,
                    'subscriptiondesc' => $subscriptiondesc,
                );
                $addsubs = $this->Model_order->add_order_subscription($subcription_data);
            }

            $this->session->unset_userdata('cart');
            $this->session->unset_userdata('totalcart');
            $this->session->unset_userdata('subscriptioncart');

            $json_array = array(
                'success' => true,
                'message' => 'Frame Added In Draft',
            );
            echo json_encode($json_array);

        } else {
            $json_array = array(
                'error' => true,
                'message' => 'Some Thing went wrong',
            );
            echo json_encode($json_array);
        }
    }


    function proceed()
    {
       /* print_r($this->session->userdata('quantity'));
        die();*/
        $order_frame = array(
            'customer_id' => $this->session->userdata('id'),
            'installationservice' => $this->input->post('installationservice')[0],
            'subtotal' => $this->input->post('totalamount'),
            'discount' => $this->input->post('promocode'),
            'vat' => $this->input->post('vatamount'),
            'totalweight' => $this->input->post('weightselected'),
            'totalamount' => $this->input->post('totalamount'),
            'status' => 2,
            'shippingamount' => $this->input->post('shippingamount'),
        );
        $addorder = $this->Model_order->add_order($order_frame);

        if ($addorder) {
            $frameid = $this->input->post('id');
            $frameimage = $this->input->post('fameimage');
            $frameprice = $this->input->post('frameamount');
            $framename = $this->input->post('famrname');
            $frameimageall = $this->input->post('frameimageall');
            $quantities = $this->input->post('quantities');
            for ($i = 0; $i < count($frameid); $i++) {
                $frame_data = array(
                    'order_id' => $addorder,
                    'customer_id' => $this->session->userdata('id'),
                    'name' => $framename[$i],
                    'price' => $frameprice[$i],
                    'image' => $frameimage[$i],
                    'quantity' => $quantities,
                    'desciption' => '-',
                    'status' => 2,

                );

                if($frame_data['name'] == '' || (int)$frame_data['price'] < 1) {
                    continue;
                }

                $addframe = $this->Model_order->add_order_frames($frame_data);
                for ($f = 0; $f < count($frameimageall); $f++) {
                    $framedata = array(
                        'frame_id' => $addframe,
                        'image' => $frameimageall[$f],
                    );
                    $this->db->insert('tbl_order_frame_images', $framedata);
                }
            }


            $subscription = $this->input->post('subscription');
            $subscriptionprice = $this->input->post('subscriptionprice');
            $subscriptiondesc = $this->input->post('subscriptiondesc');
            $subscriptionid = $this->input->post('subscriptionid');

            if (isset($subscription)) {
                $subcription_data = array(
                    'order_id' => $addorder,
                    'customer_id' => $this->session->userdata('id'),
                    'name' => $subscription,
                    'price' => $subscriptionprice,
                    'status' => 2,
                    'subscriptiondesc' => $subscriptiondesc,
                );
                $addsubs = $this->Model_order->add_order_subscription($subcription_data);
            }

            $this->session->unset_userdata('cart');
            $this->session->unset_userdata('totalcart');
            $this->session->unset_userdata('subscriptioncart');

            $json_array = array(
                'success' => true,
                'message' => 'Frame Added In Draft',
                'order_id' => $addorder,
            );
            echo json_encode($json_array);

        } else {
            $json_array = array(
                'error' => true,
                'message' => 'Some Thing went wrong',
            );
            echo json_encode($json_array);
        }
    }


    public function saveframeimage()
    {
        if ($_POST) {

            define('UPLOAD_DIR', './public/uploads/');

            $img = $_POST['imageframedata'];
            $img = str_replace(['data:image/jpeg;base64,', 'data:image/png;base64,'], '', $img);
            $ext = false;

            if($img[0] == '/') {
                $ext = 'jpg';
            }

            if($img[0] == 'i') {
                $ext = 'png';
            }

            // Check only jpg and png image
            if (!$ext) {
                $responce = array(
                    'success' => false,
                );
                echo json_encode($responce);

                return;
            }

            $data = base64_decode($img);
            $filename = uniqid() . time() . '.' .$ext;
            $file = UPLOAD_DIR . $filename;
            $success = file_put_contents($file, $data);

            $responce = array(
                'success' => true,
                'image' => $filename,
            );
            echo json_encode($responce);
        }

    }


}
