<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_profile');
        $this->load->model('Model_order');
        if (!$this->session->userdata('logged_in')) {
            redirect(site_url() . '/login');
        }

    }

    public function index()
    {
        $id = $this->session->userdata('id');
        $data['userdata'] = $this->Model_profile->get_userdata($id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['saved_orders'] = $this->Model_order->get_draft_order();
        $data['orders'] = $this->Model_order->get_order();
        $data['tab'] = $this->input->get('tab', '');

        $this->load->view('view_header', $data);
        $this->load->view('view_profile', $data);
        $this->load->view('view_footer');

    }

    public function update()
    {
        $error = '';
        $success = '';

        //$data['setting'] = $this->Model_common->get_setting_data();

        if (isset($_POST['form1'])) {

            $valid = 1;

            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');

            if ($this->form_validation->run() == FALSE) {
                $valid = 0;
                $error = validation_errors();
            }

            if ($valid == 1) {
                if (isset($_POST['password'])) {
                    $form_data = array(
                        'email' => $_POST['email'],
                        'name' => $_POST['name'],
                        'phonenumber' => $_POST['phone'],
                        'city' => $_POST['city'] ?? '',
                        'addressone' => $_POST['addressone'],
                        'addresstwo' => $_POST['addresstwo'],
                        'country' => $_POST['country'],
                        'password' => md5($_POST['password']),
                    );
                } else {
                    $form_data = array(
                        'email' => $_POST['email'],
                        'name' => $_POST['name'],
                        'phonenumber' => $_POST['phone'],
                        'city' => $_POST['city'] ?? '',
                        'addressone' => $_POST['addressone'],
                        'addresstwo' => $_POST['addresstwo'],
                        'country' => $_POST['country'],
                    );
                }


                $this->Model_profile->update($form_data, $this->session->userdata('id'));
                $success = 'Profile Information is updated successfully!';

                $this->session->set_userdata($form_data);

                $this->session->set_flashdata('success', $success);
                redirect(site_url() . '/profile');
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(site_url() . '/profile');
            }
        }


    }


    public function order()
    {
        $id = $this->session->userdata('id');
        $order_id = $this->uri->segment(3);
        $data['userdata'] = $this->Model_profile->get_userdata($id);
        $data['setting'] = $this->Model_common->all_setting();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['orders'] = $this->Model_order->getsingleorder(array('id' => $order_id));

        $this->load->view('view_header', $data);
        $this->load->view('view_order', $data);
        $this->load->view('view_footer');
    }

}
