<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Model_common');
        $this->load->model('Model_home');
        $this->load->model('Model_portfolio');
        $this->load->model('Model_user');
        $this->load->model('Model_emailtemplates');
    }

    public function index()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['all_news_category'] = $this->Model_common->all_news_category();

        $data['sliders'] = $this->Model_home->all_slider();
        $data['services'] = $this->Model_home->all_service();
        $data['features'] = $this->Model_home->all_feature();
        $data['why_choose'] = $this->Model_home->all_why_choose();
        $data['team_members'] = $this->Model_home->all_team_member();
        $data['testimonials'] = $this->Model_home->all_testimonial();
        $data['clients'] = $this->Model_home->all_client();
        $data['pricing_table'] = $this->Model_home->all_pricing_table();
        $data['home_faq'] = $this->Model_home->all_faq_home();

        $data['portfolio_category'] = $this->Model_portfolio->get_portfolio_category();
        $data['portfolio'] = $this->Model_portfolio->get_portfolio_data();

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('view_register', $data);
        $this->load->view('view_footer', $data);
    }


    public function save()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required|matches[passconf]|min_length[6]');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE) {
            $json = array(
                'success' => false,
                'message' => $this->form_validation->error_array()
            );
            echo json_encode($json);
        } else {
            $checkemail = $this->Model_user->check_email($this->input->post('email'));
            if ($checkemail->num_rows() > 0) {
                $json = array(
                    'success' => false,
                    'message' => ['email'=>'Email Already exists'],
                );
                echo json_encode($json);
            } else {
                $code = rand(1000, 9999);
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),
                    'status' => 0,
                    'code' => $code,
                    'country' => $this->input->post('country'),
                );
                $inserdata = $this->Model_user->add_customer($data);
                if ($inserdata > 0) {
                    $config = Array(
                        'protocol' => 'smtp',
                        'smtp_host' => 'frameitforme.com',
                        'smtp_port' => 587,
                        'smtp_user' => 'no-reply@frameitforme.com',
                        'smtp_pass' => 'Ffme#569745',
                        'mailtype'  => 'html',
                        'wordwrap'   => TRUE,
                        'newline'   => '\r\n'
                    );

                    $this->load->library('email');


                    $this->email->initialize($config);
                    $this->email->from('no-reply@frameitforme.com', 'Frame It For Me');
                    $this->email->to($this->input->post('email'));
                    $this->email->subject('Frame It For Me | Confirmation Email');
                    $messages = '
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" style="font-family:Open Sans,Arial,Helvetica,sans-serif;border-collapse:collapse!important;background:#fff!important;border-collapse:collapse;background:#fff">
  <tbody>
    <tr>
      <td align="center" valign="top" bgcolor="#fff" style="background:#fff!important;background:#fff">
        <img src="' . base_url() . 'public/uploads/logo.jpg">
      </td>
    </tr>
    <tr>
      <td align="center" valign="top" bgcolor="#ededed">
        <table width="700" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
          <tbody>
            <tr>
              <td align="center" valign="top">
                <table width="600" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                  <tbody>
                    <tr>
                      <td height="20" align="center" valign="top" style="height:20px!important;line-height:10px!important;font-size:20px!important;color:#ededed!important;height:10px;line-height:20px;font-size:20px;color:#ededed">.</td>
                    </tr>
                    <tr>
                      <td align="center" valign="top">
                        <table width="600" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td align="center" valign="top">
                                <table width="600" class="m_-8985627821087644018mobilewrapper" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                                  <tbody></tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td align="center" valign="top">
                        <table width="800" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                          <tbody>
                            <tr>
                              <td align="center" valign="top" bgcolor="#ffffff" style="background:#ffffff">
                                <table width="600" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                                  <tbody>
                                    <tr>
                                      <td width="" align="center" valign="top">&nbsp;</td>
                                      <td align="center" valign="top">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      
                                      <td align="center" valign="top" style="font-size:24px;font-family:Open Sans,Arial,Helvetica,sans-serif;color:#1c2c3a;text-align:center" colspan="2">Verify Your account click on this <a href="' . base_url() . '/login/active/?code=' . $code . '">link</a></td>
                                      
                                    </tr>
                                    <tr>
                                      <td align="center" valign="top">&nbsp;</td>
                                      <td  align="center" valign="top">&nbsp;</td>
                                    </tr>
                                   
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td align="center" valign="top" bgcolor="#ffffff" style="border-radius:0 0 2px 2px">
                                <table width="600" class="m_-8985627821087644018mobilewrapper" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse">
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table> ';
                    $this->email->message($messages);

                    $this->email->send();
                    //$this->response($message, 200);
                    $json = array(
                        'success' => true,
                        'message' => 'You have successfully register kindly login your account',
                    );
                    echo json_encode($json);
                } else {
                    $json = array(
                        'success' => false,
                        'message' => 'Some thig went wroong',
                    );
                    echo json_encode($json);

                }
            }

        }
    }


    function thankyou()
    {
        $data['setting'] = $this->Model_common->all_setting();
        $data['page_home'] = $this->Model_common->all_page_home();
        $data['comment'] = $this->Model_common->all_comment();
        $data['social'] = $this->Model_common->all_social();
        $data['all_news'] = $this->Model_common->all_news();
        $data['all_news_category'] = $this->Model_common->all_news_category();

        $data['sliders'] = $this->Model_home->all_slider();
        $data['services'] = $this->Model_home->all_service();
        $data['features'] = $this->Model_home->all_feature();
        $data['why_choose'] = $this->Model_home->all_why_choose();
        $data['team_members'] = $this->Model_home->all_team_member();
        $data['testimonials'] = $this->Model_home->all_testimonial();
        $data['clients'] = $this->Model_home->all_client();
        $data['pricing_table'] = $this->Model_home->all_pricing_table();
        $data['home_faq'] = $this->Model_home->all_faq_home();

        $data['portfolio_category'] = $this->Model_portfolio->get_portfolio_category();
        $data['portfolio'] = $this->Model_portfolio->get_portfolio_data();

        $data['portfolio_footer'] = $this->Model_portfolio->get_portfolio_data();

        $this->load->view('view_header', $data);
        $this->load->view('registrationthankyou', $data);
        $this->load->view('view_footer', $data);
    }

}
