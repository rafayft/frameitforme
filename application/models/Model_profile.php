<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_profile extends CI_Model
{

    function update($data, $id)
    {
        $this->db->where('customer_id', $id);
        $this->db->update('tbl_customer', $data);
    }


    public function get_userdata($id)
    {
        $query = $this->db->get_where('tbl_customer', array('customer_id' => $id));
        return $query;
    }

}