<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_orders extends CI_Model
{


    public function get_order($status)
    {
        $this->db->select('tbl_order.*,tbl_customer.name AS customer_name');
        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.customer_id', 'left');
        $data = array(

            'tbl_order.status' => $status,
            'tbl_order.paymentstatus' => 1,
        );
        $query = $this->db->get_where('tbl_order', $data);

        return $query->result_array();
    }


    public function get_order_single($id)
    {
        $this->db->select('tbl_order.*,tbl_customer.name AS customer_name,tbl_customer.email AS email,tbl_customer.phonenumber AS phonenumber');
        $this->db->join('tbl_customer', 'tbl_customer.customer_id = tbl_order.customer_id', 'left');
        $data = array(
            'tbl_order.id' => $id,
        );
        $query = $this->db->get_where('tbl_order', $data);
        return $query->first_row('array');
    }


    function order_check($id)
    {
        $sql = 'SELECT * FROM tbl_order WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }


    public function update($data, $id)
    {
        $update = $this->db->update('tbl_order', $data, $id);
        return $update;
    }


    function delete_order_frame($id)
    {
        $this->db->where('order_id', $id);
        $this->db->delete('tbl_order_frame');
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_order');
    }

    function get_order_frame_images($id)
    {
        $sql = 'SELECT * FROM tbl_order_frame_images WHERE frame_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->result_array('array');
    }

    function get_order_frame_image($id)
    {
        $sql = 'SELECT image FROM tbl_order_frame WHERE orderframe_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}
