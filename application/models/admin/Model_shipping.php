<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_shipping extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_shipping'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function add($data)
    {
        $this->db->insert('tbl_shipping', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_shipping', $data);
    }

    function subscription_check($id)
    {
        $sql = 'SELECT * FROM tbl_shipping WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_shipping WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }


    function show_active_subscriber()
    {
        $sql = "SELECT * FROM tbl_shipping";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function delete_pending_subscriber()
    {
        $this->db->where('subs_active', 0);
        $this->db->delete('tbl_shipping');
    }

    function delete($id)
    {
        $this->db->where('subs_id', $id);
        $this->db->delete('tbl_shipping');
    }

    function subscriber_check($id)
    {
        $sql = 'SELECT * FROM tbl_shipping WHERE subs_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}