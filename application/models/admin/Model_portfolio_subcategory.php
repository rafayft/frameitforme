<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_portfolio_subcategory extends CI_Model
{

    function show()
    {
        $sql = "SELECT * 
				FROM tbl_portfolio_subcategory t1
				JOIN tbl_portfolio_category t2
				ON t1.category_id = t2.category_id
                ORDER BY t1.subcategory_id ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function getcategory()
    {
        $sql = "SELECT * FROM tbl_portfolio_category";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function show_portfolio_by_id($id)
    {
        $sql = "SELECT * FROM tbl_portfolio WHERE subcategory_id=?";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
    }

    function show_portfolio_photo_by_portfolio_id($id)
    {
        $sql = "SELECT * FROM tbl_portfolio_photo WHERE portfolio_id=?";
        $query = $this->db->query($sql, $id);
        return $query->result_array();
    }

    function add($data)
    {
        $this->db->insert('tbl_portfolio_subcategory', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('subcategory_id', $id);
        $this->db->update('tbl_portfolio_subcategory', $data);
    }

    function delete($id)
    {
        $this->db->where('subcategory_id', $id);
        $this->db->delete('tbl_portfolio_subcategory');
    }

    function delete1($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_portfolio');
    }

    function delete2($id)
    {
        $this->db->where('portfolio_id', $id);
        $this->db->delete('tbl_portfolio_photo');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_portfolio_subcategory WHERE subcategory_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function getData1($id)
    {
        $sql = 'SELECT * FROM tbl_portfolio WHERE category_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->result_array();
    }

    function portfolio_category_check($id)
    {
        $sql = 'SELECT * FROM tbl_portfolio_subcategory WHERE subcategory_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function duplicate_check($var1, $var2)
    {
        $sql = 'SELECT * FROM tbl_portfolio_subcategory WHERE subcategoryname=? and subcategoryname!=?';
        $query = $this->db->query($sql, array($var1, $var2));
        return $query->num_rows();
    }

}