<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_coupon extends CI_Model
{

    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_coupon'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function show()
    {
        $sql = "SELECT * FROM tbl_coupon";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function add($data)
    {
        $this->db->insert('tbl_coupon', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('coupon_id', $id);
        $this->db->update('tbl_coupon', $data);
    }

    function delete($id)
    {
        $this->db->where('coupon_id', $id);
        $this->db->delete('tbl_coupon');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_coupon WHERE coupon_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function partner_check($id)
    {
        $sql = 'SELECT * FROM tbl_coupon WHERE coupon_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

}