<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_emailtemplates extends CI_Model
{

    function show()
    {
        $sql = "SELECT * FROM tbl_email_templates";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function add($data)
    {
        $this->db->insert('tbl_email_templates', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_email_templates', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('tbl_email_templates');
    }

    function getData($name)
    {
        $sql = 'SELECT * FROM tbl_email_templates WHERE name=?';
        $query = $this->db->query($sql, array($name));
        return $query->first_row('array');
    }

    function check($id)
    {
        $sql = 'SELECT * FROM tbl_email_templates WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function get($name)
    {
        $sql = 'SELECT * FROM tbl_email_templates WHERE name=?';
        $query = $this->db->query($sql, array($name));
        return $query->first_row('array');
    }
}