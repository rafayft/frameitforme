<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_subscription extends CI_Model
{
    function get_auto_increment_id()
    {
        $sql = "SHOW TABLE STATUS LIKE 'tbl_subscription'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


    function add($data)
    {
        $this->db->insert('tbl_subscription', $data);
        return $this->db->insert_id();
    }

    function update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_subscription', $data);
    }

    function subscription_check($id)
    {
        $sql = 'SELECT * FROM tbl_subscription WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }

    function getData($id)
    {
        $sql = 'SELECT * FROM tbl_subscription WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }


    function show_active_subscriber()
    {
        $sql = "SELECT * FROM tbl_subscription";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function delete_pending_subscriber()
    {
        $this->db->where('subs_active', 0);
        $this->db->delete('tbl_subscription');
    }

    function delete($id)
    {
        $this->db->where('subs_id', $id);
        $this->db->delete('tbl_subscription');
    }

    function subscriber_check($id)
    {
        $sql = 'SELECT * FROM tbl_subscription WHERE subs_id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}