<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_payments extends CI_Model
{
    public function add_payment($data)
    {
        $query = $this->db->insert('tbl_payments', $data);
        return $this->db->insert_id();
    }
}