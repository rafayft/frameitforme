<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_frame_category extends CI_Model
{

    function show()
    {
        $sql = "SELECT * FROM tbl_frame_category where status = 'Active' ORDER BY updated_on ASC";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


}