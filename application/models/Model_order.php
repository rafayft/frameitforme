<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_order extends CI_Model
{
    public function add_order($data)
    {
        $query = $this->db->insert('tbl_order', $data);
        return $this->db->insert_id();
    }

    public function add_order_frames($data)
    {
        $query = $this->db->insert('tbl_order_frame', $data);
        return $this->db->insert_id();
    }

    public function add_order_subscription($data)
    {
        $query = $this->db->insert('tbl_order_subscription', $data);
        return $this->db->insert_id();
    }

    public function update_order($data, $id)
    {
        $update = $this->db->update('tbl_order', $data, $id);
        return $update;
    }

    public function update_frameorder($data, $id)
    {
        $update = $this->db->update('tbl_order_frame', $data, $id);
        return $update;
    }

    public function update_subscriptionorder($data, $id)
    {
        $update = $this->db->update('tbl_order_subscription', $data, $id);
        return $update;
    }


    public function get_draft_order()
    {
        $data = array(
            'customer_id' => $this->session->userdata('id'),
            'paymentstatus' => 0,
        );
        $query = $this->db->get_where('tbl_order', $data);
        return $query;
    }

    public function get_order()
    {
        $data = array(
            'customer_id' => $this->session->userdata('id'),
            'status' => 1,
            'paymentstatus' => 1,
        );
        $query = $this->db->get_where('tbl_order', $data);
        return $query;
    }


    public function getsingleorder($data)
    {
        $query = $this->db->get_where('tbl_order', $data);
        return $query;
    }


    public function deletedraft($id)
    {
        $delete = $this->db->delete('tbl_order', array('id' => $id));
        $delete = $this->db->delete('tbl_order_frame', array('order_id' => $id));
        $delete = $this->db->delete('tbl_order_subscription', array('order_id' => $id));
        return $delete;
    }

    public function get_order_by_ref($ref)
    {
        $data = array(
            'order_ref' => $ref,
        );
        $query = $this->db->get_where('tbl_order', $data);

        return $query->row_array();
    }

    public function get_order_frames($id)
    {
        $data = array(
            'order_id' => $id,
        );
        $query = $this->db->get_where('tbl_order_frame', $data);

        return $query->result_array();
    }
}