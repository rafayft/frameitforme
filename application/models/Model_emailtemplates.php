<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_emailtemplates extends CI_Model
{
    function get($name)
    {
        $sql = 'SELECT * FROM tbl_email_templates WHERE name=?';
        $query = $this->db->query($sql, array($name));
        return $query->first_row('array');
    }

    function check($id)
    {
        $sql = 'SELECT * FROM tbl_email_templates WHERE id=?';
        $query = $this->db->query($sql, array($id));
        return $query->first_row('array');
    }
}