<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model
{

    function check_email($email)
    {
        $where = array(
            'email' => $email
        );
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }


    function checkstatus($email)
    {
        $where = array(
            'email' => $email,
            'status' => 0,
        );
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }

    function check_password($email, $password)
    {
        $where = array(
            'email' => $email,
            'password' => md5($password)
        );
        $this->db->select('*');
        $this->db->from('tbl_customer');
        $this->db->where($where);
        $query = $this->db->get();
        return $query;
    }


    function add_customer($data)
    {
        $insert = $this->db->insert('tbl_customer', $data);
        return $this->db->insert_id();
    }


    public function updatestatus($data, $id)
    {

        $update = $this->db->update('tbl_customer', $data, $id);
        return $update;
    }

}