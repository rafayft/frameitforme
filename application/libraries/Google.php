<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google
{
    public function __construct()
    {
        require_once APPPATH . 'third_party/Google/Google_Client.php';
        require_once APPPATH . 'third_party/Google/contrib/Google_Oauth2Service.php';
    }
}