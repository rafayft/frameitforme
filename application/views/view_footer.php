<!-- Footer Starts Here -->
<!-- Footer Starts Here -->
<footer>
    <div class="container">
        <div class="footer-top">
            <div class="row">

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <div class="footer-list1">
                        <h5>OUR ADDRESS </h5>

                        <p>
                            <i class="fa fa-home"> </i>
                            Fortune Executive Tower, Office 1507, JLT, POBox 414477, Dubai, UAE
                        </p>

                        <p>
                            <a href="tel:+9714 425 9844"> <i class="fa fa-phone"> </i> +971 4 425 9844 </a>
                        </p>

                        <p>
                            <a href="mailto:info@frameitforme.com"> <i class="fa fa-envelope"> </i>
                                info@frameitforme.com </a>
                        </p>

                        <p>
                            <a href="/"> <i class="fa fa-globe"> </i> www.frameitforme.com </a>
                        </p>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <div class="footer-list1">
                        <h5>INFORMATION</h5>
                        <h6>
                            <a href="<?php echo base_url(); ?>howitwork"> How it Works </a>
                        </h6>

                        <h6>
                            <a href="<?php echo base_url(); ?>terms"> Terms & Conditions </a>
                        </h6>

                        <h6>
                            <a href="<?php echo base_url(); ?>privacy"> Privacy Policy </a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>shipment"> Shipping & Handling </a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>faq"> FAQs </a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>contact"> Contact us </a>
                        </h6>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <div class="footer-list1">
                        <h5>QUICKLINKS</h5>
                        <h6>
                            <a href="<?php echo base_url(); ?>register"> Get Started</a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>shop"> Shop</a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>kids_collection"> Kids</a>
                        </h6>
                        <h6>
                            <a href="<?php echo base_url(); ?>subscribe"> Subscribe </a>
                        </h6>

                    </div>
                </div>


                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
                    <div class="footer-list1">
                        <h5>PAYMENT OPTIONS</h5>
                        <ul>
                            <li>
                                <a href=""> <img src="<?php echo base_url(); ?>images/payment7.png"> </a>
                            </li>
                        </ul>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <div class="footer-form">
        <h6>Keep us close by following us on
            <?php
            foreach ($social as $row) {
                if ($row['social_url'] != '') {
                    echo '<a href="' . $row['social_url'] . '"  data-placement="bottom">' . $row['social_name'] . '</a>';
                }
            }
            ?></h6>
    </div>
    <div class="footer-anchors">
        <p>  <?php echo $setting['footer_copyright']; ?> </p>
    </div>
    </div>
</footer>


<!-- Footer Scripts -->
<script src="<?php echo base_url(); ?>js/slick-slider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.7.6/fabric.min.js" type="text/javascript"></script>
<link href="http://code.jquery.com/ui/1.12.1/themes/blitzer/jquery-ui.css" rel="stylesheet"
      type="text/css"/>
<script type="text/javascript">
    var base_url = '<?php echo site_url(); ?>/'
    var status = '<?php echo $this->input->get('status'); ?>'
</script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/functions.js"></script>
<script src="<?php echo base_url(); ?>js/fileupload.js"></script>
<script src="<?php echo base_url(); ?>js/script.js"></script>
<script src="<?php echo base_url(); ?>js/dom-to-image.js"></script>
<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
<script type="text/javascript">

    $('.white').click(function () {
        $('#background').addClass('white_border');
        $('.layer').addClass('white_border_shadow');
    })

    $('.black').click(function () {
        $('#background').removeClass('white_border');
        $('.layer').removeClass('white_border_shadow');
    })

    $('.brown').click(function () {
        $('#background').removeClass('white_border');
        $('.layer').removeClass('white_border_shadow');
    })

    $('.grey').click(function () {
        $('#background').removeClass('white_border');
        $('.layer').removeClass('white_border_shadow');
    })

    jQuery('.registerbtn').click(function (e) {
        e.preventDefault();
        var form = jQuery('.registerform').serialize();
        $body = $("body");
        $body.addClass("loading");
        jQuery.ajax({
            url: base_url + 'registration',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    //jQuery('.registerresult').html('<div class="label label-success">'+data.message+'</div>');
                    //$body.removeClass("loading"); 
                    jQuery('.registerform')[0].reset();
                    setTimeout(function () {
                        window.location = '<?php echo base_url(); ?>register/thankyou'
                    }, 1000)
                } else {

                    $('html,body').animate({
                        scrollTop: $("#registerresult").offset().top - 10
                    });
                    $body.removeClass("loading");
                    if(data.message.email){
                        jQuery('#emailreq').html('<p>' + data.message.email + '</p>');
                    }else {
                        jQuery('#emailreq').html('<p></p>');
                    }
                    if(data.message.name){
                        jQuery('#namereq').html('<p>' + data.message.name + '</p>');
                    }else {
                        jQuery('#namereq').html('<p></p>');
                    }
                    if(data.message.password){
                        jQuery('#passwordreq').html('<p>' + data.message.password + '</p>');
                    }else {
                        jQuery('#passwordreq').html('<p></p>');
                    }
                    if(data.message.passconf){
                        jQuery('#repasswordreq').html('<p>' + data.message.passconf + '</p>');
                    }else {
                        jQuery('#repasswordreq').html('<p></p>');
                    }
                }

            }, error: function () {
                alert('failure')
            }
        })
    })


    jQuery('.loginform').on('submit', function (e) {
        e.preventDefault();

        $body = $("body");
        $body.addClass("loading");
        var form = jQuery('.loginform').serialize();

        jQuery.ajax({
            url: base_url + 'logins',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    jQuery('.loginresult').html('<div class="label label-success">' + data.message + '</div>');
                    $('html,body').animate({
                        scrollTop: $("#loginresult").offset().top - 10
                    });
                    $body.removeClass("loading");
                    $('.loginform').hide();
                    jQuery('.loginform')[0].reset();
                    setTimeout(function () {
                        if(status=="checkout"){
                            window.location = base_url + 'Cart/index'
                        }else{
                            window.location = base_url + 'profile?tab=list_oforder'
                        }
                    }, 1000)
                } else {
                    $('html,body').animate({
                        scrollTop: $("#loginresult").offset().top - 10
                    });
                    $body.removeClass("loading");
                    jQuery('.loginresult').html('<p>' + data.message + '</p>');
                }

            }, error: function () {
                alert('failure')
            }
        });

        return false;
    });

    jQuery('.forgotpassbtn').click(function () {
        $body = $("body");
        $body.addClass("loading");
        var form = jQuery('.forgotpasswordform').serialize();

        jQuery.ajax({
            url: base_url + 'Forgotpassword/send',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    jQuery('.forgotpasswordresult').html('<div class="label label-success">' + data.message + '</div>');
                    /*$('html,body').animate({
                       scrollTop: $(".forgotpasswordresult").offset().top - 10 
                    });*/
                    $body.removeClass("loading");
                    $('.forgotpasswordform').hide();
                    jQuery('.forgotpasswordform')[0].reset();
                    //setTimeout(function(){ window.location = base_url+'profile'},1000)
                } else {
                    /*$('html,body').animate({
                       scrollTop: $(".forgotpasswordresult").offset().top - 10 
                    });*/
                    $body.removeClass("loading");
                    jQuery('.forgotpasswordresult').html('<div class="label label-danger">' + data.message + '</div>');
                }

            }, error: function () {
                alert('failure')
            }
        })
    })

    jQuery('.updateforgotpassbtn').click(function () {
        $body = $("body");
        $body.addClass("loading");
        var form = jQuery('.updateforgotpasswordform').serialize();

        jQuery.ajax({
            url: base_url + 'Forgotpassword/forgot_pass',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    jQuery('.updateforgotpasswordresult').html('<div class="label label-success">' + data.message + '</div>');
                    $('html,body').animate({
                        scrollTop: $("#loginresult").offset().top - 10
                    });
                    $body.removeClass("loading");
                    $('.updateforgotpasswordform').hide();
                    jQuery('.updateforgotpasswordform')[0].reset();
                    //setTimeout(function(){ window.location = base_url+'profile'},1000)
                } else {
                    $('html,body').animate({
                        scrollTop: $("#loginresult").offset().top - 10
                    });
                    $body.removeClass("loading");
                    jQuery('.updateforgotpasswordresult').html('<div class="label label-danger">' + data.message + '</div>');
                }

            }, error: function () {
                alert('failure')
            }
        })
    })
</script>

<script type="text/javascript">

    $(function () {


        $("#dvSource canvas").draggable({
            revert: "invalid",
            refreshPositions: true,
            containment: '#photo', // Limit the area of dragging

            drag: function (event, ui) {

                ui.helper.addClass("draggable");
            },
            stop: function (event, ui) {
                ui.helper.removeClass("draggable");

            }
        });
        $(".dropped").draggable({containment: "#photo canvas", scroll: false})
        $("#photo").droppable({
            drop: function (event, ui) {

                if ($("#photo canvas").length == 0) {
                    $("#photo").html("");

                }
                console.log(event);
                ui.draggable.addClass("dropped");


                //ui.draggable.removeClass("ui-draggable ui-draggable-handle");
                //console.log(ui.draggable);
                $("#photo").append(ui.draggable);
                $('.canvas-container').each(function (index) {

                    var numItems = $('.canvas-container canvas').length;

                    var number = numItems - 1;
                    var canvasContainer = $(this)[number];
                    var canvasObject = $("canvas", this)[number];

                    /*var height = $this.height();
                    console.log(width);
                    var cntrLeft = width / 5 - ui.draggable.width() / 5;
                    var cntrTop = height / 5 - ui.draggable.height() / 5;
                    console.log(cntrLeft);
                    ui.draggable.css({
                        left: cntrLeft + "px",
                        top: cntrTop + "px",
                        marginleft : "40px"
                    });*/
                    var url = $(this).data('floorplan');
                    var canvas = canvasObject;
                    registerEvents(canvas);
                });
            }
        });
    });

    function registerEventss(canvas) {

        canvas.ondragenter = function () {
            canvas.style.border = "dashed 2px #555";  // Change the canvas borders when hovering

        };
        canvas.ondragleave = function () {
            canvas.style.border = "none";    // Reset canvas borders when hovering is not active
        };
        canvas.ondragover = function (e) {
            e.preventDefault();
        };
        canvas.ondrop = function (e) {

            e.preventDefault();
            var id = e.dataTransfer.getData("text");
            console.log(id);
            var dropImage = document.getElementById(id);
            //console.log(dropImage)
            canvas.style.border = "none";              // Reset canvas borders after image drop

            var context = canvas.getContext("2d");
            //console.log(context);
            context.drawImage(dropImage, 0, 0, canvas.width, canvas.height);     // Draw and stretch image to fill canvas
        };
    }

    function initCanvas() {
        $('.frame-selections').each(function (index) {

            var canvasContainer = $(this)[0];
            var canvasObject = $("canvas", this)[0];
            console.log(canvasObject);
            var url = $(this).data('floorplan');
            var canvas = window._canvas = new fabric.Canvas(canvasObject);

            canvas.setHeight(200);
            canvas.setWidth(500);
            canvas.setBackgroundImage(url, canvas.renderAll.bind(canvas));

            var imageOffsetX, imageOffsetY;

            function handleDragStart(e) {
                [].forEach.call(images, function (img) {
                    img.classList.remove('img_dragging');
                });
                this.classList.add('img_dragging');


                var imageOffset = $(this).offset();
                imageOffsetX = e.clientX - imageOffset.left;
                imageOffsetY = e.clientY - imageOffset.top;
            }

            function handleDragOver(e) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                e.dataTransfer.dropEffect = 'copy';
                return false;
            }

            function handleDragEnter(e) {
                this.classList.add('over');
            }

            function handleDragLeave(e) {
                this.classList.remove('over');
            }

            function handleDrop(e) {
                e = e || window.event;
                if (e.preventDefault) {
                    e.preventDefault();
                }
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
                var img = document.querySelector('img.img_dragging');
                console.log('event: ', e);

                //canvas.style.border = "none";              // Reset canvas borders after image drop

                var context = canvas.getContext("2d");
                console.log(context);
                context.drawImage(img, 0, 0, canvas.width, canvas.height);     // Draw and stretch image to fill canvas
                return false;
            }

            function handleDragEnd(e) {
                [].forEach.call(images, function (img) {
                    img.classList.remove('img_dragging');
                });
            }

            var images = document.querySelectorAll('img');
            [].forEach.call(images, function (img) {
                img.addEventListener('dragstart', handleDragStart, false);
                img.addEventListener('dragend', handleDragEnd, false);
            });
            canvasContainer.addEventListener('dragenter', handleDragEnter, false);
            canvasContainer.addEventListener('dragover', handleDragOver, false);
            canvasContainer.addEventListener('dragleave', handleDragLeave, false);
            canvasContainer.addEventListener('drop', handleDrop, false);
        });
    }


    $('.frames-selection').on('click', '.frame-wrap', function () {
        $('.frames-selection .frame-wrap').removeClass('active');
        $(this).addClass('active');
    });

    $('.nextbtn').click(function () {
        $(".next-previous").show();
        previous_bt = $('.previousbtn')
        //finish_bt 		= $('.fin_bt')
        next_bt = $('.nextbtn')
        preview_btn = $('.previousbtn');
        if ($('.step1').is(":visible")) {
            var id = $('.frames-selection .frame-wrap.active').attr('data-ids');
            var title = $('.frames-selection .frame-wrap.active').attr('data-title');
            if(isNaN(parseInt(id))) {
                alert('Please select frame.');
                return false;
            }

            $('#dLabel').attr('data-ids', id).val(title);
            $('#dLabel span').text(title);

            var csrf_test_name = $('.csrf_test_name').val();
            $.ajax({
                type: "POST",
                url: base_url + 'Frame_layout/getframes',
                data: {id: id, csrf_test_name: csrf_test_name},
                cache: false,
                success: function (data) {
                    $('.layoutdynamic').html(data);
                }
            });
            current_fs = $('.step1');
            next_fs = $('.step2');
            $('div.process-line').css('width', '30%')

            previous_bt.show();


        } else if ($('.step2').is(":visible")) {

            var layoutselect = $('.layoutseelction').val();
            var csrf_test_name = $('.csrf_test_name').val();
            let id = $('.layoutdynamic.frames-selection .frame-wrap.active .layout-select').attr('data-ids');
            let name = $('.layoutdynamic.frames-selection .frame-wrap.active .text-wrap h4').text();

            if(isNaN(parseInt(id))) {
                alert('Please select frame.');
                return false;
            }
            $(".next-previous").hide();
            $(".topback").show();
            $('div.process-line').css('width', '45%');

            if(parseVal($('.subframeid').val()) !== parseVal(id)) {
                var csrf_test_name = $('.csrf_test_name').val();
                $.ajax({
                    type: "POST",
                    url: base_url + 'Frame_layout/getframeslayout',
                    data: {id: id, csrf_test_name: csrf_test_name},
                    cache: false,
                    success: function (data) {
                        $('.layout-details').html(data);
                    }
                });
            }

            $('.subframeid').val(id);
            $('.selected-frame-name').text(name);

            current_fs = $('.step2');
            if (layoutselect != 'Customize') {
                next_fs = $('.step3');
            } else {
                next_fs = $('.step8');
            }
            //previous_bt.show();
            var customer_id = '<?php echo $this->session->userdata("id"); ?>';
            var weight = $("input[name='layout']:checked").attr('data-weight');

            $.ajax({
                type: "POST",
                url: base_url + 'Frame_layout/getshipping',
                data: {customer_id: customer_id, csrf_test_name: csrf_test_name, weight: weight},
                cache: false,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        $('.initialamount').val(data.initialamount);
                        $('.additionalamount').val(data.additionalamount);
                    } else {
                        $('.initialamount').val(0);
                        $('.additionalamount').val(0);
                    }
                }
            });
        } else if ($('.step3').is(":visible")) {

            let count = true;
            $('input.img-name').each(function(){
                if($(this).val() == ''){
                    count = false;
                    return false;
                }
            });

            $("#checkoutbtn").show();

            var preview=$("#preview").val();
            if(preview=="1"){
                count = true;
                $("#checkoutbtn").hide();
            }

            countdropimage = $('.droppedimage').length;
            countcanvas = $('#photo .layer').length;
            /*jQuery('#photo .layer').each(function(index, currentElement) {*/
            if (count) {
                $('div.process-line').css('width', '57%');
                $('.nextbtn').hide();
                $('.previousbtn').hide();

                current_fs = $('.step3');
                next_fs = $('.step4');
                $("#preview").val(2);
            } else {
                alert('Please Add All Images in Frame');
            }

            /*});*/


        } else if ($('.step4').is(":visible")) {

            $('div.process-line').css('width', '72%');
            current_fs = $('.step4');
            next_fs = $('.step6');

        } else if ($('.step8').is(":visible")) {
            $('div.process-line').css('width', '57%');
            current_fs = $('.step8');
            next_fs = $('.step3');
        }

        next_fs.show();
        current_fs.hide();

        //$('.secondstepside').hide();
        //$('.imagevolom').show();
    })


    $('.checkoutbuton').click(function () {

        previous_bt = $('.previousbtn');
        //finish_bt 		= $('.fin_bt')
        next_bt = $('.nextbtn');
        preview_btn = $('.previousbtn');

        /*if($('.step4').is(":visible"))
        {
            
            $('div.process-line').css('width','82%');
                current_fs = $('.step4');
                next_fs = $('.step5');
        
        }else*/
        if ($('.step4').is(":visible")) {
            $('div.process-line').css('width', '82%');
            var image = $('.previewimage').attr('src');

            $('.subscriptionamount').html('');
            $('.backline2').hide('');
            var image = $('.previewimage').attr('src');
            var layoutid = $('.layoutseelction').attr('data-ids');
            var layoutseelction = $('.selected-frame-name').text();

            var radio = $('.layoutdynamic.frames-selection .frame-wrap.active .layout-select');

            var radioValue = radio.attr('data-amount');
            var radioValueweight = radio.attr('data-weight');
            $('.weightselected').val(parseVal(radioValueweight) + parseVal($('.weightselected').val()));
            var installation = 75;
            var vatamount = 0;

            var weightcatt = $('.weightselectedcart').val();
            var initialamountship = $('.initialamount').val();
            var additonalamountship = $('.additionalamount').val();


            var weight = $('.weightselected').val();
            if (weight > 1) {
                var initialamount = 1 * initialamountship;
                var additonalamount = (weight - 1) * additonalamountship;
                var shippingamount = parseVal(additonalamount) + parseVal(initialamount);
                $('.shippingamount').text(shippingamount);

            } else {
                var shippingamount = initialamountship * weight;
                $('.shippingamount').text(shippingamount);
            }


            $('.frameimage').attr('src', image);
            $('.fameimages').val(image);
            $('.famrname').val(layoutseelction);
            $('.framename').text(layoutseelction);
            $('.layoutid').val(layoutid);
            $('.removerow').attr('data-ids', layoutid);
            $('.totalframeamount').text(radioValue);
            $('.frameamount').val(radioValue);


            var subtotal = $('.subtotalss').val();

            var subtotalss = parseVal(radioValue) + parseVal(installation) + parseVal(subtotal);
            //alert(subtotalss+'-'+vatamount+'-'+shippingamount);
            $('.subtotal').text(subtotalss);
            $('.subtotalss').val(subtotalss);
            $('.totalframeamountfinal').text(parseVal(subtotalss) + parseVal(vatamount) + parseVal(shippingamount));
            $('.totalamount').val(parseVal(subtotalss) + parseVal(vatamount) + parseVal(shippingamount));


            current_fs = $('.step4');
            next_fs = $('.step6');

            $body = $("body");
            $body.addClass("loading");

            var form = $('.frameform').serialize();
            jQuery.ajax({
                url: base_url + 'Frame_layout/proceedcart',
                data: form,
                type: 'POST',
                dataType: 'Json',
                success: function (data) {
                    if (data.success) {

                        $body.removeClass("loading");
                        window.location = '<?php echo site_url(); ?>/frame_layout';
                    } else {
                        $body.removeClass("loading");
                        //jQuery('.loginresult').html('<div class="label label-danger"><p>'+data.message+'</p></div>');
                    }

                }, error: function () {
                    alert('failure');
                }
            });


        }

        next_fs.show();
        current_fs.hide();
    });


    $('.skippackage').click(function () {

        previous_bt = $('.previousbtn')
        //finish_bt 		= $('.fin_bt')
        next_bt = $('.nextbtn')
        preview_btn = $('.previousbtn')

        if ($('.step5').is(":visible")) {
            $('.subscriptionamount').html('');
            $('.backline2').hide('');
            var image = $('.previewimage').attr('src');
            var layoutid = $('.layoutseelction').attr('data-ids');
            var layoutseelction = $('.layoutseelction').val();

            var radioValue = $("input[name='layout']:checked").attr('data-ids');
            var radioValueweight = $("input[name='layout']:checked").attr('data-weight');
            $('.weightselected').val(parseVal(radioValueweight) + parseVal($('.weightselected').val()));
            var installation =0;
            var vatamount = 0;

            var weightcatt = $('.weightselectedcart').val();
            var initialamountship = $('.initialamount').val();
            var additonalamountship = $('.additionalamount').val();


            var weight = $('.weightselected').val();
            if (weight > 1) {
                var initialamount = 1 * initialamountship;
                var additonalamount = (weight - 1) * additonalamountship;
                var shippingamount = parseVal(additonalamount) + parseVal(initialamount);
                $('.shippingamount').text(shippingamount);

            } else {
                var shippingamount = initialamountship * weight;
                $('.shippingamount').text(shippingamount);
            }


            $('.frameimage').attr('src', image);
            $('.fameimages').val(image);
            $('.famrname').val(layoutseelction);
            $('.framename').text(layoutseelction);
            $('.layoutid').val(layoutid);
            $('.totalframeamount').text(radioValue);
            $('.frameamount').val(radioValue);


            var subtotal = $('.subtotalss').val();

            var subtotalss = parseVal(radioValue) + parseVal(installation) + parseVal(subtotal);
            //alert(subtotalss+'-'+vatamount+'-'+shippingamount);
            $('.subtotal').text(subtotalss);
            $('.subtotalss').val(subtotalss);
            $('.totalframeamountfinal').text(parseVal(subtotalss) + parseVal(vatamount) + parseVal(shippingamount));
            $('.totalamount').val(parseVal(subtotalss) + parseVal(vatamount) + parseVal(shippingamount));
            if (radioValue > 0) {
                $('.framedesctiption').text('-');
            }

            current_fs = $('.step5');
            next_fs = $('.step6');

        }

        next_fs.show();
        current_fs.hide();
    })


    $('.previousicon').click(function () {

        previous_bt = $('.previousbtn')
        //finish_bt 		= $('.fin_bt')
        next_bt = $('.nextbtn')
        preview_btn = $('.previousbtn');
        if ($('.step4').is(":visible")) {

            current_fs = $('.step4');
            next_fs = $('.step3');
            $('div.process-line').css('width', '57%')
            var canvas = document.getElementById("background");

            $('.nextbtn').show();
            $('.previousbtn').show();


        }

        next_fs.show();
        current_fs.hide();
    });
    $('.previousbtn').click(function () {

        $(".next-previous").show();
        $(".topback").hide();
        $(".next-buttons").show();

        previous_bt = $('.previousbtn')
        //finish_bt 		= $('.fin_bt')
        next_bt = $('.nextbtn')
        preview_btn = $('.previousbtn')
        if ($('.step2').is(":visible")) {

            current_fs = $('.step2');
            next_fs = $('.step1');
            $('div.process-line').css('width', '28%');

            previous_bt.hide();


        } else if ($('.step3').is(":visible")) {
            $('div.process-line').css('width', '44%')
            current_fs = $('.step3');
            next_fs = $('.step2');
            $('.layer').remove()
            //previous_bt.show();
            //modelSelect();
        } else if ($('.step4').is(":visible")) {

            $('div.process-line').css('width', '57%')
            $('.nextbtn').hide();

            $('.previousbtn').hide();

            current_fs = $('.step4');
            next_fs = $('.step3');


        }

        next_fs.show();
        current_fs.hide();

        //$('.secondstepside').hide();
        //$('.imagevolom').show();
    })

    //initCanvas();



    function registerEvents(canvas) {
        //console.log(canvas);
        canvas.ondragenter = function () {
            //canvas.style.border = "dashed 2px #555";  // Change the canvas borders when hovering
        };
        canvas.ondragleave = function () {
            //canvas.style.border = "none";    // Reset canvas borders when hovering is not active
        };
        canvas.ondragover = function (e) {
            e.preventDefault();
        };
        canvas.ondrop = function (e) {

            e.preventDefault();
            var id = e.dataTransfer.getData("text");
            //console.log(id);
            var dropImage = document.getElementById(id);
            var imagedrop = dropImage.getAttribute("src");
            //canvas.style.border = "none";              // Reset canvas borders after image drop

            var context = canvas.getContext("2d");
            //console.log(context);
            canvas.classList.add('droppedimage');
            var csrf_test_name = jQuery('.csrf_test_name').val();


            context.drawImage(dropImage, 0, 0, canvas.width, canvas.height);     // Draw and stretch image to fill canvas
            $('#background').addClass('loader');
            jQuery.ajax({
                type: "POST",
                url: base_url + 'Frame_layout/saveframeimage',
                dataType: 'json',
                data: {
                    imageframedata: imagedrop,
                    csrf_test_name: csrf_test_name
                }, success: function (data) {
                    if (data.success) {
                        $('#background').removeClass('loader');
                        var imageinput = '<input type="hidden" class="imageboxframe" name="frameimageall[]" id="' + id + '" value="' + data.image + '"></input>';

                        $('.frameimagesall').append(imageinput)
                    }

                }
            });


        };
    }


    $('.clickcolor').click(function () {
        var color = $(this).data('ids');
        /*if(color == 'white')
        {
            $('#photo .layer').css('box-shadow','inset 0px 0px 10px rgba(0,0,0,0.5)');
        }else
        {
            $('#photo .layer').css('box-shadow','none');
        }*/
        $('#photo .layer').css('border-color', color);
    })

    $(".dropdown-menu").on('click', 'li a', function () {
        var value = $.trim($(this).text())
        var dataids = $.trim($(this).attr('data-ids'))
        $('.layoutseelction .buttontext').text(value);
        $('.layoutseelction').val(value);
        $('.layoutseelction').attr('data-ids', dataids);
    });


    function customRadio(radioName) {
        var radioButton = $('input[name="' + radioName +'[]"]');
        $(radioButton).each(function () {
            $(this).wrap("<span class='custom-radio'></span>");
            if ($(this).is(':checked')) {
                $(this).parent().addClass("selected");
            } else {
                $(this).parent().removeClass("selected");
            }

        });

        $(radioButton).click(function () {

            if ($(this).is(':checked')) {
                $(this).parent().addClass("selected");

                var subtotal = $('.subtotal .subtotal').text();
                var subtotalss = $('.subtotalss').text();
                var totalamount = $('.totalframeamountfinal').text();
                var installationamount = $(".installationamount").val();
                $('.installtion').val(parseVal(installationamount));


                var subtotalamout = parseVal(totalamount) + parseVal(installationamount)
                $('.subtotal').text(subtotalamout);
                $('.subtotalss').val(subtotalamout);
                $('.totalframeamountfinal').text(parseVal(totalamount) + parseVal(installationamount));
                $('.totalamount').val(subtotalamout);
            } else {
                $(this).parent().removeClass("selected");
                var totalamount = $('.totalframeamountfinal').text();
                var subtotal = $('.subtotal').text();
                var subtotalss = $('.subtotalss').text();
                var installationamount = $(".installationamount").val();
                var subtotalamout = parseVal(totalamount) - parseVal(installationamount);
                $('.subtotal').text(subtotalamout);
                $('.subtotalss').val(subtotalamout);
                $('.totalamount').val(subtotalamout);
                $('.totalframeamountfinal').text(subtotalamout)
                $('.installtion').val(parseVal(0));
            }
            $(radioButton).not(this).each(function () {
               // $(this).parent().removeClass("selected");
            });

        });
        $(radioButton).trigger("click");
    }

    // Calling customRadio function
    $(document).ready(function () {
        customRadio("installationservice");
    })


    /**
     * ********************* Create customize range
     */




    $('.continueshopping').click(function () {

        window.location = '<?php echo site_url(); ?>/frame_layout?go=continue';

        return false;

        var frameimage = $('.fameimages').val();
        var framename = $('.famrname').val();
        var frameamount = $('.frameamount').val();
        var subscriptionval = $('.subscriptionprice').val();
        var subscription = $('.subscriptiontitle').text();
        var subscriptionprice = $('.subscriptionprice').val();
        var subtotal = $('.subtotalss').val();
        var vatamount = $('.vatamounts').val();
        var totalamount = $('.totalamount').val();
        var csrf_test_name = $('.csrf_test_name').val();
        var lauoutid = $('.layoutid').val();
        var promocodevalue = $('.promocodevalue').val();
        var weight = $('.weightselected').val();
        //var frameimageall = $('.imageboxframe').val();


        $.ajax({
            type: 'POST',
            data: {
                frameimage: frameimage,
                framename: framename,
                frameamount: frameamount,
                subscription: subscription,
                subscriptionprice: subscriptionprice,
                subtotal: subtotal,
                vatamount: vatamount,
                totalamount: totalamount,
                csrf_test_name: csrf_test_name,
                id: lauoutid,
                promocodevalue: promocodevalue,
                weight: weight
            },
            dataType: 'json',
            url: '<?php echo site_url(); ?>/frame_layout/addcart',
            success: function (data) {
                if (data.success) {
                    window.location = '<?php echo site_url(); ?>/frame_layout';
                }
            }, error: function () {
                alert('some thing went wrong');
            }
        })

    })
    $('.framecolorkids').click(function () {
        var colorcode = $(this).data('ids');

        $('.black-frame img').css('border-color', colorcode);
    })
    $('.frame-det-color span').on('click', function () {
        let colorcode = $(this).css("background-color");
        $('.black-frame img').css('border-color', colorcode);
        $(".color-name").hide();
        $(this).next().show();
    });

    $('.framecolor').click(function () {
        var colorcode = $(this).data('ids');
        $('.canvasimage').css('border-color', colorcode);
    })


    $('.themecolor').click(function () {
        var colorcode = $(this).data('ids');
        $('.canvasbox').css('background-color', colorcode);
    })
    $('.backgroundpattern').click(function () {
        var image = $(this).attr('src');
        $('.canvasimage').css('background-image', 'url(' + image + ')');
    })


    $('.addtocartkids').click(function () {
        var container = document.getElementById("convertedfrmekid");
        ; // full page 
        html2canvas(container, {allowTaint: true}).then(function (canvas) {

            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = "html_image.png";
            image = canvas.toDataURL("image/jpg");
            link.target = '_blank';
            link.click();
            //$('.imagedisplay').attr('src',image);
            $('.convertedimage').val(image);
        });

        $('.addtocartkids').text('Processing...');
        setTimeout(function () {
            var convertedimage = $('.canvasconrtedimage').val();
            if (convertedimage != 'undefined') {
                var form = $('.checkoutnewform').serialize();
                jQuery.ajax({
                    url: base_url + 'Kids_collection/addcart',
                    data: form,
                    type: 'POST',
                    dataType: 'Json',
                    success: function (data) {
                        if (data.success) {
                            jQuery('.registerresult').html('<div class="label label-success">' + data.message + '</div>');
                            $('.addtocartkids').text('Processing...')
                            $('.addtocartkids').attr('disabled')
                            //$body.removeClass("loading"); 
                            //jQuery('.registerform')[0].reset()	;
                            setTimeout(function () {
                                window.location = '<?php echo base_url(); ?>Cart/index'
                            }, 1000)
                        } else {
                            $('html,body').animate({
                                scrollTop: $("#registerresult").offset().top - 10
                            });
                            $body.removeClass("loading");
                            jQuery('.registerresult').html('<div class="label label-danger">' + data.message + '</div>');
                        }

                    }, error: function () {
                        alert('failure')
                    }
                })
            }

        }, 4000)

    });

    $('.addtocart').click(function () {


        var container = document.getElementById("silver-frame-detail"); // full page
        html2canvas(container, {allowTaint: true}).then(function (canvas) {

            var link = document.createElement("a");
            document.body.appendChild(link);
            link.download = "html_image.png";
            image = canvas.toDataURL("image/jpg");
            link.target = '_blank';
            link.click();
            //$('.imagedisplay').attr('src',image);
            $('.convertedimage').val(image);
        });

        var convertedimage = $('.canvasconrtedimage').val();

        var titlecanvas = $('.titlecanvas').val();
        if (titlecanvas != '') {
            $('.addtocart').text('Process...');
            setTimeout(function () {

                if (convertedimage != 'undefined') {
                    var form = $('.checkoutnewform').serialize();
                    jQuery.ajax({
                        url: base_url + 'Kids_collection/addcart',
                        data: form,
                        type: 'POST',
                        dataType: 'Json',
                        success: function (data) {
                            if (data.success) {
                                jQuery('.registerresult').html('<div class="label label-success">' + data.message + '</div>');
                                $('.addtocart').text('Process...')
                                $('.addtocart').attr('disabled')
                                //$body.removeClass("loading"); 
                                //jQuery('.registerform')[0].reset()	;
                                setTimeout(function () {
                                    window.location = '<?php echo base_url(); ?>Cart/index'
                                }, 1000)
                            } else {
                                $('html,body').animate({
                                    scrollTop: $("#registerresult").offset().top - 10
                                });
                                $body.removeClass("loading");
                                jQuery('.registerresult').html('<div class="label label-danger">' + data.message + '</div>');
                            }

                        }, error: function () {
                            alert('failure')
                        }
                    })
                }

            }, 5000)
        } else {
            $('.titlecanvas').css('border-color', 'red');
        }


    })

    /**
     * ********************** get promo code*******************
     */


    $('.couponcode').click(function () {

        var couponcode = $('.promocode').val();
        var csrf_test_name = $('.csrf_test_name').val();
        if (couponcode == '') {
            jQuery('.couponresult').html('<div class="label label-danger">Please Enter Promo Code</div>');
        } else {
            jQuery('.couponcode').attr('disabled');
            jQuery('.couponcode').val('Please Wait');
            jQuery.ajax({
                url: base_url + 'Cart/getcoupon',
                data: {couponcode: couponcode, csrf_test_name: csrf_test_name},
                type: 'POST',
                dataType: 'Json',
                success: function (data) {
                    if (data.success) {
                        jQuery('.couponcode').hide();
                        jQuery('.couponcode').removeAttr('disabled');
                        jQuery('.couponcode').val('APPLY');
                        jQuery('.promocodebox').show();
                        var totalamount = $('.totalamount').val();
                        $('.codeamountplus').text(data.amount);
                        $('.promocodevalue').val(data.amount);
                        $('.totalamount').val(totalamount - data.amount);
                        $('.totalframeamountfinal').text(totalamount - data.amount);
                        $('.totalamountnew').val(totalamount - data.amount);
                        jQuery('.couponresult').html('<div class="label label-success">' + data.message + '</div>');
                    } else {
                        jQuery('.couponcode').removeAttr('disabled');
                        jQuery('.couponcode').val('APPLY');

                        jQuery('.couponresult').html('<div class="label label-danger">' + data.message + '</div>');
                    }

                }, error: function () {
                    alert('failure')
                }
            })
        }

    })

    $('.couponcodetwo').click(function () {

        var couponcode = $('.promocode').val();
        var csrf_test_name = $('.csrf_test_name').val();
        if (couponcode == '') {
            jQuery('.couponresult').html('<div class="label label-danger">Please Enter Promo Code</div>');
        } else {
            jQuery('.couponcodetwo').attr('disabled');
            jQuery('.couponcodetwo').val('Please Wait');
            jQuery.ajax({
                url: base_url + 'Cart/getcoupon',
                data: {couponcode: couponcode, csrf_test_name: csrf_test_name},
                type: 'POST',
                dataType: 'Json',
                success: function (data) {
                    if (data.success) {
                        jQuery('.couponcodetwo').hide();
                        jQuery('.couponcodetwo').val('APPLIED');
                        jQuery('.promocodebox').show();
                        var totalamount = $('.totalframeamountfinal').text();
                        $('.codeamountplus').text(data.amount);
                        $('.promocodevalue').val(data.amount);
                        $('.totalframeamountfinal').text(totalamount - data.amount);
                        $('.totalamount').val(totalamount - data.amount);
                        jQuery('.couponresult').html('<div class="label label-success">' + data.message + '</div>');
                    } else {
                        jQuery('.couponcodetwo').removeAttr('disabled');
                        jQuery('.couponcodetwo').val('APPLY');

                        jQuery('.couponresult').html('<div class="label label-danger">' + data.message + '</div>');
                    }

                }, error: function () {
                    alert('failure')
                }
            })
        }

    })


    $('.savedraft').click(function () {
        <?php
        if (!$this->session->userdata('logged_in')) {
            echo "window.location = base_url + 'login';";
            echo "return false;";
        }
        ?>
        $body = $("body");
        $body.addClass("loading");
        var form = $('.frameform').serialize();
        jQuery.ajax({
            url: base_url + 'Frame_layout/savedraft',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    $body.removeClass("loading");
                    setTimeout(function () {
                        window.location = base_url + 'profile'
                    }, 1000)
                } else {
                    $body.removeClass("loading");
                    //jQuery('.loginresult').html('<div class="label label-danger"><p>'+data.message+'</p></div>');
                }

            }, error: function () {
                alert('failure')
            }
        })
    });

    $('.proceed').click(function () {
        <?php
        if (!$this->session->userdata('logged_in')) {
            echo "window.location = base_url + 'login?status=checkout';";
            echo "return false;";
        }
        ?>
        $body = $("body");
        $body.addClass("loading");
        var form = $('.frameform').serialize();
        jQuery.ajax({
            url: base_url + 'Frame_layout/proceed',
            data: form,
            type: 'POST',
            dataType: 'Json',
            success: function (data) {
                if (data.success) {
                    $body.removeClass("loading");
                    setTimeout(function () {
                        window.location = base_url + 'cart/checkout/' + data.order_id
                    }, 1000)
                } else {
                    $body.removeClass("loading");
                    //jQuery('.loginresult').html('<div class="label label-danger"><p>'+data.message+'</p></div>');
                }

            }, error: function () {
                alert('failure')
            }
        })
    })

    $('.shippingselection').click(function () {
        if ($(this).is(':checked')) {
            $('.shippingaddress').show();
        } else {
            $('.shippingaddress').hide();
        }
    })

    setTimeout(function () {
        $(document).ready(function () {

            <?php if(!$this->session->userdata("id")) {
            echo "return;";
            } ?>

            var customer_id = '<?php echo $this->session->userdata("id"); ?>';
            var weight = $('.frameweight').val();
            var csrf_test_name = $('.csrf_test_name').val();

            $.ajax({
                type: "POST",
                url: base_url + 'Kids_collection/getshipping',
                data: {customer_id: customer_id, csrf_test_name: '<?php echo $this->security->get_csrf_hash(); ?>', weight: weight},
                cache: false,
                dataType: 'json',
                success: function (data) {

                    if (data.success) {
                        $('.shipmentamount').val(data.shipamount);
                    } else {
                        $('.shipmentamount').val(0);
                    }
                }
            });

        })

    }, 2000);
    $('.deletedraft').click(function (e) {
        e.preventDefault();
        if (confirm('Are you sure you want to delete this!')) {
            var id = $(this).data('ids');

            $.ajax({
                type: "GET",
                url: base_url + 'Cart/deletedraft/' + id,
                data: {id: id},
                cache: false,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                }
            });
        }
    });

    $('.removerow').click(function (e) {
        e.preventDefault();

        if (confirm('Are you sure you want to delete this!')) {
            var id = $(this).data('ids');

            $.ajax({
                type: "GET",
                url: base_url + 'Frame_layout/delete',
                data: {id: id},
                cache: false,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                }
            });
        }
    });


    $('.titlecanvas').on('keyup', function () {

        const quoteText = document.getElementById("quote__text");

        if ($(this).val() != "") {
            quoteText.innerHTML = this.value;
            renderCanvas();
        }
        else {
            quoteText.innerHTML = "Start typing </br>:)"
        }

    });

    $(".namecanvas").on('keyup', function () {

        const quoteText = document.getElementById("secondtext");

        if ($(this).val() != "") {
            quoteText.innerHTML = this.value;
            renderCanvas();
        }
        else {
            quoteText.innerHTML = "Start typing </br>:)"
        }

    });


    $(".datecanvas").on('keyup', function () {

        const quoteText = document.getElementById("datetext");

        if ($(this).val() != "") {
            quoteText.innerHTML = this.value;
            renderCanvas();
        }
        else {
            quoteText.innerHTML = "Start typing </br>:)"
        }

    });


    $(".daycanvas").on('keyup', function () {

        const quoteText = document.getElementById("daytext");

        if ($(this).val() != "") {
            quoteText.innerHTML = this.value;
            renderCanvas();
        }
        else {
            quoteText.innerHTML = "Start typing </br>:)"
        }

    });


    $(".timecanvas").on('keyup', function () {

        const quoteText = document.getElementById("timetext");

        if ($(this).val() != "") {
            quoteText.innerHTML = this.value;
            renderCanvas();
        }
        else {
            quoteText.innerHTML = "Start typing </br>:)"
        }

    });

    function parseVal(val) {
        val = parseFloat(val);
        if(isNaN(val)) {
            val = 0;
        }

        return val;
    }


</script>
</body>
</html>

    
  