<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>New Orders</h1>
    </div>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <div class="box box-info">
                <div class="box-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Customer Name</th>
                            <th>Total Amount</th>
                            <th>Status</th>
                            <th width="140">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach ($orders as $row) {
                            $i++;
                            ?>
                            <tr>
                                <td style="width:100px;"><?php echo $i; ?></td>
                                <td><?php echo $row['customer_name']; ?></td>
                                <td>AED <?php echo $row['totalamount']; ?></td>
                                <td style="width:250px;"><?php if ($row['status'] == 1) {
                                        echo 'NOT Delivered';
                                    } else {
                                        echo 'Delivered';
                                    } ?></td>
                                <td>
                                    <!--<a class="btn btn-success btn-xs" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">Details</a>	-->
                                    <a href="<?php echo base_url(); ?>admin/orders/detail/<?php echo $row['id']; ?>"
                                       class="btn btn-primary btn-xs">View Detail</a>
                                    <?php if ($row['status'] == 2 || $row['paymentstatus'] == 0) { ?>
                                        <a href="<?php echo base_url(); ?>admin/orders/delete/<?php echo $row['id']; ?>"
                                           class="btn btn-danger btn-xs" onClick="return confirm('Are you sure?');">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>

                            <?php
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>