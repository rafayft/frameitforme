<?php print_r($this->session->userdata('data')); ?>
<form class="frameform">
    <input type="hidden" class="<?php echo $this->security->get_csrf_token_name(); ?>"
           name="<?php echo $this->security->get_csrf_token_name(); ?>"
           value="<?php echo $this->security->get_csrf_hash(); ?>">
    <section class="order-numbering">
        <div>
            WALL DETAILS
        </div>
        <div>
            FRAME LAYOUT
        </div>
        <div>
            ADD IMAGES
        </div>
        <div>
            PREVIEW
        </div>
        <div>
            CHECKOUT
        </div>
        <div class="cancel-order" onclick="window.location='<?php echo base_url(); ?>'">
            CANCEL X
        </div>
        <div class="process-line">
        </div>
    </section>
    <?php $id = $this->input->get('catid'); ?>
    <section class="all-steps">
        <div class="container">
            <?php if ($id == '') { ?>
                <!-- First Step Starts From here -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="first-step step1">
                            <h5>What layout best suits your space?</h5>
                            <div class="layout-dropdown">
                                <button id="dLabel" type="button" class="layoutseelction" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false" value="Horizontal">
                                    <span class="buttontext">Select Layout</span>
                                    <img src="<?php echo base_url(); ?>images/angle-down.png">
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <?php
                                    foreach ($frame_category as $category) {
                                        ?>
                                        <li><a href="javascript:void()"
                                               data-value="<?php echo $category['category_name']; ?>"
                                               data-ids="<?php echo $category['category_id']; ?>"> <?php echo $category['category_name']; ?> </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            <?php } ?>
            <!-- First Step Ends Here -->
            <!-- Second Step Starts From Here -->
            <div class="row">
                <div class="col-md-12">
                    <div class="second-step step2">

                        <div class="frame-det-right">
                            <div class="custom-frame-head">
                                <h5>Frame Layout</h5>
                            </div>
                            <div class="custom-frame-boxes" style="background:transparent">
                                <div class="">
                                    <div class="row layoutdynamic text-center">


                                    </div>

                                    <div class="row customizebox">
                                        <label>X-pos<input type=range id=xpos min=0 value=150 max=950
                                                           step=1></label><br>
                                        <label>Y-pos<input type=range id=ypos min=0 value=150 max=550
                                                           step=1></label><br>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Second Step Ends Here -->
            <!-- Second Step Starts From Here -->
            <div class="row">

            </div>
            <div class="second-step step3">
                <div class="custom-frame-head suxtomizeframe">
                    <h5>Customize your frame details</h5>
                </div>
                <div class="frame-det-left">
                    <div class="secondstepside">
                        <div class="frame-det-color">
                            <h5>FRAME COLOR</h5>
                            <span class="black clickcolor" data-ids="#C7C9C7">.</span>
                            <span class="white clickcolor" data-ids="white">.</span>
                            <span class="brown clickcolor" data-ids="#84754E">.</span>
                            <span class="grey clickcolor" data-ids="#5C3D31">.</span>


                        </div>
                        <div class="layercanvascustom">
                            <div class="frame-det-color no-padding">
                                <h5>FRAME <br/> DETAILS</h5>
                                <h6>Your set comes with the following frame sizes </h6>
                            </div>
                            <div class="frame-selections" id="dvSource">
                                <canvas id="myCanvas" class="layer" width="70" height="60"
                                        style="border:    outline: 14px solid #b6b6b6;border: 24px solid #f4f6f5;">
                                </canvas>
                                <canvas id="myCanvas1" class="layer" width="70" height="70"
                                        style="    outline: 14px solid #b6b6b6;border: 24px solid #f4f6f5;">
                                </canvas>
                                <canvas id="myCanvas2" class="layer" width="135" height="170"
                                        style="    outline: 14px solid #b6b6b6;border: 24px solid #f4f6f5;">
                                </canvas>
                                <?php
                                $frameget = $this->db->get_where('tbl_frame', array('category_id' => $id));
                                if ($frameget->num_rows() > 0) {
                                    foreach ($frameget->result() as $fr) {
                                        //echo '<img src="'.base_url().'public/uploads/'.$fr->photo.'" class="ui-draggable">';
                                        //echo '<p>1 Piece: 21.5 x 25.5</p>';
                                    }

                                } else {
                                    ?>
                                    <p>No frame found</p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="column imagevolom">
                        <div class="frame-det-color imagevolom-color">
                            <h5>ADD IMAGES</h5>
                        </div>
                        <div id="leftSide">
                            <h4>Or upload your own images.</h4>
                            <div id="images_preview"></div>
                            <div id="dropZone">
                                <a href="havascript:void(0)" class="drag_dorpBtn">Drag &amp; Drop or click to upload
                                    your image</a>
                            </div>
                        </div>
                        <p>For best printing results, images must be 300dpi resolution, jpeg or tiff format , less than
                            4MB <u></u>!</p>
                    </div>
                </div>
                <div class="frame-det-right">

                    <div class="custom-frame-boxes" id="custom-frame-boxes"
                         style="background : transparent;height:800px">
                        <div class="">
                            <div class="row">
                                <div id="dvDest">
                                    <div id="photo" class="canvas-container">
                                        <canvas id="background" class="" width="950" height="550"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="frame-toggle">
                  <p>Select to view in metric or <br/> imperial dimensions</p>
                  <div class="frame-toggle-btn">
                    <span class="frame-style1"> CM </span>
                    <b> <sup> </sup> </b>
                    <span class="frame-style2">IN</span>
                  </div>
                </div>-->
            </div>
            <!-- Second Step Ends Here -->

            <div class="fourth-step step4">
                <div class="frame-det-right">
                    <div class="custom-frame-head">
                        <h5>Preview your wall</h5>
                        <p>That's pretty good looking wall. Are you happy with it?</p>
                    </div>
                    <div id="previewImage"></div>
                    <img src="" class="previewimage"></img>
                </div>
                <div class="order-completed">
                    <a href="javascript:void()" class="previousicon"> NO, GO BACK </a>
                    <a href="javascript:void()" class="checkoutbuton"> YES, CHECKOUT </a>
                </div>

            </div>


            <section class="print-page step5">
                <div class="container">
                    <div class="print-head">
                        <h5>Print Subscription </h5>
                        <p>Let your wall grow with you as you make new memories.
                            Sign up for a subscription package that best suits you.</p>
                    </div>

                    <div class="print-data">
                        <div class="row">

                            <?php foreach ($subcription->result() as $subsrow) { ?>
                                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 border-right">
                                    <div class="subscription-text">
                                        <h5><?php echo $subsrow->name; ?></h5>
                                        <h6>AED <?php echo $subsrow->price; ?> / YEAR </h6>
                                        <p><?php echo $subsrow->description; ?></p>
                                        <div class="radio-toolbar">
                                            <input type="radio" id="subscription<?php echo $subsrow->id; ?>"
                                                   name="subscription" class="layout-select"
                                                   value="<?php echo $subsrow->price; ?>"
                                                   data-title="<?php echo $subsrow->name; ?>"
                                                   data-desc="<?php echo $subsrow->description; ?>">
                                            <label for="subscription<?php echo $subsrow->id; ?>" class="print-button1">Select</label>
                                        </div>


                                    </div>
                                </div>
                            <?php } ?>


                        </div>
                    </div>

                    <div class="print-button">
                        <a href="javascript:void()" class="skippackage"> Skip For Now </a>
                        <a href="javascript:void()" class="checkoutbuton"> ChECKOUT </a>
                    </div>

                </div>
            </section>


            <section class="checkout-page step6">
                <div class="container">


                    <div class="account-head">
                        <h5>Your Cart</h5>
                        <p>1 FRAME SET</p>
                    </div>
                    <div class="row_1">
                        <div class="checkout-item-head">
                            <div class="check-col1">
                                ITEM
                            </div>
                            <div class="check-col1">
                                DESCRIPTION
                            </div>
                            <div class="check-col1">
                                DETAILS
                            </div>
                            <div class="check-col1">
                                PRICE
                            </div>
                        </div>
                        <?php
                        $cart = $this->session->userdata('cart');
                        if (isset($cart)) {

                            foreach ($cart as $artrow) {

                                ?>
                                <div class="checkout-item-data">
                                    <div class="check-col1">
                                        <img src="<?php echo $artrow['frameimage']; ?>">
                                        <input type="hidden" name="fameimage[]"
                                               value="<?php echo $artrow['frameimage']; ?>"></input>

                                    </div>
                                    <div class="check-col1 ">
                                        <?php echo $artrow['framename']; ?>

                                    </div>

                                    <div class="check-col1 ">
                                        -
                                        <br/>
                                        <br/>
                                        <span class="est-delivery"> <b> Estimated Delivery: </b>5-7 business days </span>
                                    </div>
                                    <div class="check-col1">
                                        AED <?php echo $artrow['frameamount']; ?>
                                        <input type="hidden" name="frameamount[]"
                                               value="<?php echo $artrow['frameamount']; ?>"></input>
                                        <input type="hidden" name="famrname[]"
                                               value="<?php echo $artrow['framename']; ?>"></input>
                                        <input type="hidden" name="id[]" value="<?php echo $artrow['id']; ?>"></input>

                                    </div>
                                    <div class="check-col1">
                                        <a class="removerow" data-ids="1"> REMOVE </a>
                                    </div>
                                </div>
                            <?php }
                        } ?>


                        <div class="checkout-item-data">
                            <div class="check-col1">
                                <img src="images/item1.jpg" class="frameimage">
                                <input type="hidden" name="fameimage[]" class="fameimages"></input>

                            </div>
                            <div class="check-col1 framename">


                            </div>

                            <div class="check-col1 framedesctiption">
                                1 Frame (25.5 x 21.5 in) <br/>
                                1 Frame (25.5 x 21.5 in) <br/>
                                1 Frame (25.5 x 21.5 in)
                                <br/>
                                <br/>
                                <span class="est-delivery"> <b> <?php echo $setting['estimate']; ?></span>
                            </div>
                            <div class="check-col1">
                                AED <span class="totalframeamount"></span>
                                <input type="hidden" name="frameamount[]" class="frameamount"></input>
                                <input type="hidden" name="famrname[]" class="famrname"></input>
                                <input type="hidden" name="id[]" class="layoutid"></input>
                            </div>
                            <div class="check-col1">
                                <a class="removerow" data-ids="1"> REMOVE </a>
                            </div>
                        </div>
                    </div>


                    <div class="checkout-item-data">
                        <div class="check-col1 empty-width">
                            &nbsp;
                        </div>
                        <div class="check-col1">
                            <input type="checkbox" name="installationservice" value="75" class="installationservice"
                                   checked> Installation Service
                        </div>
                        <div class="check-col1">
                            Frame installation service upon delivery in Dubai only
                        </div>
                        <div class="check-col1">
                            AED 75
                            <input type="hidden" name="installationamount" class="installationamount"
                                   value="<?php echo $setting['installationamount']; ?>"></input>
                            <div class="frameimagesall"></div>
                        </div>
                    </div>


                    <div class="black-line ">
                    </div>
                    <?php
                    $subscriptioncart = $this->session->userdata('subscriptioncart');

                    if (isset($subscriptioncart)) {

                        foreach ($subscriptioncart as $sbartrowqwe) {

                            ?>
                            <div class="checkout-item-data no-padding ">
                                <div class="check-col1">
                                    <img src="<?php echo base_url(); ?>images/item2.jpg">
                                </div>
                                <div class="check-col1 ">
                                    <?php echo $sbartrowqwe['subscription']; ?>
                                </div>
                                <div class="check-col1 subscriptiondesc">
                                    1 Print (11 x 14 in) <br/>
                                    12 Prints (5x7 in) <br/>
                                    <br/>

                                </div>
                                <div class="check-col1 ">
                                    AED <?php echo $sbartrowqwe['subscriptionprice']; ?>

                                </div>
                                <div class="check-col1">
                                    <a> REMOVE </a>
                                </div>
                            </div>
                            <input type="hidden" name="subscriptionprice"
                                   value="<?php echo $sbartrowqwe['subscriptionprice']; ?>"></input>
                            <input type="hidden" name="subscription"
                                   value="<?php echo $sbartrowqwe['subscription']; ?>"></input>
                            <input type="hidden" name="subscriptiondesc"
                                   value="<?php echo $sbartrowqwe['subscriptiondesc']; ?>"></input>
                        <?php }
                    } ?>
                    <div class="checkout-item-data no-padding subscriptionamount">
                        <div class="check-col1">
                            <img src="<?php echo base_url(); ?>images/item2.jpg">
                        </div>
                        <div class="check-col1 subscriptiontitle">
                            Subscription <br/> Bi-Annual Prints
                            <input type="hidden" name="subscription" class="subscription"></input>
                        </div>
                        <div class="check-col1 subscriptiondesc">
                            1 Print (11 x 14 in) <br/>
                            12 Prints (5x7 in) <br/>
                            <br/>
                            <input type="hidden" name="subscriptiondesc" class="subscriptiondescription"></input>
                        </div>
                        <div class="check-col1 ">
                            AED <span class="subscriptionval"></span>
                            <input type="hidden" name="subscriptionprice" class="subscriptionprice"></input>
                        </div>
                        <div class="check-col1">
                            <a> REMOVE </a>
                        </div>
                    </div>
                    <div class="black-line backline2">
                    </div>


                    <div class="checkout-payments">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="promo-form">
                                    <h5>PROMO CODE</h5>

                                    <input type="text" placeholder="" class="promocode" name="">
                                    <input type="button" class="couponcodetwo" value="APPLY" name="">

                                    <div class="couponresult"></div>

                                </div>
                            </div>

                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <?php
                                $totalcart = $this->session->userdata('totalcart');

                                if (isset($totalcart)) {

                                    foreach ($totalcart as $ardartrow) {

                                        ?>
                                        <div class="checkout-totals">
                                            <p>
                                                <b>SUBTOTAL</b>
                                                <span class="subtotal">AED <span
                                                            class="subtotal"><?php echo $ardartrow['subtotal']; ?></span></span>
                                                <input type="hidden" class="subtotalss" name="subtotal"
                                                       value="<?php echo $ardartrow['subtotal']; ?>"></input>
                                            </p>

                                            <p>
                                                <b>SHIPPING</b>
                                                <span class="shippingamount">Free</span>
                                                <input type="hidden" name="shippingamount" class="shippingamount"
                                                       value="0"></input>
                                            </p>
                                            <p>
                                                <b>Promo Code</b>
                                                <span class="">AED <span class="codeamountplus">0</span></span>
                                                <input type="hidden" name="promocode"
                                                       value="<?php echo $ardartrow['promoamount']; ?>"
                                                       class="promocodevalue"></input>
                                            </p>

                                            <p style="display:none">
                                                <b>VAT</b>
                                                <span>AED <span
                                                            class="vatamount"><?php echo $ardartrow['vatamount']; ?></span></span>
                                                <input type="hidden" name="vatamount" class="vatamounts"
                                                       value="<?php echo $ardartrow['vatamount']; ?>"></input>
                                                <input type="hidden" name="weightselected" class="weightselected"
                                                       value="<?php echo $ardartrow['weight']; ?>"></input>
                                            </p>

                                            <h5>
                                                <b>TOTAL</b>
                                                <span class="">AED <span
                                                            class="totalframeamountfinal"><?php echo $ardartrow['totalamount']; ?></span></span>
                                                <input type="hidden" name="totalamount" class="totalamount"
                                                       value="<?php echo $ardartrow['totalamount']; ?>"></input>
                                            </h5>

                                        </div>
                                    <?php }
                                } else { ?>

                                    <div class="checkout-totals">
                                        <p>
                                            <b>SUBTOTAL</b>
                                            <span class="">AED <span class="subtotal">0</span></span>
                                            <input type="hidden" class="subtotalss" name="subtotal" value="0"></input>
                                        </p>

                                        <p>
                                            <b>SHIPPING</b>
                                            <span class="shippingamount">Free</span>
                                            <input type="hidden" name="shippingamount" class="shippingamount"
                                                   value="0"></input>
                                        </p>
                                        <p>
                                            <b>Promo Code</b>
                                            <span class="">AED <span class="codeamountplus">0</span></span>
                                            <input type="hidden" name="promocode" value="0"
                                                   class="promocodevalue"></input>
                                        </p>
                                        <p style="display:none">
                                            <b>VAT</b>
                                            <span>AED <span class="vatamount">0</span></span>
                                            <input type="hidden" name="vatamount" class="vatamounts" value=""></input>
                                            <input type="hidden" name="weightselected" class="weightselected"
                                                   value="0"></input>
                                        </p>

                                        <h5>
                                            <b>TOTAL</b>
                                            <span class="">AED <span class="totalframeamountfinal">0</span></span>
                                            <input type="hidden" name="totalamount" class="totalamount"
                                                   value="0"></input>
                                        </h5>

                                    </div>
                                <?php } ?>

                                <input type="hidden" name="initialamount" value="" class="initialamount"></input>
                                <input type="hidden" name="initialamount" value="" class="additionalamount"></input>
                            </div>
                        </div>


                        <div class="proceed-checkout">
                            <a href="javascript:void()" class="continueshopping">CONTINUE SHOPPING</a> | <a
                                    href="javascript:void()" class="savedraft"> SAVE TO DRAFTS </a>
                            <button type="button" name="PROCEED" class="proceed">PROCEED TO SECURE CHECKOUT</button>
                        </div>

                    </div>


                </div>
            </section>

        </div>
        <div class="row">
            <div class="next-previous">
                <div class="next-buttons">
                    <a href="javascript:void(0)" class="previousbtn" data-ids="">PREVIOUS</a>
                    <a href="javascript:void(0)" class="nextbtn" data-ids="">NEXT</a>
                </div>
            </div>
        </div>

    </section>
</form>

<div class="modal"></div>