<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Edit Management</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/feature" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/feature/edit/' . $feature['id'], array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Name *</label>
                        <div class="col-sm-8">
                            <input type="text" autocomplete="off" class="form-control" name="name"
                                   value="<?php echo $feature['name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Content *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="content"
                                      style="height:120px;"><?php echo $feature['content']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Position *</label>
                        <div class="col-sm-8">
                            <input type="text" autocomplete="off" class="form-control" name="position"
                                   value="<?php echo $feature['position']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Existing Image</label>
                        <div class="col-sm-6" style="padding-top:6px;">
                            <img src="<?php echo base_url(); ?>public/uploads/<?php echo $feature['image']; ?>"
                                 class="existing-photo" style="height:180px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Image </label>
                        <div class="col-sm-6" style="padding-top:5px;">
                            <input type="file" name="home_feature_image">
                            <input type="hidden" name="featureimage" value="<?php echo $feature['image']; ?>"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Update</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>