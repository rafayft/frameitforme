<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Add Email Template</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/emailtemplate" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/emailtemplate/' . (isset($template['id']) ? 'edit/' . $template['id'] : 'add'), array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Template Name *</label>
                        <div class="col-sm-6">
                            <input max="50" type="text" autocomplete="off" class="form-control" name="name" value="<?= isset($template['name']) ? $template['name'] : '' ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Email Subject *</label>
                        <div class="col-sm-6">
                            <input max="50" type="text" autocomplete="off" class="form-control" name="subject" value="<?= isset($template['subject']) ? $template['subject'] : '' ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Body *</label>
                        <div class="col-sm-6">
                            <textarea class="editor" name="body" id="" cols="30" rows="10" required><?= isset($template['body']) ? $template['body'] : '' ?></textarea>
                        </div>
                        <div class="col-sm-4">
                            <p><strong>Tokens:</strong></p>
                            <p>{base_url} - Base URL</p>
                            <p>{register_code} - Registration Code</p>
                            <p>{order_details} - Order Details List</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>
