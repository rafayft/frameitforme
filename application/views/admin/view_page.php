<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Page Section</h1>
    </div>
</section>

<section class="content" style="min-height:auto;margin-bottom: -30px;">
    <div class="row">
        <div class="col-md-12">
            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Home</a></li>
                    <!--<li><a href="#tab_2" data-toggle="tab">How It Work</a></li>-->
                    <li><a href="#tab_4" data-toggle="tab">FAQ</a></li>
                    <!--<li><a href="#tab_5" data-toggle="tab">Service</a></li>
                    <li><a href="#tab_7" data-toggle="tab">Testimonial</a></li>
                    <li><a href="#tab_8" data-toggle="tab">News</a></li>-->
                    <li><a href="#tab_16" data-toggle="tab">Disclaimer</a></li>
                    <li><a href="#tab_9" data-toggle="tab">Contact Us</a></li>
                    <li><a href="#tab_10" data-toggle="tab">Refund Policy</a></li>
                    <li><a href="#tab_11" data-toggle="tab">Terms</a></li>
                    <li><a href="#tab_12" data-toggle="tab">Privacy</a></li>
                    <li><a href="#tab_14" data-toggle="tab">Shipping & Handling</a></li>
                    <li><a href="#tab_15" data-toggle="tab">Kids Collection</a></li>

                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="tab_1">

                        <h3 class="sec_title">Meta Items</h3>
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Title </label>
                            <div class="col-sm-6">
                                <input type="text" name="title" class="form-control"
                                       value="<?php echo $page_home['title']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="meta_keyword"
                                          style="height:60px;"><?php echo $page_home['meta_keyword']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-6">
                                <textarea class="form-control" name="meta_description"
                                          style="height:60px;"><?php echo $page_home['meta_description']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_home">Update</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>

                        <h3 class="sec_title">Welcome Section</h3>
                        <?php echo form_open_multipart(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Title </label>
                            <div class="col-sm-6">
                                <input type="text" name="home_welcome_title" class="form-control"
                                       value="<?php echo $page_home['home_welcome_title']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Text </label>
                            <div class="col-sm-6">
                                <textarea name="home_welcome_text" class="form-control editor_short" cols="30"
                                          rows="10"><?php echo $page_home['home_welcome_text']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Existing Image</label>
                            <div class="col-sm-6" style="padding-top:6px;">
                                <img src="<?php echo base_url(); ?>public/uploads/<?php echo $page_home['home_welcome_video_bg']; ?>"
                                     class="existing-photo" style="height:180px;">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Image </label>
                            <div class="col-sm-6" style="padding-top:5px;">
                                <input type="file" name="home_welcome_video_bg">
                                <input type="hidden" name="welcomeimage"
                                       value="<?php echo $page_home['home_welcome_video_bg']; ?>"></input>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Show on Home? </label>
                            <div class="col-sm-2">
                                <select name="home_welcome_status" class="form-control select2" style="width:auto;">
                                    <option value="Show" <?php if ($page_home['home_welcome_status'] == 'Show') {
                                        echo 'selected';
                                    } ?>>Show
                                    </option>
                                    <option value="Hide" <?php if ($page_home['home_welcome_status'] == 'Hide') {
                                        echo 'selected';
                                    } ?>>Hide
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-2"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_home_welcome">
                                    Update
                                </button>
                                </select>
                            </div>
                        </div>

                        <?php echo form_close(); ?>


                        <h3 class="sec_title">Home Servcies</h3>
                        <?php echo form_open_multipart(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Show on Home? </label>
                            <div class="col-sm-2">
                                <select name="home_why_choose_status" class="form-control select2" style="width:auto;">
                                    <option value="Show" <?php if ($page_home['home_why_choose_status'] == 'Show') {
                                        echo 'selected';
                                    } ?>>Show
                                    </option>
                                    <option value="Hide" <?php if ($page_home['home_why_choose_status'] == 'Hide') {
                                        echo 'selected';
                                    } ?>>Hide
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_home_why_choose">
                                    Update
                                </button>
                                </select>
                            </div>
                        </div>
                        <?php echo form_close(); ?>


                        <h3 class="sec_title">Standard Frame Set</h3>
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Title </label>
                            <div class="col-sm-6">
                                <input type="text" name="home_service_title" class="form-control"
                                       value="<?php echo $page_home['home_service_title']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Description </label>
                            <div class="col-sm-6">
                                <textarea name="home_service_desccription" class="form-control editor_short" cols="30"
                                          rows="10"><?php echo $page_home['home_service_desccription']; ?></textarea>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Show on Home? </label>
                            <div class="col-sm-2">
                                <select name="home_service_status" class="form-control select2" style="width:auto;">
                                    <option value="Show" <?php if ($page_home['home_service_status'] == 'Show') {
                                        echo 'selected';
                                    } ?>>Show
                                    </option>
                                    <option value="Hide" <?php if ($page_home['home_service_status'] == 'Hide') {
                                        echo 'selected';
                                    } ?>>Hide
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_home_service">
                                    Update
                                </button>
                                </select>
                            </div>
                        </div>
                        <?php echo form_close(); ?>


                        <h3 class="sec_title">Kids Collection Section</h3>
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Title </label>
                            <div class="col-sm-6">
                                <input type="text" name="home_portfolio_title" class="form-control"
                                       value="<?php echo $page_home['home_portfolio_title']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">SubTitle </label>
                            <div class="col-sm-6">
                                <input type="text" name="home_portfolio_subtitle" class="form-control"
                                       value="<?php echo $page_home['home_portfolio_subtitle']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Description </label>
                            <div class="col-sm-9">
                                <textarea name="home_portfolio_desccription" class="form-control" cols="30" rows="10"
                                          id="editor1"><?php echo $page_home['home_portfolio_desccription']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Show On Home </label>
                            <div class="col-sm-6">
                                <input type="number" name="home_portfolio_showonhome" class="form-control"
                                       value="<?php echo $page_home['home_portfolio_showonhome']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Show on Home? </label>
                            <div class="col-sm-2">
                                <select name="home_portfolio_status" class="form-control select2" style="width:auto;">
                                    <option value="Show" <?php if ($page_home['home_portfolio_status'] == 'Show') {
                                        echo 'selected';
                                    } ?>>Show
                                    </option>
                                    <option value="Hide" <?php if ($page_home['home_portfolio_status'] == 'Hide') {
                                        echo 'selected';
                                    } ?>>Hide
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_home_portfolio">
                                    Update
                                </button>

                            </div>
                        </div>
                        <?php echo form_close(); ?>


                    </div>

                    <div class="tab-pane" id="tab_2">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">About Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="about_heading" class="form-control"
                                       value="<?php echo $page_about['about_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">About Content </label>
                            <div class="col-sm-9">
                                <textarea name="about_content" class="form-control" cols="30" rows="10"
                                          id="editor1"><?php echo $page_about['about_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_about" class="form-control"
                                       value="<?php echo $page_about['mt_about']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_about"
                                          style="height:60px;"><?php echo $page_about['mk_about']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_about"
                                          style="height:60px;"><?php echo $page_about['md_about']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_about">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_4">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">FAQ Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="faq_heading" class="form-control"
                                       value="<?php echo $page_faq['faq_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">FAQ Description </label>
                            <div class="col-sm-6">
                                <textarea name="faq_description" class="form-control editor_short" cols="30"
                                          rows="10"><?php echo $page_faq['faq_description']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_faq" class="form-control"
                                       value="<?php echo $page_faq['mt_faq']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_faq"
                                          style="height:60px;"><?php echo $page_faq['mk_faq']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_faq"
                                          style="height:60px;"><?php echo $page_faq['md_faq']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_faq">Update</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_5">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Service Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="service_heading" class="form-control"
                                       value="<?php echo $page_service['service_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_service" class="form-control"
                                       value="<?php echo $page_service['mt_service']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_service"
                                          style="height:60px;"><?php echo $page_service['mk_service']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_service"
                                          style="height:60px;"><?php echo $page_service['md_service']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_service">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_7">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Testimonial Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="testimonial_heading" class="form-control"
                                       value="<?php echo $page_testimonial['testimonial_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_testimonial" class="form-control"
                                       value="<?php echo $page_testimonial['mt_testimonial']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_testimonial"
                                          style="height:60px;"><?php echo $page_testimonial['mk_testimonial']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_testimonial"
                                          style="height:60px;"><?php echo $page_testimonial['md_testimonial']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_testimonial">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_8">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">News Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="news_heading" class="form-control"
                                       value="<?php echo $page_news['news_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_news" class="form-control"
                                       value="<?php echo $page_news['mt_news']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_news"
                                          style="height:60px;"><?php echo $page_news['mk_news']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_news"
                                          style="height:60px;"><?php echo $page_news['md_news']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_news">Update</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_16">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Disclaimer Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="disclaimer_heading" class="form-control"
                                       value="<?php echo $page_disclaimer['disclaimer_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Refund Policy Content </label>
                            <div class="col-sm-9">
                                <textarea name="disclaimer_content" class="form-control editor_short" cols="30"
                                          rows="10"
                                          id="editor3"><?php echo $page_disclaimer['disclaimer_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_disclaimer" class="form-control"
                                       value="<?php echo $page_disclaimer['mt_disclaimer']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_disclaimer"
                                          style="height:60px;"><?php echo $page_disclaimer['mk_disclaimer']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_disclaimer"
                                          style="height:60px;"><?php echo $page_disclaimer['md_disclaimer']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_disclaimer">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_9">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Contact Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="contact_heading" class="form-control"
                                       value="<?php echo $page_contact['contact_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Contact Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control editor_short" name="contact_address"
                                          style="height:60px;"><?php echo $page_contact['contact_address']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Contact Email </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="contact_email"
                                          style="height:60px;"><?php echo $page_contact['contact_email']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Contact Phone </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="contact_phone"
                                          style="height:60px;"><?php echo $page_contact['contact_phone']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_contact" class="form-control"
                                       value="<?php echo $page_contact['mt_contact']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_contact"
                                          style="height:60px;"><?php echo $page_contact['mk_contact']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_contact"
                                          style="height:60px;"><?php echo $page_contact['md_contact']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_contact">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_10">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Refund Policy Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="refund_heading" class="form-control"
                                       value="<?php echo $page_refund['refund_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Refund Policy Content </label>
                            <div class="col-sm-9">
                                <textarea name="refund_content" class="form-control editor_short" cols="30" rows="10"
                                          id="editor3"><?php echo $page_refund['refund_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_refund" class="form-control"
                                       value="<?php echo $page_refund['mt_refund']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_refund"
                                          style="height:60px;"><?php echo $page_refund['mk_refund']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_refund"
                                          style="height:60px;"><?php echo $page_refund['md_refund']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_refund">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_11">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Term & Condition Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="term_heading" class="form-control"
                                       value="<?php echo $page_term['term_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Term & Condition Content </label>
                            <div class="col-sm-9">
                                <textarea name="term_content" class="form-control" cols="30" rows="10"
                                          id="editor2"><?php echo $page_term['term_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_term" class="form-control"
                                       value="<?php echo $page_term['mt_term']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_term"
                                          style="height:60px;"><?php echo $page_term['mk_term']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_term"
                                          style="height:60px;"><?php echo $page_term['md_term']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_term">Update</button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_12">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Privacy Policy Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="privacy_heading" class="form-control"
                                       value="<?php echo $page_privacy['privacy_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Privacy Policy Content </label>
                            <div class="col-sm-9">
                                <textarea name="privacy_content" class="form-control" cols="30" rows="10"
                                          id="editor3"><?php echo $page_privacy['privacy_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_privacy" class="form-control"
                                       value="<?php echo $page_privacy['mt_privacy']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_privacy"
                                          style="height:60px;"><?php echo $page_privacy['mk_privacy']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_privacy"
                                          style="height:60px;"><?php echo $page_privacy['md_privacy']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_privacy">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_14">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Shipment Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="shipment_heading" class="form-control"
                                       value="<?php echo $page_shipment['shipment_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Shipment Policy Content </label>
                            <div class="col-sm-9">
                                <textarea name="shipment_content" class="form-control editor_short" cols="30" rows="10"
                                          id="editor3"><?php echo $page_shipment['shipment_content']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_shipment" class="form-control"
                                       value="<?php echo $page_shipment['mt_shipment']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_shipment"
                                          style="height:60px;"><?php echo $page_shipment['mk_shipment']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_shipment"
                                          style="height:60px;"><?php echo $page_shipment['md_shipment']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_shipment">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                    <div class="tab-pane" id="tab_15">
                        <?php echo form_open(base_url() . 'admin/page/update', array('class' => 'form-horizontal')); ?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Portfolio Heading </label>
                            <div class="col-sm-6">
                                <input type="text" name="portfolio_heading" class="form-control"
                                       value="<?php echo $page_portfolio['portfolio_heading']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Title </label>
                            <div class="col-sm-9">
                                <input type="text" name="mt_portfolio" class="form-control"
                                       value="<?php echo $page_portfolio['mt_portfolio']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Keyword </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="mk_portfolio"
                                          style="height:60px;"><?php echo $page_portfolio['mk_portfolio']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Meta Description </label>
                            <div class="col-sm-9">
                                <textarea class="form-control" name="md_portfolio"
                                          style="height:60px;"><?php echo $page_portfolio['md_portfolio']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form_portfolio">Update
                                </button>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>


                </div>
            </div>


        </div>
    </div>

</section>