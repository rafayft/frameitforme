<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Edit Frame Sub Category</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/portfolio_category" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open(base_url() . 'admin/portfolio_subcategory/edit/' . $portfolio_category['subcategory_id'], array('class' => 'form-horizontal')); ?>

            <div class="box box-info">

                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Sub Category Name *</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="subcategory_name"
                                   value="<?php echo $portfolio_category['subcategoryname']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Category Name <span>*</span></label>
                        <div class="col-sm-4">
                            <select class="form-control" name="category_id">
                                <option value="">Select Category</option>
                                <?php
                                foreach ($category as $catrow) {
                                    ?>
                                    <option value="<?php echo $catrow['category_id']; ?>" <?php if ($catrow['category_id'] == $portfolio_category['category_id']) {
                                        echo 'selected';
                                    } ?>><?php echo $catrow['category_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-4">
                            <select name="status" class="form-control select2">
                                <option value="Active" <?php if ($portfolio_category['status'] == 'Active') {
                                    echo 'selected';
                                } ?>>Active
                                </option>
                                <option value="Inactive" <?php if ($portfolio_category['status'] == 'Inactive') {
                                    echo 'selected';
                                } ?>>Inactive
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Update</button>
                        </div>
                    </div>

                </div>

            </div>

            <?php echo form_close(); ?>

        </div>
    </div>

</section>