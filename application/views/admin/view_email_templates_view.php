<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $template['subject'] ?></title>
</head>
<body>
<?= str_replace('{base_url}', base_url(), $template['body']) ?>
</body>
</html>
