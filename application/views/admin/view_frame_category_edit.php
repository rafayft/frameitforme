<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Edit Frame Category</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/frame_category" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/frame_category/edit/' . $frame_category['category_id'], array('class' => 'form-horizontal')); ?>

            <div class="box box-info">

                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Category Name *</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" name="category_name"
                                   value="<?php echo $frame_category['category_name']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Price <span>*</span></label>
                        <div class="col-sm-4">
                            <input value="<?= $frame_category['price']; ?>" type="number" name="price" placeholder="" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Status *</label>
                        <div class="col-sm-4">
                            <select name="status" class="form-control select2">
                                <option value="Active" <?php if ($frame_category['status'] == 'Active') {
                                    echo 'selected';
                                } ?>>Active
                                </option>
                                <option value="Inactive" <?php if ($frame_category['status'] == 'Inactive') {
                                    echo 'selected';
                                } ?>>Inactive
                                </option>
                            </select>
                        </div>
                    </div>
                    <h3 class="seo-info">Featured Photo</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Existing Featuerd Photo</label>
                        <div class="col-sm-9" style="padding-top:5px">
                            <img src="<?php echo base_url(); ?>public/uploads/<?php echo $frame_category['photo']; ?>" alt=""
                                 style="width:120px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Change Featuerd Photo</label>
                        <div class="col-sm-9" style="padding-top:5px">
                            <input type="file" name="photo">(Only jpg, jpeg, gif and png are allowed)
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Update</button>
                        </div>
                    </div>

                </div>

            </div>

            <?php echo form_close(); ?>

        </div>
    </div>

</section>