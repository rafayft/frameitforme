<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Add Frame Collection</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/portfolio" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/portfolio/add', array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Name *</label>
                        <div class="col-sm-6">
                            <input type="text" autocomplete="off" class="form-control" name="name"
                                   value="<?php if (isset($_POST['name'])) {
                                       echo $_POST['name'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Short Content *</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="short_content"
                                      style="height:100px;"><?php if (isset($_POST['short_content'])) {
                                    echo $_POST['short_content'];
                                } ?></textarea>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="cost"
                                   value="<?php if (isset($_POST['cost'])) {
                                       echo $_POST['cost'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Dimension</label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="dimension"
                                   value="<?php if (isset($_POST['dimension'])) {
                                       echo $_POST['dimension'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Collection</label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="collection"
                                   value="<?php if (isset($_POST['collection'])) {
                                       echo $_POST['collection'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Shipping</label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="shipping"
                                   value="<?php if (isset($_POST['shipping'])) {
                                       echo $_POST['shipping'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">International</label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="international"
                                   value="<?php if (isset($_POST['international'])) {
                                       echo $_POST['international'];
                                   } ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Select Category *</label>
                        <div class="col-sm-4">
                            <select name="category_id" class="form-control select2 getsubcategory">
                                <option value="">Select Category</option>
                                <?php
                                foreach ($all_photo_category as $row) {
                                    ?>
                                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group subcategory">

                    </div>
                    <h3 class="seo-info">Featured Photo</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Featuerd Photo *</label>
                        <div class="col-sm-9" style="padding-top:5px">
                            <input type="file" name="photo">(Only jpg, jpeg, gif and png are allowed)
                        </div>
                    </div>
                    <h3 class="seo-info">Other Photos</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Other Photos</label>
                        <div class="col-sm-6" style="padding-top:5px">
                            <table id="PhotosTable" style="width:100%;">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="upload-btn">
                                            <input type="file" name="photos[]">
                                        </div>
                                    </td>
                                    <td style="width:28px;"><a href="javascript:void()"
                                                               class="Delete btn btn-danger btn-xs">X</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-2" style="padding-top:5px">
                            <input type="button" id="btnAddNew" value="Add Item"
                                   style="margin-bottom:10px;border:0;color: #fff;font-size: 14px;border-radius:3px;"
                                   class="btn btn-warning btn-xs">
                        </div>
                    </div>
                    <h3 class="seo-info">Theme Color</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Theme Color</label>
                        <div class="col-sm-6" style="padding-top:5px">
                            <table id="Tablecolor" style="width:100%;">
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="upload-btn">
                                            <input type="text" name="color[]" class="form-control"
                                                   placeholder="Color Name">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="upload-btn">
                                            <input type="text" name="colorcode[]" class="form-control"
                                                   placeholder="Color Code">
                                        </div>
                                    </td>
                                    <td style="width:28px;"><a href="javascript:void()"
                                                               class="Delete btn btn-danger btn-xs">X</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-2" style="padding-top:5px">
                            <input type="button" id="btnAddNewcolor" value="Add Item"
                                   style="margin-bottom:10px;border:0;color: #fff;font-size: 14px;border-radius:3px;"
                                   class="btn btn-warning btn-xs">
                        </div>
                    </div>
                    <h3 class="seo-info">SEO Information</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Meta Title</label>
                        <div class="col-sm-6">
                            <input type="text" autocomplete="off" class="form-control" name="meta_title"
                                   value="<?php if (isset($_POST['meta_title'])) {
                                       echo $_POST['meta_title'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Meta Keyword</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="meta_keyword"
                                      style="height:100px;"><?php if (isset($_POST['meta_keyword'])) {
                                    echo $_POST['meta_keyword'];
                                } ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Meta Description</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" name="meta_description"
                                      style="height:100px;"><?php if (isset($_POST['meta_description'])) {
                                    echo $_POST['meta_description'];
                                } ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
                            <input type="hidden" class="csrf"
                                   name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                   value="<?php echo $this->security->get_csrf_hash(); ?>">
                        </div>
                    </div>

                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>