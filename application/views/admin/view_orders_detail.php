<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Order Detail</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/orders" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/orders/update/' . $orders['id'], array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">
                    <h3 class="seo-info">Order Information</h3>
                    <?php /*if ($orders['status'] == 1) { */?>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Status *</label>
                            <div class="col-sm-6">
                                <select name="status" class="form-control select2">
                                    <option value="4" <?php if ($orders['status'] == '4') {
                                        echo 'selected';
                                    } ?>>Delivered
                                    </option>
                                    <option value="1" <?php if ($orders['status'] == '1') {
                                        echo 'selected';
                                    } ?>>Not Delivered
                                    </option>
                                    <option value="5" <?php if ($orders['status'] == '5') {
                                        echo 'selected';
                                    } ?>>Cancel
                                    </option>
                                    <option value="6" <?php if ($orders['status'] == '6') {
                                        echo 'Hold';
                                    } ?>>Hold
                                    </option>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label"></label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
                            </div>
                        </div>
                    <?php /*} */?>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 style="color : #000 !important">Customer Name
                                : <?php echo $orders['customer_name']; ?></h3>
                            <h3 style="color : #000 !important">Customer Email : <?php echo $orders['email']; ?></h3>
                            <h3 style="color : #000 !important">Customer Phone
                                : <?php echo $orders['phonenumber']; ?></h3>
                            <h3 style="color : #000 !important">Billing Address
                                : <?php echo $orders['addressone'] . ' ' . $orders['state'] . ' ' . $orders['country']; ?></h3>
                            <h3 style="color : #000 !important">Shipping Address
                                : <?php echo $orders['shippingaddressone'] . ' ' . $orders['shippingstate'] . ' ' . $orders['shippingcountry']; ?></h3>
                        </div>
                        <div class="col-md-6">
                            <h3 style="color : #000 !important">Subtotal : <?php echo $orders['subtotal']; ?></h3>
                            <h3 style="color : #000 !important">Discount : <?php echo $orders['discount']; ?></h3>
                            <h3 style="color : #000 !important">Shipping Amount
                                : <?php echo $orders['shippingamount']; ?></h3>
                            <h3 style="color : #000 !important">Total Amount
                                : <?php echo $orders['totalamount']; ?></h3>

                        </div>
                    </div>
                    <?php
                    $frames = $this->db->get_where('tbl_order_frame', array('order_id' => $orders['id']));
                    foreach ($frames->result_array() as $frame) {
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="<?php echo $frame['image']; ?>" class="img-responsive">
                            </div>
                            <div class="col-md-6">
                                <div class="row ">
                                    <?php
                                    $frameimage = $this->db->get_where('tbl_order_frame_images', array('frame_id' => $frame['orderframe_id']));
                                    foreach ($frameimage->result_array() as $framerow) {
                                        ?>
                                        <div class="col-md-3">
                                            <a href="<?php echo base_url() . 'public/uploads/' . $framerow['image']; ?>"
                                               target="_target"><img
                                                        src="<?php echo base_url() . 'public/uploads/' . $framerow['image']; ?>"
                                                        class="img-responsive" style="margin-bottom : 10px"></a>
                                        </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center" style="margin-top: 20px;">
                                <a href="<?= base_url() . 'admin/orders/downloadzip/?frame_id=' . $frame['orderframe_id'] ?>" class="btn btn-warning btn-lg"><i class="fa fa-download"></i> Download Zip</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure want to delete this item?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>