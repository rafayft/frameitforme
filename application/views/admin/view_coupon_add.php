<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin');
}
?>
<section class="content-header">
    <div class="content-header-left">
        <h1>Add Promo Code</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/coupon" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php if ($error): ?>
                <div class="callout callout-danger">
                    <p>
                        <?php echo $error; ?>
                    </p>
                </div>
            <?php endif; ?>

            <?php if ($success): ?>
                <div class="callout callout-success">
                    <p><?php echo $success; ?></p>
                </div>
            <?php endif; ?>

            <?php echo form_open_multipart(base_url() . 'admin/coupon/add', array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Code <span>*</span></label>
                        <div class="col-sm-4">
                            <input type="text" autocomplete="off" class="form-control" name="code"
                                   value="<?php if (isset($_POST['code'])) {
                                       echo $_POST['code'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Expiry Date <span>*</span></label>
                        <div class="col-sm-4">
                            <input type="date" autocomplete="off" class="form-control" name="expirydate"
                                   value="<?php if (isset($_POST['expirydate'])) {
                                       echo $_POST['expirydate'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Amount <span>*</span></label>
                        <div class="col-sm-4">
                            <input type="number" autocomplete="off" class="form-control" name="amount"
                                   value="<?php if (isset($_POST['amount'])) {
                                       echo $_POST['amount'];
                                   } ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>