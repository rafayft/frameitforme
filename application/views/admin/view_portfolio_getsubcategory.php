<label for="" class="col-sm-2 control-label">Select Sub Category *</label>
<div class="col-sm-4">
    <select name="subcategory_id" class="form-control select2">
        <option value="">Select Sub Category</option>
        <?php
        foreach ($all_photo_subcategory as $row) {
            ?>
            <option value="<?php echo $row['subcategory_id']; ?>"><?php echo $row['subcategoryname']; ?></option>
            <?php
        }
        ?>
    </select>
</div>