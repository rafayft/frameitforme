<?php
if (!$this->session->userdata('id')) {
    redirect(base_url() . 'admin/login');
}
?>

<section class="content-header">
    <div class="content-header-left">
        <h1>Add Frame Collection</h1>
    </div>
    <div class="content-header-right">
        <a href="<?php echo base_url(); ?>admin/frame" class="btn btn-primary btn-sm">View All</a>
    </div>
</section>


<section class="content">

    <div class="row">
        <div class="col-md-12">

            <?php
            if ($this->session->flashdata('error')) {
                ?>
                <div class="callout callout-danger">
                    <p><?php echo $this->session->flashdata('error'); ?></p>
                </div>
                <?php
            }
            if ($this->session->flashdata('success')) {
                ?>
                <div class="callout callout-success">
                    <p><?php echo $this->session->flashdata('success'); ?></p>
                </div>
                <?php
            }
            ?>

            <?php echo form_open_multipart(base_url() . 'admin/frame/add', array('class' => 'form-horizontal')); ?>
            <div class="box box-info">
                <div class="box-body">


                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Select Category *</label>
                        <div class="col-sm-4">
                            <select name="category_id" class="form-control select2">
                                <?php
                                foreach ($all_photo_category as $row) {
                                    ?>
                                    <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Price</label>
                        <div class="col-sm-4">
                            <input type="number" name="price" placeholder="" class="form-control"></input>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Weight</label>
                        <div class="col-sm-4">
                            <select name="weight" class="form-control select2">

                                <option value="1">1 KG</option>
                                <option value="2">2 KG</option>
                                <option value="3">3 KG</option>

                            </select>
                        </div>
                    </div>
                    <h3 class="seo-info">Featured Photo</h3>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Featuerd Photo *</label>
                        <div class="col-sm-9" style="padding-top:5px">
                            <input type="file" name="photo">(Only jpg, jpeg, gif and png are allowed)
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"></label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-success pull-left" name="form1">Submit</button>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>

</section>