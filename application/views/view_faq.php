<!-- Faqs Page Data Starts Here -->
<section class="faqs-page">
    <div class="container">
        <div class="faq-heading">
            <h5><?php echo $page_faq['faq_heading']; ?> </h5>
            <?php echo $page_faq['faq_description']; ?>
        </div>


        <div class="faqs-questions">


            <div class="row">
                <?php foreach ($faqs as $row) { ?>
                    <div class="col-md-4 col-lg-4 col-sm-6 col-xs-6">
                        <div class="faq-box">
                            <h5><?php echo $row['faq_title']; ?></h5>
                            <?php echo $row['faq_content']; ?>
                        </div>
                    </div>

                <?php } ?>
            </div>

        </div>


    </div>
</section>


<section class="contact-call">
    <div class="container">
        <p>STILL HAVE QUESTIONS?</p>
        <h5><a href="<?php echo base_url(); ?>contact"> Get in touch </a> and we'll help you get started. </h5>
        <a href="<?php echo base_url(); ?>contact"> CONTACT US</a>
    </div>
</section>

