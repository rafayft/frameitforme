<?php


defined('BASEPATH') OR exit('No direct script access allowed');


?>


<?php


$error_message = '';


$success_message = '';


?>


<!DOCTYPE html>


<html class="no-js" lang="en">


<head>


    <!-- Meta Tags -->


    <meta name="viewport" content="width=device-width,initial-scale=1.0"/>


    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>


    <?php


    $CI =& get_instance();


    $CI->load->model('Model_common');


    $language = $CI->Model_common->get_language_data();


    foreach ($language as $lang) {


        define($lang['name'], $lang['value']);


    }


    $class_name = '';


    $segment_2 = 0;


    $segment_3 = 0;


    $class_name = $this->router->fetch_class();


    $segment_2 = $this->uri->segment('2');


    $segment_3 = $this->uri->segment('3');


    if ($class_name == 'home') {


        echo '<meta name="description" content="' . $page_home['meta_description'] . '">';


        echo '<meta name="keywords" content="' . $page_home['meta_keyword'] . '">';


        echo '<title>' . $page_home['title'] . '</title>';


    }


    if ($class_name == 'about') {


        echo '<meta name="description" content="' . $page_about['md_about'] . '">';


        echo '<meta name="keywords" content="' . $page_about['mk_about'] . '">';


        echo '<title>' . $page_about['mt_about'] . '</title>';


    }


    if ($class_name == 'faq') {


        echo '<meta name="description" content="' . $page_faq['md_faq'] . '">';


        echo '<meta name="keywords" content="' . $page_faq['mk_faq'] . '">';


        echo '<title>' . $page_faq['mt_faq'] . '</title>';


    }


    if ($class_name == 'team') {


        echo '<meta name="description" content="' . $page_team['md_team'] . '">';


        echo '<meta name="keywords" content="' . $page_team['mk_team'] . '">';


        echo '<title>' . $page_team['mt_team'] . '</title>';


    }


    if ($class_name == 'team_member') {


        $single_team_member = $this->Model_team_member->team_member_detail($segment_2);


        echo '<meta name="description" content="' . $single_team_member['meta_description'] . '">';


        echo '<meta name="keywords" content="' . $single_team_member['meta_keyword'] . '">';


        echo '<title>' . $single_team_member['meta_title'] . '</title>';


    }


    if ($class_name == 'testimonial') {


        echo '<meta name="description" content="' . $page_testimonial['md_testimonial'] . '">';


        echo '<meta name="keywords" content="' . $page_testimonial['mk_testimonial'] . '">';


        echo '<title>' . $page_testimonial['mt_testimonial'] . '</title>';


    }


    if ($class_name == 'contact') {


        echo '<meta name="description" content="' . $page_contact['md_contact'] . '">';


        echo '<meta name="keywords" content="' . $page_contact['mk_contact'] . '">';


        echo '<title>' . $page_contact['mt_contact'] . '</title>';


    }


    if ($class_name == 'search') {


        echo '<meta name="description" content="' . $page_search['md_search'] . '">';


        echo '<meta name="keywords" content="' . $page_search['mk_search'] . '">';


        echo '<title>' . $page_search['mt_search'] . '</title>';


    }


    if ($class_name == 'terms') {


        echo '<meta name="description" content="' . $page_term['md_term'] . '">';


        echo '<meta name="keywords" content="' . $page_term['mk_term'] . '">';


        echo '<title>' . $page_term['mt_term'] . '</title>';


    }


    if ($class_name == 'shipment') {


        echo '<meta name="description" content="' . $page_shipment['md_shipment'] . '">';


        echo '<meta name="keywords" content="' . $page_shipment['mk_shipment'] . '">';


        echo '<title>' . $page_shipment['mt_shipment'] . '</title>';


    }


    if ($class_name == 'refund') {


        echo '<meta name="description" content="' . $page_refund['md_refund'] . '">';


        echo '<meta name="keywords" content="' . $page_refund['mk_refund'] . '">';


        echo '<title>' . $page_refund['mt_refund'] . '</title>';


    }


    if ($class_name == 'disclaimer') {


        echo '<meta name="description" content="' . $page_disclaimer['md_disclaimer'] . '">';


        echo '<meta name="keywords" content="' . $page_disclaimer['mk_disclaimer'] . '">';


        echo '<title>' . $page_disclaimer['mt_disclaimer'] . '</title>';


    }


    if ($class_name == 'privacy') {


        echo '<meta name="description" content="' . $page_privacy['md_privacy'] . '">';


        echo '<meta name="keywords" content="' . $page_privacy['mk_privacy'] . '">';


        echo '<title>' . $page_privacy['mt_privacy'] . '</title>';


    }


    if ($class_name == 'pricing') {


        echo '<meta name="description" content="' . $page_pricing['md_pricing'] . '">';


        echo '<meta name="keywords" content="' . $page_pricing['mk_pricing'] . '">';


        echo '<title>' . $page_pricing['mt_pricing'] . '</title>';


    }


    if ($class_name == 'photo_gallery') {


        echo '<meta name="description" content="' . $page_photo_gallery['md_photo_gallery'] . '">';


        echo '<meta name="keywords" content="' . $page_photo_gallery['mk_photo_gallery'] . '">';


        echo '<title>' . $page_photo_gallery['mt_photo_gallery'] . '</title>';


    }


    if ($class_name == 'kids_collection') {


        if ($segment_3 == 0) {


            echo '<meta name="description" content="' . $page_portfolio['md_portfolio'] . '">';


            echo '<meta name="keywords" content="' . $page_portfolio['mk_portfolio'] . '">';


            echo '<title>' . $page_portfolio['mt_portfolio'] . '</title>';


        } else {


            $single_service = $this->Model_portfolio->get_portfolio_detail($segment_3);


            echo '<meta name="description" content="' . $single_service['meta_description'] . '">';


            echo '<meta name="keywords" content="' . $single_service['meta_keyword'] . '">';


            echo '<title>' . $single_service['meta_title'] . '</title>';


        }


    }


    if ($class_name == 'category') {


        $single_category = $this->Model_category->category_by_id($segment_2);


        echo '<meta name="description" content="' . $single_category['meta_description'] . '">';


        echo '<meta name="keywords" content="' . $single_category['meta_keyword'] . '">';


        echo '<title>' . $single_category['meta_title'] . '</title>';


    }


    if ($class_name == 'news') {


        if ($segment_3 == 0) {


            echo '<meta name="description" content="' . $page_news['md_news'] . '">';


            echo '<meta name="keywords" content="' . $page_news['mk_news'] . '">';


            echo '<title>' . $page_news['mt_news'] . '</title>';


        } else {


            $news_single_item = $this->Model_news->news_detail($segment_3);


            echo '<meta name="description" content="' . $news_single_item['meta_description'] . '">';


            echo '<meta name="keywords" content="' . $news_single_item['meta_keyword'] . '">';


            echo '<title>' . $news_single_item['meta_title'] . '</title>';


            $og_id = $news_single_item['news_id'];


            $og_photo = $news_single_item['photo'];


            $og_title = $news_single_item['news_title'];


            $og_description = $news_single_item['news_content_short'];


            echo '<meta property="og:title" content="' . $og_title . '">';


            echo '<meta property="og:type" content="website">';


            echo '<meta property="og:url" content="' . base_url() . 'news/view/' . $og_id . '">';


            echo '<meta property="og:description" content="' . $og_description . '">';


            echo '<meta property="og:image" content="' . base_url() . 'public/uploads/' . $og_photo . '">';


        }


    }


    if ($class_name == 'event') {


        if ($segment_3 == 0) {


            echo '<meta name="description" content="' . $page_event['md_event'] . '">';


            echo '<meta name="keywords" content="' . $page_event['mk_event'] . '">';


            echo '<title>' . $page_event['mt_event'] . '</title>';


        } else {


            $event_single_item = $this->Model_event->event_detail($segment_3);


            echo '<meta name="description" content="' . $event_single_item['meta_description'] . '">';


            echo '<meta name="keywords" content="' . $event_single_item['meta_keyword'] . '">';


            echo '<title>' . $event_single_item['meta_title'] . '</title>';


            $og_id = $event_single_item['event_id'];


            $og_photo = $event_single_item['photo'];


            $og_title = $event_single_item['event_title'];


            $og_description = $event_single_item['event_content_short'];


            echo '<meta property="og:title" content="' . $og_title . '">';


            echo '<meta property="og:type" content="website">';


            echo '<meta property="og:url" content="' . base_url() . 'event/view/' . $og_id . '">';


            echo '<meta property="og:description" content="' . $og_description . '">';


            echo '<meta property="og:image" content="' . base_url() . 'public/uploads/' . $og_photo . '">';


        }


    } else {


        echo '<title>Frame It For Me</title>';


    }


    ?>


    <!-- Favicon -->


    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/uploads/<?php echo $setting['favicon']; ?>">


    <!-- Stylesheets -->


    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/animate.css">


    <!-- Bootstrap Grids -->


    <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">


	<!-- Custom Stylings -->


    <link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">


    <!-- Font Awesome Icons -->


    <link href="<?php echo base_url(); ?>web-fonts-with-css/css/fontawesome-all.min.css" rel="stylesheet">


    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/uploads/<?php echo $setting['favicon']; ?>">

    <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-3.2.1.min.js"></script>
    <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

</head>


<body class="home-body">


<?php echo $comment['code_body']; ?>


<!-- Header Starts Here -->


<header>


    <div class="container">


        <div class="logo">


            <a href="<?php echo base_url(); ?>">


                <img src="<?php echo base_url(); ?>public/uploads/<?php echo $setting['logo']; ?>">


            </a>


        </div>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">


            <div class="navbar-handler">


                <img src="images/hamburger.png">


            </div>


        </button>


        <nav class="navbar navbar-expand-lg navbar-light bg-light">


            <div class="navbar-custom2 navbar-collapse" id="navbarNav">


                <div class="navbar-nav">


                    <div class="menu-item <?= ($class_name == 'home')?" active":"" ?>">


                        <a href="<?php echo base_url(); ?>"> Home </a>


                    </div>


                    <div class="menu-item <?= ($class_name == 'howitwork')?" active":"" ?>">


                        <a href="<?php echo base_url(); ?>howitwork"> How it works </a>


                    </div>


                    <div class="menu-item <?= ($class_name == 'frames')?" active":"" ?>">


                        <a href="<?php echo base_url(); ?>frames"> Our Frames </a>


                    </div>


                    <div class="menu-item <?= ($class_name == 'kids_collection')?" active":"" ?>">


                        <a href="<?php echo base_url(); ?>kids_collection"> Kids </a>


                    </div>

                    <div class="menu-item <?= ($class_name == 'contact')?" active":"" ?>">
                        <a href="<?php echo base_url(); ?>contact"> Contact Us </a>
                    </div>


                    <?php


                    if ($this->session->userdata('logged_in')) {


                        ?>


                        <div class="menu-login">


                            <a class=" <?= ($class_name == 'profile')?" active":"" ?>" href="<?php echo base_url(); ?>profile"> My Account </a>


                            <a class="<?= ($class_name == 'login/logout')?" active":"" ?>" href="<?php echo base_url(); ?>login/logout"> Logout</a>


                        </div>


                    <?php } else {


                        ?>


                        <div class="menu-login">


                            <a class="<?= ($class_name == 'login')?" active":"" ?>" href="<?php echo base_url(); ?>login"> Login </a>


                            <a class="<?= ($class_name == 'register')?" active":"" ?>" href="<?php echo base_url(); ?>register"> Sign Up</a>


                        </div>


                        <?php


                    }


                    ?>


                    <div class="menu-started">


                        <a class="<?= ($class_name == 'frame_layout')?" active":"" ?>" href="<?php echo base_url() . 'frame_layout'; ?>"> Get Started</a>


                    </div>

                   


                </div>


            </div>


        </nav>
 <div class="shopping_cart">
     <?php $cart = $this->session->userdata('cart'); ?>
     <a href="<?php echo base_url(); ?>Cart/index"><span class="badge badge-secondary badge-pill" style="    margin-left: 19px;margin-top: -22px;"><?php echo !empty($cart)>0?count($cart):""?></span></a>
 </div>

    </div>


</header>


<div class="header-empty">


</div>


<!-- Header Ends Here -->


<!-- Header Starts Here -->


<!--<header class="home-header">


	<div class="container">


	<div class="logo">


		<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/uploads/<?php echo $setting['logo']; ?>" alt="Logo"></a>


	</div>


	<div class="hamburger-btn">


	<a class="menu-open"> <img src="<?php echo base_url(); ?>images/hamburger.png"> </a>


	</div>


	<div class="navbar-custom">


	<?php


if ($this->session->userdata('logged_in')) {


    echo '<p> <a href="' . base_url() . 'profile"> My Account </a> </p>';


    echo '<p> <a href="' . base_url() . 'cart"> My Cart </a> </p>';


} else {


    echo '<p> <a href="' . base_url() . 'login"> Log in </a> </p>';


}


?>	


	


	<p> <a href=""> Shop </a> </p>


	<p> <a href="<?php echo base_url(); ?>howitwork"> How it works </a></p>


	<p> <a href="#"> Kids </a></p>


	<p> <a href="#footer-frame"> Subscribe </a></p>


	<p> <a href="<?php echo base_url(); ?>contact"> Contact Us </a></p>


	<p> <a href="<?php echo base_url(); ?>faq"> FAQs</a></p>


	</div>


	</div>


</header>-->


<!-- Header Ends Here -->