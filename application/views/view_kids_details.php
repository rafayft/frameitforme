<input type="hidden" name="frameweight" class="frameweight" value="<?php echo $portfolio['cost']; ?>"></input>
<section class="kids-collection-details">
    <div class="container">

        <div class="row">

            <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12">
                <div class="all-frames convertedfrmekid" id="convertedfrmekid">
                    <?php
                    if (!empty($portfolio_photo)) {
                        foreach ($portfolio_photo as $row) {
                            ?>
                            <div class="col-md-4">
                                <div class="black-frame">
                                    <img src="<?php echo base_url(); ?>public/uploads/portfolio_photos/<?php echo $row['photo']; ?>">

                                </div>
                            </div>

                        <?php }
                    } ?>


                </div>
            </div>


            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                <div class="collection-detail-text">
                    <h5><?php echo $portfolio['name']; ?></h5>
                    <h6>AED <?php echo $portfolio['cost']; ?></h6>
                    <p><?php echo $portfolio['short_content']; ?></p>
                    <?php
                    $frames = $this->db->get_where('tbl_portfolio_color', array('portfolio_id' => $portfolio['id']));
                    if ($frames->num_rows() > 0) {

                        ?>
                        <h6>FRAME COLOR</h6>
                        <div class="frame-det-color">
                            <div class="col-md-2 text-center p-0">
                                <span class="black">&nbsp;</span>
                                <div class="color-name" >SILVER</div>
                            </div>
                            <div class="col-md-2 text-center p-0">
                                <span class="white">&nbsp;</span>
                                <div class="color-name" style="display: none">WHITE</div>
                            </div>
                            <div class="col-md-2 text-center p-0">
                                <span class="brown">&nbsp;</span>
                                <div class="color-name" style="display: none">GOLD</div>
                            </div>
                            <div class="col-md-2 text-center p-0">
                                <span class="grey">&nbsp;</span>
                                <div class="color-name" style="display: none">BROWN</div>
                            </div>
                        </div>
                    <?php } ?>
                    <form class="checkoutnewform">
                        <h1>QUANTITY</h1>
                        <select name="quantity">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                        </select>
                        <input type="hidden" name="image" class="convertedimage" value=""></input>
                        <input type="hidden" name="shipmentamount" class="shipmentamount" value=""></input>
                        <input type="hidden" name="name" class="proname"
                               value="<?php echo $portfolio['name']; ?>"></input>
                        <input type="hidden" name="price" class="proprice"
                               value="<?php echo $portfolio['cost']; ?>"></input>
                        <input type="hidden" class="<?php echo $this->security->get_csrf_token_name(); ?>"
                               name="<?php echo $this->security->get_csrf_token_name(); ?>"
                               value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div id="render"></div>
                        <input type="hidden" name="product_id" value="<?php echo $this->uri->segment(3); ?>"></input>
                        <input type="hidden" name="description" value="<?php echo $portfolio['collection']; ?>"></input>
                        <input type="hidden" name="frameweight" class="frameweight"
                               value="<?php echo $portfolio['weight']; ?>"></input>
                        <button type="button" class="addtocartkids">ADD TO CART</button>

                    </form>
                </div>
            </div>

        </div>

    </div>
</section>


<section class="gallery-dimensions">
    <div class="container">
        <div class="row">


            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 border-right">
                <div class="gallery-dimensions-box">
                    <?php if ($portfolio['collection'] != '') { ?>
                        <h5>COLLECTION</h5>
                        <p><?php echo $portfolio['collection']; ?> </p>
                    <?php } ?>
                    <?php if ($portfolio['shipping'] != '') { ?>
                        <h5>SHIPPING</h5>
                        <b><?php echo $portfolio['shipping']; ?> </b>
                    <?php } ?>
                </div>
            </div>

            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <div class="gallery-dimensions-box">
                    <?php if ($portfolio['dimension'] != '') { ?>
                        <h5>FULL FRAME DIMENSIONS</h5>
                        <p><?php echo $portfolio['dimension']; ?></p>
                    <?php } ?>
                    <?php if ($portfolio['international'] != '') { ?>
                        <h5>INTERNATIONAL</h5>
                        <b>Shipping outside the UAE? <a href="<?php echo base_url(); ?>shipment">See our standard shipping rates</a></b>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</section>

<section>
    <div class="container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php
                $kids_collection = $this->db->get_where('tbl_portfolio', array('category_id' => 2))->result_array();
                $i = 0;
                ?>
                <?php
                foreach ($kids_collection as $kidsrow) {
                    $row = $i++;
                    ?>
                    <div class="item <?php if ($row == 1) {
                        echo 'active';
                    } ?>">
                        <div class="myCarousel_gallery">
                            <?php
                            $pics = $this->db->get_where('tbl_portfolio_photo', array('portfolio_id' => $kidsrow['id']));
                            $picscount = $pics->num_rows();
                            if ($picscount == 3) {
                                $cols = 'col-md-4 col-lg-4 col-sm-4 col-xs-6';
                            } else if ($picscount == 6) {
                                $cols = 'col-md-2 col-lg-2 col-sm-2 col-xs-3';
                            } else {
                                $cols = 'col-md-6 col-lg-6 col-sm-6 col-xs-6';
                            }
                            foreach ($pics->result_array() as $pic) {
                                ?>

                                <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">
                                    <img src="<?php echo base_url(); ?>public/uploads//portfolio_photos/<?php echo $pic['photo']; ?>">
                                </div>
                            <?php } ?>

                            <div class="collection-btn"><a
                                        href="<?php echo base_url(); ?>/kids_collection/view/<?php echo $kidsrow['id']; ?>">
                                    SHOP </a></div>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>


<!--

<section class="similar-images-slider">
<div class="similar-images-head">
<h5>SIMILAR IMAGES YOU MIGHT LIKE</h5>
</div>


<div class="similar-images-data">

<div class="similar-image-main">
<img src="images/kids1.jpg">
<h5>Andersen Colak</h5>
</div>

<div class="similar-image-main">
<img src="images/kids2.jpg">
<h5>Sara Shakshouk</h5>
</div>

<div class="similar-image-main">
<img src="images/kids3.jpg">
<h5>Sara Shakshouk</h5>
</div>


<div class="similar-image-main">
<img src="images/image-gallery4.jpg">
<h5>Nadia Chahin</h5>
</div>

</div>

<div class="similar-detail">
<h5>The Little Astronaut</h5>
</div>


</section>

-->


