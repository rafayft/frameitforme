<div class="multisteps-form">
    <form class="frameform multisteps-form__form">
        <input type="hidden" class="<?php echo $this->security->get_csrf_token_name(); ?>"
               name="<?php echo $this->security->get_csrf_token_name(); ?>"
               value="<?php echo $this->security->get_csrf_hash(); ?>">

        <?php $id = $this->input->get('catid'); ?>
        <section class="all-steps multisteps-form__panel js-active" data-animation="scaleIn">
            <div class="container">
                <div class="row h-100 align-items-center">
                    <div class="col-md-12">
                        <div class="row multisteps-form__form">
                            <?php if ($id == '') { ?>
                                <?php $this->load->view("partials/steps/step1.php"); ?>
                            <?php } ?>
                            <?php $this->load->view("partials/steps/step2.php"); ?>
                            <?php $this->load->view("partials/steps/step3.php"); ?>
                            <?php $this->load->view("partials/steps/step4.php"); ?>
                            <?php $this->load->view("partials/steps/step5.php"); ?>
                            <?php $this->load->view("partials/steps/step6.php"); ?>
                            <?php $this->load->view("partials/steps/step8.php"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="next-previous">
                    <div class="next-buttons">
                        <a href="javascript:void(0)" class="btn btn-secondary previousbtn" data-ids="">Previous</a>
                        <a href="javascript:void(0)" class="btn btn-secondary nextbtn" data-ids="">Continue</a>
                    </div>
                </div>
            </div>

        </section>
    </form>
</div>

<div class="modal fade modal-crop" id="cropImagePop" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="upload-demo" class="center-block"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link pl-3 pr-3" data-dismiss="modal">Skip</button>
                <button type="button" id="cropImageBtn" class="btn btn-secondary pl-5 pr-5">Save</button>
            </div>
        </div>
    </div>
</div>

<div style="background: none" class="modal fade" id="uploadProgress" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div ><strong>Uploading <span>0</span>%</strong></div>
            </div>
        </div>
    </div>
</div>

<div style="background: none;" class="modal fade" id="resolution" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document" style="height: 150px;    width: 430px;">
        <div class="modal-content">
            <div class="modal-body">
                <h3>Low Resolution</h3>
                <p>
                    Image resolution is too low and may not in best printing quality.
                    To ensure better printing quality, please image size is minimum 1000X1000.
                    Do you waht to use this image anyway?
                </p>
            </div>
        </div>
        <div class="modal-footer" style="background-color: white;">
            <button type="button" id="yesuseimage" class="btn btn-secondary pl-3 pr-3" data-dismiss="modal">YES, USE THIS IMAGE</button>
            <button type="button" id="removeimage" class="btn btn-secondary pl-5 pr-5">NO, REMOVE IMAGE</button>
        </div>
    </div>
</div>

<link href="//foliotek.github.io/Croppie/croppie.css" rel="stylesheet">
<script src="//foliotek.github.io/Croppie/croppie.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        <?php
        $cart = $this->session->userdata('cart');
        $framecart_cart = $this->session->userdata('framecart');
        if ($go != 'continue' && (isset($cart) && !empty($cart) && !empty($framecart_cart))) {
            echo "$('.step1,.hidenewrow,.nextbtn').hide();";
            echo "$('.step6').show();";
        } else {
            echo "$('.step1').show();";
        }
        ?>
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });

        $('.frame-det-color span').on('click', function () {
            let color = $(this).css("background-color");
            $('.frame-holder .frame').css('border-color', color);
            $(".color-name").hide();
            $(this).next().show();
            var container = document.getElementById("layout-details");
            domtoimage.toPng(container)
                .then(function (dataUrl) {
                    $('.previewimage').attr('src', dataUrl);
                })
                .catch(function (error) {
                    console.error('oops, something went wrong!', error);
                });
        });

        $('button.upload-first').one('click', function () {
            $('.item-img:first').trigger('click');
        });

        $('button.addtocartbtn').on('click', function () {
            $("#preview").val(2);
            $('.next-buttons .nextbtn').trigger('click');
        });
        $('button.btnpreview').on('click', function () {
            $("#preview").val(1);
            $('.next-buttons .nextbtn').trigger('click');
        });
    });

    function zoomin(ev) {
        targetZoom = self._currentZoom + 0.005;

        ev.preventDefault();
        _setZoomerVal.call(self, targetZoom);
        change.call(self);
    }

    function zoomout(ev) {
        targetZoom = self._currentZoom - 0.005;

        ev.preventDefault();
        _setZoomerVal.call(self, targetZoom);
        change.call(self);
    }

    // Start upload preview image
    $(".gambar").attr("src", "");
    let $uploadCrop,
        tempFilename,
        rawImg,
        imageId,
        imagePreview,
        imageBox,
        origWidth,
        origHeight,
        width,
        height;

    function readFile(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.upload-demo').addClass('ready');


                rawImg = e.target.result;

                let image = new Image();
                image.src = rawImg;

                image.onload = function () {

                    origWidth = image.width;
                    origHeight = image.height;
                    if(origWidth<1000 || origHeight<1000){
                        $('#resolution').modal('show');
                    }else {
                        $('#cropImagePop').modal('show');
                    }

                    width = imageBox.width() * 6;
                    height = imageBox.height() * 6;

                    if (width > origWidth) {
                        width = origWidth;
                    }

                    if (height > origHeight) {
                        height = origHeight;
                    }

                };
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $('#yesuseimage').on('click', function (ev) {
        $('#resolution').modal('hide');
        $('#cropImagePop').modal('show');
    });

    $('#removeimage').on('click', function (ev) {
        $('#resolution').modal('hide');
        $('#cropImagePop').modal('hide');
    });

    $('#cropImagePop').on('shown.bs.modal', function () {
         width=imageBox.width();
         height=imageBox.height();

        if(height>140 ){
            width = width * 2.5;
            height = height * 2.5;
        }else if(width>250){
            width = width * 2;
            height = height * 2;
        }
        else {
            width = width * 4;
            height = height * 4;
        }
        $uploadCrop = $('#upload-demo').croppie({
            enableExif: true,
            enableResize: true,
            viewport: {
                width: width,
                height: height
            },
        });

        $uploadCrop.croppie('bind', {
            url: rawImg,
        }).then(function () {
        });
    }).on('hide.bs.modal', function () {
        var container = document.getElementById("layout-details");
        domtoimage.toPng(container)
            .then(function (dataUrl) {
                $('.previewimage').attr('src', dataUrl);
            })
            .catch(function (error) {
                console.error('oops, something went wrong!', error);
            });
        $uploadCrop.croppie('destroy');
        $uploadCrop = null;
    });

    $('.layout-details').on('change', '.item-img', function () {
        imageId = $(this).data('id');
        tempFilename = $(this).val();
        imageBox = $(this).parents('.img-box');
        imagePreview = imageBox.find('img.item-img-output');
        $('#cancelCropBtn').data('id', imageId);
        readFile(this);
    });


    $('#cropImageBtn').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'base64',
            format: 'jpeg',
            size: {
                width: width,
                height: height,
            }
        }).then(function (resp) {
            imagePreview.attr('src', resp);
            let $uploadProgress = $('#uploadProgress');
            $uploadProgress.find('.modal-body span').text(0);
            $uploadProgress.modal('show');

            var csrf_test_name = jQuery('.csrf_test_name').val();
            jQuery.ajax({
                type: "POST",
                url: base_url + 'Frame_layout/saveframeimage',
                dataType: 'json',
                data: {
                    imageframedata: rawImg,
                    csrf_test_name: csrf_test_name
                },
                xhr: function () {
                    let myXhr = $.ajaxSettings.xhr();
                    if (myXhr.upload) {
                        myXhr.upload.addEventListener('progress', progress, false);
                    }
                    return myXhr;
                },
                success: function (data) {
                    if (data.success) {
                        imageBox.find('.img-name').val(data.image);
                        imageBox.find('.file.item-img').val(null);
                        $('#cropImagePop').modal('hide');
                    } else {
                        alert('Image not saved on server. Please try again');
                    }

                    $uploadProgress.modal('hide');
                }
            });

            function progress(e) {

                if (e.lengthComputable) {
                    var max = e.total;
                    var current = e.loaded;

                    var Percentage = parseInt((current * 100) / max);
                    $uploadProgress.find('.modal-body span').text(Percentage);


                    if (Percentage >= 100) {
                        $uploadProgress.find('.modal-body span').text(100);
                    }
                }
            }
        });
        $('.first_photo').hide();
        $('#btnpreview').show();
    });
    // End upload preview image
</script>