<?php
$totalcart = $this->session->userdata('totalcart');
//print_r($totalcart);
$order_id = $this->uri->segment(3);
if ($order_id != '') {
    $cart = $this->db->get_where('tbl_order', array('id' => $order_id))->result_array();
} else {

    if ($this->session->userdata('cart') && empty($this->session->userdata('framecart'))) {
        $cart = $this->session->userdata('cart');
    } else if (!empty($this->session->userdata('framecart'))) {
        $cart = $this->session->userdata('cart');
    } else {
        $cart = array();
    }


}
//print_r($cart);
if (is_array($cart) && count($cart) > 0) {
    ?>
    <section class="checkout-page">
        <div class="container">


            <div class="account-head">
                <h5>Your Cart</h5>
                <p>1 FRAME SET</p>
            </div>
            <div class="checkout-item-head">
                <div class="check-col1">
                    ITEM
                </div>
                <div class="check-col1">
                    DESCRIPTION
                </div>
                <div class="check-col1">
                    DETAILS
                </div>
                <div class="check-col1">
                    PRICE
                </div>
            </div>
            <form class="processcheckout" action="<?php echo site_url(); ?>/Cart/checkout/<?php echo $order_id; ?>" method="post">
            <?php
            $subtotal = 0;

            $get_frames = $this->db->get_where('tbl_order_frame', array('order_id' => $order_id));
            $get_subscription = $this->db->get_where('tbl_order_subscription', array('order_id' => $order_id));

            foreach ($cart as $item) {
                //print_r($item);

                if ($order_id != '') {
                    $subtotal = $item['subtotal'];
                    $totalamount = $item['totalamount'];
                    //$shipping = $item['shippingamount'];
                    $promocode = $item['discount'];
                } else {
                    if (!empty($this->session->userdata('framecart'))) {
                        if(is_array($item['price'])) {
                            foreach ($item['price'] as $itemto) {
                                $subtotal += $itemto * $item['quantity'];
                            }
                        }else{
                            $subtotal += $item['price'] * $item['quantity'];
                        }
                        if(is_array($item['price'])) {
                            foreach ($item['id'] as $itemid) {
                                $itemids = $itemid;

                            }
                        }else{
                            $itemids = $item['id'];
                        }

                    } else {
                        $subtotal += $item['price'] * $item['quantity'];
                        $itemids = $item['id'];
                    }

                    $totalamount = $subtotal;
                    $totalcart = $this->session->userdata('totalcart');
                   /* foreach ($totalcart as $tlt) {
                        $shipping = $tlt['shippingamount'];
                    }*/

                    $promocode = 0;
                }
                if ($get_frames->num_rows() > 0 && $order_id != '') {
                    foreach ($get_frames->result_array() as $frame) {
                       // print_r($frame)
                        ?>
                        <div class="checkout-item-data">
                            <div class="check-col1">
                                <img src="<?php echo $frame['image']; ?>">

                            </div>
                            <div class="check-col1">
                                <?php echo $frame['name']; ?>
                            </div>
                            <div class="check-col1">
                                <?php echo $frame['name']; ?>
                                <br/>
                                <br/>
                                <span class="est-delivery"> <b> Estimated Delivery: </b>5-7 business days </span>
                            </div>
                            <div class="check-col1">
                                AED <?php echo $frame['price']; ?>
                            </div>
                            <div class="check-col1">
                                <a class="removerow" data-ids="<?php echo $order_id ?>"> REMOVE </a>
                            </div>
                        </div>

                        <div class="black-line">
                        </div>
                        <?php foreach ($get_subscription->result() as $subscription) {
                            ?>
                            <div class="checkout-item-data no-padding subscriptionamount">
                                <div class="check-col1">
                                    <img src="<?php echo base_url(); ?>images/item2.jpg">
                                </div>
                                <div class="check-col1 subscriptiontitle">
                                    <?php echo $subscription->name; ?>
                                    <input type="hidden" name="subscription" class="subscription"></input>
                                </div>
                                <div class="check-col1 subscriptiondesc">
                                    <?php echo $subscription->subscriptiondesc; ?>
                                    <br/>
                                    <input type="hidden" name="subscriptiondesc"
                                           class="subscriptiondescription"></input>
                                </div>
                                <div class="check-col1 ">
                                    AED <span class="subscriptionval"><?php echo $subscription->price; ?></span>
                                    <input type="hidden" name="subscriptionprice" class="subscriptionprice"></input>
                                </div>
                                <div class="check-col1">
                                    <a> REMOVE </a>
                                </div>
                            </div>
                            <div class="black-line backline2">
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                    }
                } else { ?>

                    <div class="checkout-item-data">
                        <div class="check-col1">
                            <img src="<?php echo is_array($item['photo']) ? $item['photo'][0] : $item['photo']; ?>">

                        </div>
                        <div class="check-col1">
                            <?php echo is_array($item['name']) ? $item['name'][0] : $item['name']; ?>
                        </div>
                        <div class="check-col1">
                            <?php
                            if (!empty($item['description'][0])) {
                                echo is_array($item['description']) ? $item['description'][0] : $item['description'];;
                            } else {
                                echo '-';
                            }
                            ?>
                            <br/>
                            <br/>
                            <span class="est-delivery"> <b> Estimated Delivery: </b>5-7 business days </span>
                        </div>
                        <div class="check-col1">
                            AED <?php echo is_array($item['price']) ? $item['price'][0]*$item['quantity'][0] : $item['price']* $item['quantity']; ?>
                        </div>
                        <div class="check-col1">
                            <?php if (!is_array($item['price'])){ ?>
                            <a data-ids="<?php echo $itemids; ?>" class="deletedraft" href="<?php echo base_url(); ?>Kids_collection/remove/<?php echo $itemids; ?>">
                                REMOVE </a>
                        <?php }else{ ?>
                            <a class="removerow" data-ids="<?php echo $itemids ?>"> REMOVE </a>
                        <?php }?>
                        </div>
                    </div>

                    <div class="black-line">
                    </div>

                <?php } ?>



            <?php } ?>

                <div class="checkout-item-data">
                    <div class="check-col1 empty-width">
                        &nbsp;
                    </div>
                    <div class="check-col1">
                        <input type="checkbox" name="installationservice[]" value="<?php echo $itemids?>"
                               class="installationservice" > Installation Service
                    </div>
                    <div class="check-col1">
                        Frame installation service upon delivery in Dubai only
                    </div>
                    <div class="check-col1">
                        AED 75
                        <input type="hidden" name="installationamount" class="installationamount"
                               value="75">
                    </div>
                </div>

                <div class="checkout-payments">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12" <?php if ($promocode > 0) {
                            echo 'style="visibility:hidden"';
                        } ?>>
                            <div class="promo-form">
                                <h5>PROMO CODE</h5>
                                <form>
                                    <input type="text" placeholder="" name="couponcode" class="promocode">
                                    <input type="button" value="APPLY" name="" class="couponcode">
                                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                                           class="<?php echo $this->security->get_csrf_token_name(); ?>"
                                           value="<?php echo $this->security->get_csrf_hash(); ?>">
                                    <span class="couponresult"></span>
                                </form>

                            </div>
                        </div>

                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-totals">
                                <p>
                                    <b>SUBTOTAL</b>
                                    <span >AED <span class="subtotal"> <?php echo $subtotal; ?></span></span>
                                </p>

                               <!-- <p>
                                    <b>SHIPPING</b>
                                    <span ><?php /*echo $shipping; */?></span>
                                </p>-->
                                <?php if ($promocode > 0) { ?>
                                    <p>
                                        <b>Promo Code</b>
                                        <span class="codeamountplus"><?php echo $promocode; ?></span>
                                    </p>
                                <?php } else {
                                    ?>
                                    <p class="promocodebox">
                                        <b>Promo Code</b>
                                        <span class="codeamountplus">Free</span>
                                    </p>
                                    <?php
                                } ?>
                                <p style="display:none">
                                    <b>VAT</b>
                                    <span>AED 0</span>
                                </p>
                                <input type="hidden" name="totalamountnew" class="totalamount"
                                       value="<?php echo $totalamount; ?>"></input>
                                <input type="hidden" name="promocodevalue" class="promocodevalue"
                                       value="0"></input>
                                <input type="hidden" name="subtotalss" class="subtotalss"
                                       value=<?php echo $subtotal; ?>></input>
                                <input type="hidden" name="installtion" class="installtion"
                                       value="75"></input>
                                <h5>
                                    <b>TOTAL</b>
                                    <span>AED <span class="totalframeamountfinal"><?php echo $totalamount; ?></span></span>
                                </h5>

                            </div>
                        </div>
                    </div>


                    <div class="proceed-checkout">
                        <?php if ($order_id == '') {
                            ?>
                            <a href="<?php echo base_url(); ?>Kids_collection">CONTINUE SHOPPING</a>
                        <?php } ?>

                        <button>PROCEED TO SECURE CHECKOUT</button>


                    </div>

                </div>
            </form>

        </div>
    </section>

<?php } else { ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mb-70">
                <h1 class="mb-70">No Frames In cart</h1>
                <a href="<?= base_url()?>frame_layout" class="btn btn-secondary btn-lg">Get Start Now</a>
            </div>
        </div>
    </div>
<?php } ?>