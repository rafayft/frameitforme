<section class="privacy-page">
    <div class="container">

        <div class="row">

            <?php $this->load->view('view_sidebar'); ?>


            <div class="privacy-policy-right">
                <div class="privacy-policy-head">
                    <h5><?php echo $page_privacy['privacy_heading']; ?></h5>
                </div>

                <div class="privacy-policy-data">
                    <?php echo $page_privacy['privacy_content']; ?>

                </div>

            </div>


        </div>

    </div>
</section>
