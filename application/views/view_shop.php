<section class="collection-page">
    <div class="container">
        <div class="collection-head">
            <h5>Our Frames </h5>
            <p>Design something special for your child's room; Choose between <br/> personalized and fun prints to
                brighten up their rooms.</p>
        </div>


        <!-- Most Speical Day Collection Starts From Here -->
        <?php $kids_collection = $this->db->get_where('tbl_portfolio')->result_array(); ?>
        <?php foreach ($kids_collection as $kidsrow) { ?>
            <div class="collection-cat1">
                <h5><?php echo $kidsrow['name']; ?></h5>
                <div class="row">

                    <?php
                    $pics = $this->db->get_where('tbl_portfolio_photo', array('portfolio_id' => $kidsrow['id']));
                    $picscount = $pics->num_rows();
                    if ($picscount == 3) {
                        $cols = 'col-md-4 col-lg-4 col-sm-4 col-xs-6';
                    } else if ($picscount == 6) {
                        $cols = 'col-md-2 col-lg-2 col-sm-2 col-xs-3';
                    } else {
                        $cols = 'col-md-6 col-lg-6 col-sm-6 col-xs-6';
                    }
                    foreach ($pics->result_array() as $pic) {
                        ?>
                        <div class="<?php echo $cols; ?>">
                            <div class="collection-image">
                                <img src="<?php echo base_url(); ?>public/uploads//portfolio_photos/<?php echo $pic['photo']; ?>">
                            </div>
                        </div>
                    <?php } ?>


                </div>
                <div class="collection-btn"><a href="<?php if ($this->session->userdata('logged_in')) {
                        if ($kidsrow['category_id'] == 1) {
                            echo base_url() . 'shop/views/' . $kidsrow['id'];
                        } else {
                            echo base_url() . 'shop/view/' . $kidsrow['id'];
                        }
                    } else {
                        echo base_url() . 'register';
                    } ?>"> SHOP </a></div>
            </div>
            <!-- Most Speical Day Collection Ends Here -->
        <?php } ?>


    </div>
</section>
