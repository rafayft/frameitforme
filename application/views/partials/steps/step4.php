<div class="fourth-step step4">
    <div class="frame-det-right">
        <div class="custom-frame-head">
            <h5>Preview your wall</h5>
            <p>That's pretty good looking wall. Are you happy with it?</p>
        </div>
        <div id="previewImage"></div>
        <img src="" class="previewimage">
    </div>
    <div class="order-completed">
        <a href="javascript:void()" class="previousicon"> NO, GO BACK </a>
        <a href="javascript:void()" class="checkoutbuton" id="checkoutbtn"> YES, CHECKOUT </a>
    </div>
</div>