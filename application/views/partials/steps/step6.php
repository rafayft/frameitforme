<div class="checkout-page step6">
    <div class="container">
        <div class="account-head">
            <h3 class="multisteps-form__title">Your Frames</h3>
        </div>
        <div class="row_1">
            <div class="checkout-item-head">
                <div class="check-col1">
                    ITEM
                </div>
                <div class="check-col1">
                    DESCRIPTION
                </div>
                <div class="check-col1">
                    DETAILS
                </div>
                <div class="check-col1">
                    PRICE
                </div>
            </div>
            <?php
            $cart = $this->session->userdata('cart');
            $framecart_cart = $this->session->userdata('framecart');
            //print_r($cart);
            $i = 0;
            $total=0;
            if (isset($cart) && !empty($cart) && !empty($framecart_cart)) {

                foreach ($cart as $artrow) {
                    $cartplus = $i++;
                    $total+= $artrow['frameamount'][$cartplus]*$artrow['quantity'][$cartplus];
                    ?>
                    <div class="checkout-item-data">
                        <div class="check-col1">
                            <img src="<?php echo $artrow['photo'][$cartplus]; ?>">
                            <input type="hidden" name="fameimage[]" value="<?php echo $artrow['photo'][$cartplus]; ?>">

                            <?php foreach ($artrow['image'] as $image) { ?>
                                <input type="hidden" name="frameimageall[]" value="<?php echo $image; ?>">
                            <?php } ?>
                        </div>
                        <div class="check-col1 ">
                            <?php echo $artrow['name'][$cartplus]; ?>
                        </div>

                        <div class="check-col1 ">
                            1 Frame (25.5 x 21.5 in) <br/>
                            1 Frame (25.5 x 21.5 in) <br/>
                            1 Frame (25.5 x 21.5 in)
                            <br/>
                            <br/>
                            <span class="est-delivery"> <b> Estimated Delivery: </b>5-7 business days </span>
                        </div>
                        <div class="check-col1">
                            AED <?php echo $artrow['frameamount'][$cartplus]*$artrow['quantity'][$cartplus]; ?>
                            <input type="hidden" name="frameamount[]" value="<?php echo $artrow['frameamount'][$cartplus]; ?>">
                            <input type="hidden" name="famrname[]" value="<?php echo $artrow['name'][$cartplus]; ?>">
                            <input type="hidden" name="quantities" value="<?php echo $artrow['quantity'][$cartplus]; ?>">
                            <input type="hidden" name="id[]" value="<?php echo $artrow['id'][$cartplus]; ?>">
                        </div>
                        <div class="check-col1">
                            <a class="removerow" data-ids="<?php echo $artrow['id'][$cartplus]; ?>"> REMOVE </a>
                        </div>
                    </div>
                    <?php
                }
            } ?>

        <div class="checkout-item-data hidenewrow">
            <div class="check-col1">
                <img src="images/item1.jpg" class="frameimage">
                <input type="hidden" name="fameimage[]" class="fameimages">

            </div>
            <div class="check-col1 framename"></div>

            <div class="check-col1 framedesctiption">
                1 Frame (25.5 x 21.5 in) <br/>
                1 Frame (25.5 x 21.5 in) <br/>
                1 Frame (25.5 x 21.5 in)
                <br/>
                <br/>
                <span class="est-delivery"><?php echo $setting['estimate']; ?></span>
            </div>
            <div class="check-col1">
                AED <span class="totalframeamount"></span>
                <input type="hidden" name="frameamount[]" class="frameamount">
                <input type="hidden" name="famrname[]" class="famrname">
                <input type="hidden" name="id[]" class="layoutid">
            </div>
            <div class="check-col1">
                <a class="removerow" data-ids="1"> REMOVE </a>
            </div>
        </div>
    </div>



    <div class="checkout-item-data iam ">
            <div class="check-col1 empty-width">
                &nbsp;
            </div>
            <div class="check-col1">

                <input type="checkbox" name="installationservice[]" value="75" class="installationservice"
                        > Installation Service
            </div>
            <div class="check-col1">
                Frame installation service upon delivery in Dubai only
            </div>
            <div class="check-col1">
                AED 75
                <input type="hidden" name="installationamount[]" class="installationamount"
                       value="<?php echo $setting['installationamount']; ?>">
                <div class="frameimagesall"></div>
            </div>
        </div>


        <div class="checkout-payments">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <div class="promo-form">
                        <h5>PROMO CODE</h5>

                        <input type="text" placeholder="" class="promocode" name="">
                        <input type="button" class="couponcodetwo" value="APPLY" name="">

                        <div class="couponresult"></div>

                    </div>
                </div>

                <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                    <?php
                    $totalcart = $this->session->userdata('totalcart');

                    if (isset($totalcart) && !empty($cart) && !empty($framecart_cart)) {

                        foreach ($totalcart as $ardartrow) {

                            ?>
                            <div class="checkout-totals">
                                <p>
                                    <b>SUBTOTAL</b>
                                    <span class="subtotal">AED <span
                                                class="subtotal"><?php echo $total?></span></span>
                                    <input type="hidden" class="subtotalss" name="subtotal"
                                           value="<?php echo $ardartrow['subtotal']; ?>">
                                </p>

                                <p>
                                    <input type="hidden" name="shippingamount" class="shippingamount"
                                           value="0">
                                </p>
                                <p>
                                    <b>Promo Code</b>
                                    <span class="">AED <span class="codeamountplus">0</span></span>
                                    <input type="hidden" name="promocode"
                                           value="<?php echo $ardartrow['promoamount']; ?>"
                                           class="promocodevalue">
                                </p>

                                <p style="display:none">
                                    <b>VAT</b>
                                    <span>AED <span
                                                class="vatamount"><?php echo $ardartrow['vatamount']; ?></span></span>
                                    <input type="hidden" name="vatamount" class="vatamounts"
                                           value="<?php echo $ardartrow['vatamount']; ?>">
                                    <input type="hidden" name="weightselected" class="weightselected"
                                           value="<?php echo $ardartrow['weight']; ?>">
                                </p>

                                <h5>
                                    <b>TOTAL</b>
                                    <span class="">AED <span
                                                class="totalframeamountfinal"><?php echo $ardartrow['totalamount'] ?? 0; ?></span></span>
                                    <input type="hidden" name="totalamount" class="totalamount"
                                           value="<?php echo $ardartrow['totalamount'] ?? 0; ?>">
                                </h5>

                            </div>
                        <?php }
                    }?>



                    <input type="hidden" name="initialamount" value="" class="initialamount">
                    <input type="hidden" name="initialamount" value="" class="additionalamount">
                    <input type="hidden" value="" class="subframeid">
                </div>
            </div>

            <div class="proceed-checkout">
                <a href="javascript:void()" class="continueshopping">CONTINUE SHOPPING</a> | <a
                        href="javascript:void()" class="savedraft"> SAVE TO DRAFTS </a>
                <button type="button" name="PROCEED" class="proceed">PROCEED TO SECURE CHECKOUT</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>