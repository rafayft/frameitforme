<div class="print-page step5">
    <div class="container">
        <div class="print-head">
            <h5>Print Subscription </h5>
            <p>Let your wall grow with you as you make new memories.
                Sign up for a subscription package that best suits you.</p>
        </div>
        <div class="print-data">
            <div class="row">
                <?php foreach ($subcription->result() as $subsrow) { ?>
                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 border-right">
                        <div class="subscription-text">
                            <h5><?php echo $subsrow->name; ?></h5>
                            <h6>AED <?php echo $subsrow->price; ?> / YEAR </h6>
                            <p><?php echo $subsrow->description; ?></p>
                            <div class="radio-toolbar">
                                <input type="radio" id="subscription<?php echo $subsrow->id; ?>"
                                       name="subscription" class="layout-select"
                                       value="<?php echo $subsrow->price; ?>"
                                       data-title="<?php echo $subsrow->name; ?>"
                                       data-desc="<?php echo $subsrow->description; ?>">
                                <label for="subscription<?php echo $subsrow->id; ?>" class="print-button1">Select</label>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="print-button">
            <a href="javascript:void()" class="skippackage"> Skip For Now </a>
            <a href="javascript:void()" class="checkoutbuton"> ChECKOUT </a>
        </div>
    </div>
</div>