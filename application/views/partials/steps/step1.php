<div class="first-step step1">
    <div class="multisteps-form__header text-center">
        <h3 class="multisteps-form__title">What layout best suits your space?</h3>
        <p class="multisteps-form__desc">Choose below frame layout you love.</p>
    </div>
    <div class="frames-selection justify-content-center">
        <?php
        foreach ($frame_category as $category) {
            ?>
            <div class="frame-wrap" data-title="<?= $category['category_name']; ?>" data-ids="<?= $category['category_id']; ?>">
                <div class="img-wrap">
                    <img src="<?= base_url() . 'public/uploads/' . $category['photo'] ?>" alt="" class="img-fluid">
                </div>
                <div class="text-center text-wrap">
                    <h4><?= $category['category_name']; ?></h4>
                    <small>AED <?= $category['price']; ?></small>
                </div>
            </div>
        <?php } ?>
        <div style="display: none">
            <button id="dLabel" type="button" class="layoutseelction" value="" data-ids="">
                <span class="buttontext"></span>
            </button>
        </div>
    </div>
</div>