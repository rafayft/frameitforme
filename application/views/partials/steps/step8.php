<div class="second-step step8">
    <div class="frame-det-right">
        <div class="custom-frame-head">
            <h5 class="text-center">Frame Size</h5>
        </div>
        <div class="custom-frame-boxes" style="background:transparent">
            <div class="">
                <div class="row customizebox">
                    <label>X-pos<input type=range id=xpos min=0 value=150 max=950
                                       step=1></label><br>
                    <label>Y-pos<input type=range id=ypos min=0 value=150 max=550
                                       step=1></label><br>
                </div>
            </div>
        </div>
    </div>

</div>