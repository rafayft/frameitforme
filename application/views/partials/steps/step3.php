<div class="back-row topback" style="position: inherit;padding-top: 20px;display: none">
    <button class="btn btn-link js-btn-prev with-arrow d-flex align-items-center previousbtn" type="button" title="Prev"><i class="fa fa-angle-left"></i> Back to layouts</button>
</div>
<div class="second-step step3">
    <div class="custom-frame-head suxtomizeframe">
        <h5 class="text-center">Customize your frame details</h5>
    </div>
    <div class="multisteps-form__content">
        <div class="col-md-8 pr-5 layout-details" id="layout-details"></div>
        <div class="frame-det-right col-md-4">
            <div class="first_photo">
            <p class="multisteps-form__desc">
                Upload your photos by clicking inside the frame, or use the button below.
            </p>
            <button type="button" class="btn btn-secondary btn-lg d-flex align-items-center text-uppercase upload-first">
                <span class="icon mr-3">
                    <?php $this->load->view('partials/svg/upload_img.php'); ?>
                </span>
                <span>Upload your first photo</span>
            </button>
            <hr class="divider">
            </div>
            <div class="frame-price mb-4">
                <h3><strong class="selected-frame-name">Price</strong></h3>
                <h4 class="m-0">AED 790</h4>
            </div>
            <div class="frame-det-color mb-4">
                <h6>Frame Colors </h6>
                <div class="col-md-2 text-center p-0">
                    <span class="black">&nbsp;</span>
                    <div class="color-name" >SILVER</div>
                </div>
                <div class="col-md-2 text-center p-0">
                    <span class="white">&nbsp;</span>
                    <div class="color-name" style="display: none">WHITE</div>
                </div>
                <div class="col-md-2 text-center p-0">
                    <span class="brown">&nbsp;</span>
                    <div class="color-name" style="display: none">GOLD</div>
                </div>
                <div class="col-md-2 text-center p-0">
                    <span class="grey">&nbsp;</span>
                    <div class="color-name" style="display: none">BROWN</div>
                </div>
            </div>

            <div class="frame-qty mb-4">
                <h6>Quantity </h6>
                <div class="number d-flex align-items-center justify-content-center">
                    <span class="minus">-</span>
                    <input type="text" name="quantity" value="1"/>
                    <span class="plus">+</span>
                </div>
            </div>
            <div class=" mb-4" style="display: inline-flex">
            <div class=" mb-2" style="    margin-right: 20px;">
                <button type="button" class="addtocartbtn btn btn-secondary btn-lg text-uppercase pl-5 pr-5">Add to cart</button>
            </div>
            <div class=" mb-2">
                <button type="button" id="btnpreview" style="display: none" class="btnpreview btn btn-secondary btn-lg text-uppercase pl-5 pr-5">Preview &nbsp;&nbsp; <i class="fa fa-eye"></i></button>
            </div>
                <input id="preview" value="1" type="hidden">
            </div>
            <hr class="divider">
            <div class="frame-selections">
                <h6>Frame Details</h6>
                <ul class="list-unstyled p-0 m-0">
                    <li>1x W 25.5 x H 21.5 inches (landscape)</li>
                    <li>7x W 12 x H 10 inches (landscape)</li>
                    <li>2x W 12 x H 21.5 inches (portrait)</li>
                </ul>
            </div>
        </div>
    </div>
</div>