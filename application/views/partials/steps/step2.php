<div class="second-step step2">
    <div class="frame-det-right">
        <div class="multisteps-form__header">
            <h3 class="multisteps-form__title text-center">Select your frame style</h3>
        </div>
        <div class="layoutdynamic frames-selection d-flex flex-wrap justify-content-center"></div>
    </div>
</div>