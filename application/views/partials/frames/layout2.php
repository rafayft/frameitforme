<div class="frame-det-left custom-frame-wrap">
    <div id="custom-frame-boxes">
        <div class="frame-holder">
            <div class="flex-8 margin-right">
                <div class="frame-holder">
                    <div class="frame">
                        <div class="img-box">
                            <span class="upload-img d-flex align-items-center justify-content-center">
                                <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                <input type="hidden" name="frameimageall[]" class="img-name">
                            </span>
                            <label class="cabinet center-block">
                                <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                            </label>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-box">
                            <span class="upload-img d-flex align-items-center justify-content-center">
                                <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                <input type="hidden" name="frameimageall[]" class="img-name">
                            </span>
                            <label class="cabinet center-block">
                                <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                            </label>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-box">
                            <span class="upload-img d-flex align-items-center justify-content-center">
                                <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                <input type="hidden" name="frameimageall[]" class="img-name">
                            </span>
                            <label class="cabinet center-block">
                                <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="frame-holder  m-0">

                    <div class="frame two-pic">
                        <div class="both-img-box">
                            <div class="img-box">
                                <span class="upload-img d-flex align-items-center justify-content-center">
                                    <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                    <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                    <input type="hidden" name="frameimageall[]" class="img-name">
                                </span>
                                <label class="cabinet center-block">
                                    <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                                </label>
                            </div>
                            <div class="img-box">
                                <span class="upload-img d-flex align-items-center justify-content-center">
                                    <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                    <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                    <input type="hidden" name="frameimageall[]" class="img-name">
                                </span>
                                <label class="cabinet center-block">
                                    <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="frame">
                        <div class="img-box">
                            <span class="upload-img d-flex align-items-center justify-content-center">
                                <?php $this->load->view('partials/svg/upload_img.php'); ?>
                                <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                                <input type="hidden" name="frameimageall[]" class="img-name">
                            </span>
                            <label class="cabinet center-block">
                                <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                            </label>
                        </div>
                    </div>
                </div>

            </div>
            <div class="flex-4">
                <div class="frame">
                    <div class="img-box">
                        <span class="upload-img d-flex align-items-center justify-content-center">
                            <?php $this->load->view('partials/svg/upload_img.php'); ?>
                            <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                            <input type="hidden" name="frameimageall[]" class="img-name">
                        </span>
                        <label class="cabinet center-block">
                            <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="frame-holder">
            <div class="frame">
                <div class="img-box">
                    <span class="upload-img d-flex align-items-center justify-content-center">
                        <?php $this->load->view('partials/svg/upload_img.php'); ?>
                        <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                        <input type="hidden" name="frameimageall[]" class="img-name">
                    </span>
                    <label class="cabinet center-block">
                        <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                    </label>
                </div>
            </div>
            <div class="frame two-pic">
                <div class="both-img-box">
                    <div class="img-box">
                        <span class="upload-img d-flex align-items-center justify-content-center">
                            <?php $this->load->view('partials/svg/upload_img.php'); ?>
                            <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                            <input type="hidden" name="frameimageall[]" class="img-name">
                        </span>
                        <label class="cabinet center-block">
                            <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                        </label>
                    </div>
                    <div class="img-box">
                        <span class="upload-img d-flex align-items-center justify-content-center">
                            <?php $this->load->view('partials/svg/upload_img.php'); ?>
                            <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                            <input type="hidden" name="frameimageall[]" class="img-name">
                        </span>
                        <label class="cabinet center-block">
                            <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                        </label>
                    </div>
                </div>
            </div>
            <div class="frame">
                <div class="img-box">
                    <span class="upload-img d-flex align-items-center justify-content-center">
                        <?php $this->load->view('partials/svg/upload_img.php'); ?>
                        <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                        <input type="hidden" name="frameimageall[]" class="img-name">
                    </span>
                    <label class="cabinet center-block">
                        <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                    </label>
                </div>
            </div>
            <div class="frame">
                <div class="img-box">
                    <span class="upload-img d-flex align-items-center justify-content-center">
                        <?php $this->load->view('partials/svg/upload_img.php'); ?>
                        <input type="file" accept="image/*" class="item-img file center-block" name="file_photo"/>
                        <input type="hidden" name="frameimageall[]" class="img-name">
                    </span>
                    <label class="cabinet center-block">
                        <figure class="m-0"><img src="/images/view.png" class="gambar img-fluid item-img-output"/></figure>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
