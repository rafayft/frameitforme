<div class="list_oforder">
    <div class="">

        <div class="personal-head">

            <h5>ORDERS</h5>

        </div>


        <div class="order-col-head">

            <div class="order-col1">

                Order Placed

            </div>


            <div class="order-col1">

                Order Details

            </div>


            <div class="order-col1">

                Delivered

            </div>


            <div class="order-col1">

                Total

            </div>


        </div>

        <?php if ($orders->num_rows() > 0){ ?>
        <?php foreach ($orders->result() as $orderrow) { ?>

        <div class="order-col-data">

            <div class="order-col1">

                <?php echo date('M d,Y', strtotime($orderrow->created_on)); ?>

            </div>


            <div class="order-col1">

                <?php $getframe = $this->db->get_where('tbl_order_frame', array('order_id' => $orderrow->id)); ?>

                Standard Frame Set,

                <?php

                foreach ($getframe->result() as $framess) {

                    echo $framess->name . ',';

                }

                ?>


            </div>


            <div class="order-col1">

                <?php if ($orderrow->status == 4) {
                    echo 'Delivered';
                } else {
                    echo 'Not Delivered';
                } ?>

            </div>


            <div class="order-col1">

                AED <?php echo $orderrow->totalamount; ?>

            </div>


            <div class="order-col1">

                <a href="<?php echo base_url(); ?>profile/order/<?php echo $orderrow->id; ?>"> VIEW SUMMARY </a>

            </div>


        </div>

        <?php } ?>

        <?php } ?>





        <?php

        $draftorder = $saved_orders;

        if ($draftorder->num_rows() > 0)

        {


        ?>


        <div class="">

            <div class="personal-head">

                <h5>SAVED DRAFTS</h5>

            </div>


            <div class="draft-col-head">

                <div class="draft-col1">

                    Draft Name

                </div>

                <div class="draft-col1">

                    Draft Details

                </div>

            </div>

            <?php

            $i = 1;

            foreach ($draftorder->result() as $draft) { ?>

            <div class="draft-col-data  draft_<?php echo $draft->id; ?>">

                <div class="draft-col1">

                    Draft <?php echo $i++; ?>

                </div>

                <div class="draft-col1">

                    <?php $getframes = $this->db->get_where('tbl_order_frame', array('order_id' => $draft->id)); ?>

                        Standard Frame Set,

                    <?php

                    foreach ($getframes->result() as $frame) {

                    echo $frame->name . ',';

                    }

                    ?>


                </div>


                <div class="draft-col-buttons">

                    <a href="javascript:void()" class="deletedraft" data-ids="<?php echo $draft->id; ?>">
                        DELETE </a>

                    <a href="<?php echo site_url(); ?>/Cart/index/<?php echo $draft->id; ?>"> EDIT DRAFT </a>

                </div>

            </div>

            <?php } ?>

                <?php } ?>



        </div>



    </div>
</div>