<section class="checkout-page custom_checkout">

    <div class="container">

        <?php

        $order_id = $this->uri->segment(3);

        if ($order_id != '') {

            $cart = $this->db->get_where('tbl_order', array('id' => $order_id))->result_array();

        } else {


            if ($this->session->userdata('cart') && empty($this->session->userdata('framecart'))) {
                $cart = $this->session->userdata('cart');
            } else if (!empty($this->session->userdata('framecart'))) {
                $cart = $this->session->userdata('cart');
                //$order_id= $this->session->userdata('order_id');
                //$cart = $this->db->get_where('tbl_order', array('id' => $order_id))->result_array();
            } else {
                $cart = array();
            }


        }
       // print_r($cart);
        //print_r($this->session->userdata('totalcart'));
        ?>

        <div class="row">

            <form class="" action="<?php echo site_url(); ?>/cart/createpayment" method="post">

                <div class="col-md-8 order-md-1">

                    <h4 class="mb-3">Billing address</h4>


                    <div class="row">

                        <div class="col-md-4 mb-3">

                            <label for="firstName">First name <span class="text-red">*</span></label>

                            <input type="text" class="form-control" id="firstName" name="firstname" placeholder=""
                                   value="" required="">


                        </div>

                        <div class="col-md-4 mb-3">

                            <label for="lastName">Last name <span class="text-red">*</span></label>

                            <input type="text" class="form-control" id="lastName" name="lastname" placeholder=""
                                   value="" required="">


                        </div>

                        <div class="col-md-4 mb-3">

                            <label for="phone">Phone <span class="text-red">*</span></label>

                            <input type="text" class="form-control" id="phone" name="phone" placeholder=""
                                   value="" required="">


                        </div>

                    </div>


                    <div class="mb-3">

                        <label for="address">Address <span class="text-red">*</span></label>

                        <input type="text" class="form-control" id="address" name="addressone"
                               placeholder="1234 Main St" required="">


                    </div>


                    <div class="mb-3">

                        <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>

                        <input type="text" class="form-control" id="address2" name="addresstwo"
                               placeholder="Apartment or suite">

                    </div>


                    <div class="row">

                        <div class="col-md-5 mb-3">

                            <label for="country">Country <span class="text-red">*</span></label>

                            <select class="custom-select d-block w-100 form-control country" name="country" id="country"
                                    required="">

                                <option value="">Choose...</option>
                                <option selected value="United Arab Emirates">United Arab Emirates</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Egypt">Egypt</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Oman">Oman</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>

                            </select>


                        </div>

                        <div class="col-md-4 mb-3 state">

                            <label for="state">City <span class="text-red">*</span></label>

                            <select class="custom-select d-block w-100 form-control statef" name="state" id="state">

                                <option value="">Choose...</option>

                                <option selected value="Dubai">Dubai</option>
                                <option value="Abu Dhabi">Abu Dhabi</option>
                                <option value="Sharjah">Sharjah</option>

                            </select>


                        </div>
                        <div class="col-md-4 mb-3 statetext" >

                            <label for="state">City <span class="text-red">*</span></label>

                            <input type="text" class="form-control statetextf" id="statetext" placeholder="city" name="state" disabled="disabled">



                        </div>

                        <div class="col-md-3 mb-3">

                            <label for="zip">Zip</label>

                            <input type="text" class="form-control" id="zip" placeholder="" name="zipcode">


                        </div>

                    </div>


                    <div class="row">

                        <div class="col-md-6">

                            <input type="checkbox" name="shippingaddress" value="1" class="shippingselection"></input>
                            Please check this box, If shipping address is different.

                        </div>

                    </div>


                    <div class="row shippingaddress">

                        <h4 class="col-md-12 mb-3">Shipping address</h4>

                        <div class="col-md-12 mb-3">

                            <label for="address">Address</label>

                            <input type="text" class="form-control" id="address" name="shippingaddressone"
                                   placeholder="1234 Main St">


                        </div>


                        <div class="col-md-12 mb-3">

                            <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>

                            <input type="text" class="form-control" id="address2" name="shippingaddresstwo"
                                   placeholder="Apartment or suite">

                        </div>


                        <div class="col-md-5 mb-3">

                            <label for="country">Country</label>

                            <select class="custom-select d-block w-100 form-control country" name="shippingcountry" id="ship"
                                    >

                                <option value="">Choose...</option>
                                <option value="United Arab Emirates">United Arab Emirates</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Egypt">Egypt</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Oman">Oman</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>

                            </select>


                        </div>


                        <div class="col-md-4 mb-3 state">

                            <label for="state">City <span class="text-red">*</span></label>

                            <select class="custom-select d-block w-100 form-control statef" name="shippingstate" id="state">

                                <option value="">Choose...</option>

                                <option value="Dubai">Dubai</option>
                                <option value="Abu Dhabi">Abu Dhabi</option>
                                <option value="Sharjah">Sharjah</option>

                            </select>


                        </div>
                        <div class="col-md-4 mb-3 statetext" >

                            <label for="state">City <span class="text-red">*</span></label>

                            <input type="text" class="form-control statetextf" id="statetext" placeholder="city" name="shippingstate" disabled="disabled">



                        </div>


                        <div class="col-md-3 mb-3">

                            <label for="zip">Zip</label>

                            <input type="text" class="form-control" id="zip" placeholder="" name="shippingzipcode">


                        </div>

                    </div>


                    <hr class="mb-4">


                    <hr class="mb-4">

                    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>"></input>

                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                           class="<?php echo $this->security->get_csrf_token_name(); ?>"
                           value="<?php echo $this->security->get_csrf_hash(); ?>">


                </div>

                <div class="col-md-4 order-md-2 mb-4">

                    <h4 class="d-flex justify-content-between align-items-center mb-3">

                        <span class="text-muted">Your cart</span>

                        <span class="badge badge-secondary badge-pill" style="margin-left: 10px;"><?php echo count($cart); ?></span>

                    </h4>

                    <ul class="list-group mb-3 custom_checkmain">
                        <li style="    background-color: rgba(0,0,0,.05);" class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6"><strong>Product Name</strong></div>
                            <div class="col-md-6"><strong>Total</strong></div>
                        </li>

                        <?php

                        $subtotal = 0;
                        $size='stnd';
                        $installtion = 0;



                        $shipping = 0;

                        $promocode = 0;
                        $subtotals = 0;
                       // print_r($this->session->userdata('totalcart'));
                        //die();
                        $details=$this->session->userdata('totalcart');
                           //print_r($details);
                        if(!empty($details)){
                            // print_r($item);

                            $shipping = $details['shippingamount'];
                            $promocode = $details['promoamount'];

                        }

                        foreach ($cart as $key=>$item) {


                            if ($order_id != '' || !empty($order_id)) {
                                //print_r($item);

                                $subtotals = $item['subtotal'];

                                $totalamount = $item['totalamount'];

                                $shipping = $item['shippingamount'];

                                //$promocode = $item['discount'];
                                $installtion = $item['installationservice'];



                            } else {
                                //print_r($item);
                                $size=isset($item['size'])?$item['size']:$size;

                                $totalamount = $total + 0;

                                if (!empty($this->session->userdata('framecart'))) {
                                    if(is_array($item['price'])) {
                                        foreach ($item['price'] as $itemto) {
                                            $subtotal = $itemto * $item['quantity'];
                                        }
                                    }else{
                                        $subtotal = $item['price'] * $item['quantity'];
                                    }
                                    if(is_array($item['price'])) {
                                        foreach ($item['id'] as $itemid) {
                                            $itemids = $itemid;

                                        }
                                    }else{
                                        $itemids = $item['id'];
                                    }

                                } else {
                                    $subtotal = $item['price'] * $item['quantity'];
                                    $itemids = $item['id'];
                                }
                                if(is_array($details['installtion'])) {
                                    foreach ($details['installtion'] as $inst) {
                                        if ($inst == $itemids) {
                                            $installtion += 75;
                                        }
                                    }
                                }
                                $subtotals+=$subtotal+$installtion;
                                $totalamount=$subtotals-$promocode;


                            }

                            $get_frames = $this->db->get_where('tbl_order_frame', array('order_id' => $order_id));

                            if ($get_frames->num_rows() > 0) {

                                foreach ($get_frames->result_array() as $frame) {

                                    ?>

                                    <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                                        <div class="col-md-6"><?php echo $frame['name']; ?></div>
                                        <div class="col-md-6"><?php echo $frame['price']*$frame['quantity']; ?> AED</div>
                                    </li>
                                    <?php

                                }

                            } else {

                                ?>

                                <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                                    <div class="col-md-6"><?php echo is_array($item['name']) ? $item['name'][0] : $item['name']; ?><br></div>
                                    <div class="col-md-6"><?php echo $subtotal ?> AED</div>
                                </li>



                                <input type="hidden" name="fameimage[]"
                                       value="<?php echo is_array($item['photo']) ? $item['photo'][0] : $item['photo'];  ?>"></input>

                                <input type="hidden" name="frameamount[]"
                                       value="<?php echo is_array($item['price']) ? $item['price'][0] : $item['price'];?>"></input>

                                <input type="hidden" name="framequantity[]"
                                       value="<?php echo is_array($item['quantity']) ? $item['quantity'][0] : $item['quantity']; ?>"></input>

                                <input type="hidden" name="famrname[]"
                                       value="<?php echo is_array($item['name']) ? $item['name'][0] : $item['name']; ?>"></input>

                                <input type="hidden" name="id[]" value="<?php echo $itemids; ?>"></input>


                                <?php

                            }


                            ?>


                        <?php }

                        $count=count($cart);
                        ?>

                        <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6">Installation</div>
                            <div class="col-md-6"> <span id="installtion_text"><?php echo $installtion; ?></span> AED</div>
                        </li>


                        <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6">Subtotal</div>
                            <div class="col-md-6"> <span id="subtotal_text"><?php echo $subtotals; ?></span> AED</div>
                        </li>

                        <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6">Shipping</div>
                            <div class="col-md-6"> <span id="shippingfee"><?php echo $shipping; ?></span> AED</div>
                        </li>

                        <li class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6">PROMO CODE</div>
                            <div class="col-md-6"><?php echo $promocode; ?> AED</div>
                        </li>

                        <li class="list-group-item d-flex justify-content-between lh-condensed">


                            <input type="hidden" class="shippingfee" name="shippingfee" value="<?php echo $shipping; ?>"></input>
                            <input type="hidden" name="discount" value="<?php echo $promocode; ?>"></input>
                            <input type="hidden" name="intial_installtion" id="intial_installtion" value="<?php echo $installtion; ?>"></input>
                            <input type="hidden" name="installtion" id="installtion_fee" value="<?php echo $installtion; ?>"></input>
                            <input type="hidden" name="intial_subtotal" id="intial_subtotal" value="<?php echo $subtotals; ?>"></input>
                            <input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotals; ?>"></input>



                        </li>

                        <li style="    background-color: rgba(0,0,0,.05);" class="list-group-item d-flex justify-content-between lh-condensed checkout_name">
                            <div class="col-md-6"><strong>Total</strong></div>
                            <div class="col-md-6"><strong>
                                   <span id="totalamount"><?php echo $totalamount; ?></span> AED
                                    <input type="hidden" class="totalamount" name="totalamount" value="<?php echo $totalamount; ?>"></input>
                                    <input type="hidden" class="totalamounts" name="totalamounts" value="<?php echo $totalamount; ?>"></input>
                                </strong></div>
                        </li>



                    </ul>


                    <button class="btn btn-primary btn-lg btn-block custom_checkoutBtn" type="submit">Continue to
                        checkout
                    </button>

                </div>

            </form>

        </div>

    </div>
<script  type="text/javascript">
    var size="<?=$size?>";
    var count="<?=$count?>";
    var installtion_fee="<?=$installtion?>";
    $(".statetext").hide();
    $(".statef").on('change', function (event) {
        if($(this).val()!="Dubai"){

            var total= $('.totalamounts').val();

            $('#installtion_text').text(0);
            $('#installtion_fee').val(0);

            $('#subtotal_text').text(parseVal(total)-parseVal(installtion_fee));
            $('#subtotal').val(parseVal(total)-parseVal(installtion_fee));


            $('#totalamount').text(parseVal(total)-parseVal(installtion_fee));
            $('.totalamount').val(parseVal(total)-parseVal(installtion_fee));

        }else {
            var total= $('.totalamounts').val();
            var intial_subtotal= $('#intial_subtotal').val();
            var intial_installtion= $('#intial_installtion').val();

            $('#installtion_text').text(intial_installtion);
            $('#installtion_fee').val(intial_installtion);


            $('#subtotal_text').text(intial_subtotal);
            $('#subtotal').val(intial_subtotal);


            $('#totalamount').text(total);
            $('.totalamount').val(total);
        }
    });
    $(".country").on('change', function (event) {

       var id=event.target.id;
        var country=$(this).val();
        if(country=="United Arab Emirates"){
            $(".statetextf").attr("disabled", "disabled");
            $(".statef").attr("disabled", false);
            $(".state").show();
            $(".statetext").hide();
        }else {
            $(".statef").attr("disabled", "disabled");
            $(".statetextf").attr("disabled", false);
            $(".state").hide();
            $(".statetext").show();


        }

       console.log(id);
        if ($(".shippingselection").is(':checked')) {
            if(id=="country"){
                return true;
            }
        }


        var country=$(this).val();
        var fee=0;

        var array=[
        ["United Arab Emirates",0,0,0],
        ["Bahrain",306.1,232.6,262],
        ["Egypt",292.7,296.1,339.2],
        ["Jordan",315,296.1,315],
        ["Kuwait",265.7,232.6,262],
        ["Lebanon",315,296.1,315],
        ["Morocco",545,545,545],
        ["Oman",306.1,232.6,262],
        ["Qatar",306.1,232.6,262],
        ["Saudi Arabia",286.7,286.7,286.7]
    ];

        array.forEach(function(value, index) {
            if(country==value[0]){
                if(count==1){
                    if(size=="sml"){
                        fee=value[2];
                    }else if(size=="lrg"){
                        fee=value[3];
                    }else {
                        fee=value[1];
                    }
                }else{
                    if(count==2)
                        fee=value[1];
                    else if(count==3)
                        fee=value[1]+value[3];
                    else if(count==4)
                        fee=2*value[1];
                    else if(count==5)
                        fee=2*(value[1]+value[3]);
                    else if(count==6)
                        fee=3*value[1];
                    else
                        fee= count/2*value[1]
                }

            }

        });


        var total= $('.totalamounts').val();
        $('#shippingfee').text(fee);
        $('.shippingfee').val(fee);
        $('#totalamount').text(parseVal(fee)+parseVal(total));
        $('.totalamount').val(parseVal(fee)+parseVal(total));
        console.log(fee );


        if(country=="United Arab Emirates"){

            var total= $('.totalamounts').val();
            var intial_subtotal= $('#intial_subtotal').val();
            var intial_installtion= $('#intial_installtion').val();

            $('#installtion_text').text(intial_installtion);
            $('#installtion_fee').val(intial_installtion);


            $('#subtotal_text').text(intial_subtotal);
            $('#subtotal').val(intial_subtotal);


            $('#totalamount').text(total);
            $('.totalamount').val(total);
        }else {

            var total= $('.totalamounts').val();

            $('#installtion_text').text(0);
            $('#installtion_fee').val(0);

            $('#subtotal_text').text(parseVal(total)-parseVal(installtion_fee));
            $('#subtotal').val(parseVal(total)-parseVal(installtion_fee));


            $('#totalamount').text(parseVal(total)-parseVal(installtion_fee)+parseVal(fee));
            $('.totalamount').val(parseVal(total)-parseVal(installtion_fee)+parseVal(fee));

        }


    });
</script>
</section>