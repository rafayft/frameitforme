<?php if ($page_home['home_welcome_status'] == 'Show'): ?>

    <section class="home-banner">

        <div class="container">

            <div class="banner-text">

                <h5><?php echo $page_home['home_welcome_title']; ?></h5>

                <?php echo $page_home['home_welcome_text']; ?>

                <img src="<?php echo base_url(); ?>public/uploads/<?php echo $page_home['home_welcome_video_bg']; ?>">

            </div>

        </div>

    </section>

<?php endif; ?>

<?php if ($page_home['home_why_choose_status'] == 'Show'): ?>

    <section class="info-text">

        <div class="container">

            <div class="row">

                <?php foreach ($why_choose as $row) { ?>


                    <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                        <div class="info-box">

                            <h5><img src="<?php echo base_url(); ?>public/uploads/<?php echo $row['photo']; ?>">
                                <span> <?php echo $row['name']; ?></h5>

                            <p><?php echo $row['content']; ?></p>

                        </div>

                    </div>

                <?php } ?>

                <!--<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <div class="info-box">

                    <h5><img src="images/frame-icon1.png"> <span> Select your <br> frame set</h5>

                    <p>Get started with our standard 10-piece frame set or make a custom order and curate it to fit your specific wall space.</p>

                </div>

                </div>

                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <div class="info-box">

                    <h5><img src="images/frame-icon2.png"> <span> Design your <br /> wall space</h5>

                    <p>Our online system will guide you through the process so you can design your wall from the comfort of your couch.</p>

                </div>

                </div>

                <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <div class="info-box">

                    <h5><img src="images/frame-icon3.png"> <span> Easily install <br /> your frames</h5>

                    <p>We offer delivery and installation in Dubai, and our easy-to-follow instructions will guide you along the way.</p>

                </div>

                -->

            </div>


        </div>

        </div>

    </section>

<?php endif; ?>





<?php if ($page_home['home_service_status'] == 'Show'): ?>

    <section class="standard-frame">

        <div class="standard-text">

            <h5><?php echo $page_home['home_service_title']; ?></h5>

            <?php echo $page_home['home_service_desccription']; ?>

        </div>


        <div class="get-started">

            <a href="<?php if ($this->session->userdata('logged_in')) {
                echo base_url() . 'frame_layout';
            } else {
                echo base_url() . 'register';
            } ?>"><span>GET STARTED</span></a>

        </div>


    </section>

<?php endif; ?>


<!--Portfolio Start-->

<?php if ($page_home['home_portfolio_status'] == 'Show'): ?>

    <section class="home-collection">


        <div class="container">

            <div class="collection-head">

                <h1><?php echo $page_home['home_portfolio_title']; ?></h1>

                <h5><?php echo $page_home['home_portfolio_subtitle']; ?> </h5>

                <?php echo $page_home['home_portfolio_desccription']; ?>

            </div>


            <div class="collection-cat1 collection_cat_bottom">

                <div class="row">

                    <?php
                    $kids_collection = $this->db->get_where('tbl_portfolio', array('id' => 11))->result_array();
                    $i = 0;
                    ?>
                    <?php
                    foreach ($kids_collection as $kidsrow) {
                        $row = $i++;
                        ?>
                        <?php
                        $pics = $this->db->get_where('tbl_portfolio_photo', array('portfolio_id' => $kidsrow['id']));
                        $picscount = $pics->num_rows();
                        if ($picscount == 3) {
                            $cols = 'col-md-4 col-lg-4 col-sm-4 col-xs-6';
                        } else if ($picscount == 6) {
                            $cols = 'col-md-2 col-lg-2 col-sm-2 col-xs-3';
                        } else {
                            $cols = 'col-md-6 col-lg-6 col-sm-6 col-xs-6';
                        }
                        foreach ($pics->result_array() as $pic) {
                            ?>

                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-6">

                                <div class="img_bor">

                                    <div class="collection-image">

                                        <img src="<?php echo base_url(); ?>public/uploads//portfolio_photos/<?php echo $pic['photo']; ?>">

                                    </div>

                                </div>

                            </div>

                        <?php } ?>
                    <?php } ?>

                </div>


                <div class="collection-btn"><a href="<?php echo base_url(); ?>kids_collection" class="cart-button"> <img
                                src="<?php echo base_url(); ?>images/cart-icon.png"> <span>SHOP NOW</span> </a></div>

            </div>

        </div>

    </section>


<?php endif; ?>

<!--Portfolio End-->





