<?php

include('wp-load.php');
require('fpdf/fpdf.php');

class PDF extends FPDF
{
    // Page header
    function Header()
    {
        // Logo
        $this->Image('https://highereducation.ae/intcas/wp-content/uploads/2019/09/intcas-logo.png', 10, 6, 30);
        // Arial bold 15
        $this->SetFont('Arial', '', 8);
        // Move to the right
        $this->Cell(80);
        // Title
        $this->Cell(0, 0, '+44 (0) 20 7118 1822', 0, 1, 'R');
        $this->Cell(0, 10, 'www.intcas.com', 0, 1, 'R');
        $this->Cell(0, 0, '4th Floor AMP House 4 Dingwall Road Croydon CR0 2LX United Kingdom', 0, 1, 'R');
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-20);
        // Arial italic 8
        $this->SetFont('Arial', '', 8);
        // Page number
        $this->Cell(0, 0, '______________________________________________________________________________________________________', 0, 1, 'C');
        $this->Cell(0, 10, 'INTCAS (UK) Limited', 0, 1, 'C');
        $this->Cell(0, 0, 'Incorporated under the laws of England & Wales and registered at Companies House under number 07867627', 0, 1, 'C');
        $this->Cell(0, 10, 'VAT Registration Number: 170 8099 94', 0, 1, 'C');
    }
}

$user_id = wp_create_user($_POST['username'], $_POST['password'], $_POST['contact_email']);
if (isset($user_id->errors)) {
    header('Location: ' . home_url() . '/register/?msg=error');
} else {
    update_user_meta($user_id, 'institution_name', $_POST['institution_name']);
    update_user_meta($user_id, 'country', $_POST['country']);
    update_user_meta($user_id, 'contact_name', $_POST['contact_name']);
    update_user_meta($user_id, 'position', $_POST['position']);
    update_user_meta($user_id, 'phone', $_POST['phone']);
    $event_location = explode(",", $_COOKIE['booking_item_id']);
    $item_price = explode(',', $_COOKIE['booking_item_price']);
    $multi_booking = explode(',', $_COOKIE['booking_item_quantity']);
    $event_date = explode(',', $_COOKIE['booking_item_date']);
    for ($i = 0; $i < count($event_location); $i++) {
        if ($event_location[$i] == '1') {
            $region = 'Middle East';
        } elseif ($event_location[$i] == '2') {
            $region = 'India';
        } elseif ($event_location[$i] == '3') {
            $region = 'Canada';
        } else {
            $region = '';
        }
        update_user_meta($user_id, 'event_location_' . $event_location[$i], $region);
        update_user_meta($user_id, 'event_date_' . $event_location[$i], $event_date[$i]);
        update_user_meta($user_id, 'multi_booking_' . $event_location[$i], $multi_booking[$i]);
        update_user_meta($user_id, 'multi_booking_price_' . $event_location[$i], $item_price[$i]);
    }
    update_user_meta($user_id, 'total_amount', $_POST['total_amount']);

    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->SetFont('Arial', 'B', 16);
    $pdf->Cell(40, 10, 'Hello World!');

    $to = 'hello@navaneeth.me';
    $from = "no-reply@hello.navaneeth.me";
    $message = "Test email with attachment";
// a random hash will be necessary to send mixed content
    $separator = md5(time());

// carriage return type (we use a PHP end of line constant)
    $eol = PHP_EOL;

// attachment name
    $filename = 'sample_data.pdf';

// encode data (puts attachment in proper format)
    $pdfdoc = $pdf->Output("", "S");
    $attachment = chunk_split(base64_encode($pdfdoc));

// main header
    $headers = "From: Navaneeth.me <" . $from . ">" . $eol;

    $headers .= "MIME-Version: 1.0" . $eol;
    $headers .= "Content-Type: multipart/mixed; boundary=\"" . $separator . "\"";

// no more headers after this, we start the body! //

    $body = "--" . $separator . $eol;
    $body .= "Content-Transfer-Encoding: 7bit" . $eol . $eol;
    $body .= "" . $eol;

// message
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: text/html; charset=\"iso-8859-1\"" . $eol;
    $body .= "Content-Transfer-Encoding: 8bit" . $eol . $eol;
    $body .= $message . $eol;

// attachment
    $body .= "--" . $separator . $eol;
    $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . $eol;
    $body .= "Content-Transfer-Encoding: base64" . $eol;
    $body .= "Content-Disposition: attachment" . $eol . $eol;
    $body .= $attachment . $eol;
    $body .= "--" . $separator . "--";

// send message
    $mail_sent = mail($to, $subject, $body, $headers);
//if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
    echo $mail_sent ? "Mail sent" : "Mail failed";


    $Adminto = 'mailto:hira@digihyve.com';//'bilal.azam1@outlook.com';
    $Adminsubject = 'Event Booking Request';
    $Adminbody = 'You recieved a new event booking request. Booking details are: <br>';
    $Adminbody .= 'Instituion Name: ' . $_POST['institution_name'] . '<br>';
    $Adminbody .= 'country: ' . $_POST['country'] . '<br>';
    $Adminbody .= 'Full Name: ' . $_POST['contact_name'] . '<br>';
    $Adminbody .= 'Email: ' . $_POST['contact_email'] . '<br>';
    $Adminbody .= 'Position: ' . $_POST['position'] . '<br>';
    $Adminbody .= 'Phone: ' . $_POST['phone'] . '<br>';
    $Adminbody .= 'Event Date and Location: ' . $_POST['event_date'] . '<br>';
    $Adminbody .= 'Number of Bookings: ' . $_POST['multi_booking'] . '<br>';
    $Adminbody .= 'Availability: ' . $_POST['availability'] . '<br>';
    $Adminbody .= 'Total Amount: ' . $_POST['total_amount'] . '<br>';
    $Adminheaders = array('Content-Type: text/html; charset=UTF-8');

    wp_mail($Adminto, $Adminsubject, $Adminbody, $Adminheaders);

    header('Location: thankyou/');
}
